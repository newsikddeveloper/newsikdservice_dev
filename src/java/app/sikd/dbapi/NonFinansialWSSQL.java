/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.dbapi;

import app.sikd.dbapi.apbd.IAPBDConstants;
import app.sikd.entity.ws.DaftarPegawai_WS;
import app.sikd.entity.ws.RincianPegawai_WS;
import app.sikd.entity.ws.SKPDPegawai_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;

/**
 *
 * @author detra
 */
public class NonFinansialWSSQL implements INonFinansialWSSQL{
    
    @Override
    public long getLastDaftarPegawaiIndex(Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select max(pegawaiindex) id from daftarpegawai");
            if (rs.next()) {
                return rs.getLong("id");
            }
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public boolean cekFinalDaftarPegawai(String kodeSatker, short tahun, short bulanGaji, short jenisData, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select statusdata from daftarpegawai where kodesatker= '" + kodeSatker + "' "
                    + " and tahunanggaran=" + tahun 
                    + " and bulangaji=" + bulanGaji
                    + " and jenisdata=" + jenisData
                    
            );
            short status = 0;
            if (rs.next()) {
                status = rs.getShort("statusdata");
            }
            if( status == 0 ) return false;
            return true;
        } catch (SQLException ex) {
            throw new SQLException("gagal mengambil Status data Daftar Pegawai: " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public String createDaftarPegawai(DaftarPegawai_WS obj, Connection conn) throws SQLException {
        int level = conn.getTransactionIsolation();
        try {
                conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            } catch (Exception e) {
            }
        PreparedStatement stm = null;
        try {
            String sql = "insert into daftarpegawai ("
                    + IAPBDConstants.ATTR_KODE_SATKER + ", "
                    + IAPBDConstants.ATTR_KODE_PEMDA + ", "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + ", "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + ", "
                    + " bulangaji, jenisdata, tanggalpengiriman, statusdata, namaaplikasi, pengembangaplikasi"
                    + ") "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);
            stm.setString(1, obj.getKodeSatker().trim());
            stm.setString(2, obj.getKodePemda().trim());
            stm.setString(3, obj.getNamaPemda().trim());
            stm.setShort(4, obj.getTahunAnggaran());
            stm.setShort(5, obj.getBulanGaji());
            stm.setShort(6, obj.getJenisData());            
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            stm.setTimestamp(7, time);
            stm.setShort(8, obj.getStatusData());
            stm.setString(9, obj.getNamaAplikasi());
            stm.setString(10, obj.getPengembangAplikasi());
            
            stm.executeUpdate();
            return "Sukses Create Daftar Pegawai";
        } catch (SIKDServiceException | SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
            try {
                conn.setTransactionIsolation(level);
            } catch (Exception e) {
            }
        }
    }
    
    /*
    @Override
    public String updateDaftarPegawai(DaftarPegawai_WS obj, long pegawaiIndex, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "update daftarpegawai set "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=?, "
                    + IAPBDConstants.ATTR_KODE_PEMDA + "=?, "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + "=?, "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? "
                    + " WHERE pegawaiindex=?";
            stm = conn.prepareStatement(sql);
            stm.setString(1, obj.getKodeSatker().trim());
            stm.setString(2, obj.getKodePemda().trim());
            stm.setString(3, obj.getNamaPemda().trim());
            stm.setShort(4, obj.getTahunAnggaran());
            stm.setLong(5, pegawaiIndex);
            stm.executeUpdate();
            return "Sukses Update Daftar Pegawai";
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    */
    @Override
    public void deleteDaftarPegawai(String kodeSatker, short thn, short bulanGaji, short jenisData, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from daftarpegawai where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? and "
                    + " bulangaji= ? and "
                    + " jenisdata= ?";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, thn);
            stm.setShort(3, bulanGaji);
            stm.setShort(4, jenisData);
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    
    @Override
    public long getDaftarPegawaiIndex(String kodeSatker, String kodePemda, short thn, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {            
            String sql = "select pegawaiindex from daftarpegawai where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_KODE_PEMDA + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=?";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setString(2, kodePemda.trim());
            stm.setShort(3, thn);
            rs = stm.executeQuery();
            long id = 0;
            if( rs.next() ) id = rs.getLong("pegawaiindex");
            return id;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if( rs!= null ) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public long createSKPDPegawai(long indexPegawai, SKPDPegawai_WS obj, Connection conn) throws SQLException {
        int level = conn.getTransactionIsolation();
        try {
                conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            } catch (Exception e) {
            }
        PreparedStatement stm = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            String sql = "insert into skpdpegawai ("
                    + "pegawaiindex, kodeurusan, namaurusan, kodeskpd, namaskpd"
                    + ")"
                    + "VALUES (?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);
            stm.setLong(1, indexPegawai);
            stm.setString(2, obj.getKodeUrusan());
            stm.setString(3, obj.getNamaUrusan());
            stm.setString(4, obj.getKodeSKPD());
            stm.setString(5, obj.getNamaSKPD());
            stm.executeUpdate();
            statement = conn.createStatement();
            rs = statement.executeQuery("select max(skpdindex) id from skpdpegawai");
            if( rs.next() )
                return rs.getLong("id");
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if( rs!= null) rs.close();            
            if (statement != null) statement.close();
            if (stm != null) stm.close();
            try {
                conn.setTransactionIsolation(level);
            } catch (Exception e) {
            }
        }
    }
    
    @Override
    public String createRincianPegawai(long indexSKPD, RincianPegawai_WS obj, Connection conn) throws SQLException {
        int level = conn.getTransactionIsolation();
        try {
                conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            } catch (Exception e) {
            }
        PreparedStatement stm = null;
        try {
            String sql = "insert into rincianpegawai "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "
                    + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "
                    + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "
                    + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "
                    + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);            
            stm.setLong(1, indexSKPD);
            stm.setString(2, obj.getNip());
            stm.setString(3, obj.getNamaPegawai());
            stm.setString(4, obj.getNpwpPegawai());
            stm.setString(5, obj.getKodeStatus());
            stm.setString(6, obj.getUraianStatus());
            stm.setShort(7, obj.getGender());
            if( obj.getTanggalLahir() != null)
            stm.setDate(8, new java.sql.Date(obj.getTanggalLahir().getTime()));
            else stm.setNull(8, Types.DATE);
            stm.setString(9, obj.getTempatLahir());
            stm.setString(10, obj.getKodeGolongan());
            stm.setString(11, obj.getNamaGolongan());
            stm.setString(12, obj.getKodeStruktural());
            stm.setString(13, obj.getNamaStruktural());
            stm.setString(14, obj.getKodeKelompokFungsional());
            stm.setString(15, obj.getNamaKelompokFungsional());
            stm.setString(16, obj.getKodeJabatanFungsional());
            stm.setString(17, obj.getNamaJabatanFungsional());
            stm.setString(18, obj.getKodeJabatanKhusus());
            stm.setString(19, obj.getNamaJabatanKhusus());
            stm.setString(20, obj.getKodeGuru());
            stm.setString(21, obj.getNamaGuru());
            stm.setString(22, obj.getKodeSertifikasi());
            stm.setString(23, obj.getNamaSertifikasi());
            stm.setShort(24, obj.getJumlahPasangan());
            stm.setShort(25, obj.getJumlahAnak());
            stm.setDouble(26, obj.getGajiPokok());
            stm.setDouble(27, obj.getPersentaseGajiPokok());
            stm.setDouble(28, obj.getTunjanganPasangan());
            stm.setDouble(29, obj.getTunjanganAnak());
            stm.setDouble(30, obj.getTunjanganPerbaikan());
            stm.setDouble(31, obj.getTunjanganStruktural());
            stm.setDouble(32, obj.getTunjanganFungsional());
            stm.setDouble(33, obj.getTunjanganKhusus());
            stm.setDouble(34, obj.getTunjanganUmum());
            stm.setDouble(35, obj.getTunjanganKemahalan());
            stm.setDouble(36, obj.getTunjanganPendidikan());
            stm.setDouble(37, obj.getTunjanganTerpencil());
            stm.setDouble(38, obj.getTunjanganAskes());
            stm.setDouble(39, obj.getTunjanganPajak());
            stm.setDouble(40, obj.getTunjanganPembulatan());
            stm.setDouble(41, obj.getTunjanganBeras());
            stm.setDouble(42, obj.getJumlahKotor());
            stm.setDouble(43, obj.getIwp());
            stm.setDouble(44, obj.getPotonganAskes());
            stm.setDouble(45, obj.getPotonganBulog());
            stm.setDouble(46, obj.getPotonganPerum());
            stm.setDouble(47, obj.getPotonganPajak());
            stm.setDouble(48, obj.getPotonganSewarumah());
            stm.setDouble(49, obj.getPotonganUtang());
            stm.setDouble(50, obj.getJumlahPotongan());
            stm.setDouble(51, obj.getJumlahBersih());
            
            stm.executeUpdate();
            return "Sukses Create Rincian Pegawai";
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
            try {
                conn.setTransactionIsolation(level);
            } catch (Exception e) {
            }
        }
    }
    
    /*@Override
    public String createRincianDaftarPegawai(long indexPegawai, RincianDaftarPegawai_WS obj, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "insert into rinciandaftarpegawai "
                    + "VALUES (?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);
            stm.setLong(1, indexPegawai);
            stm.setShort(2, obj.getKodeGolongan());//SIKDUtil.getGolonganAsShort(obj.getKodeGolongan().trim()));
            stm.setShort(3, obj.getKodeGrade());//SIKDUtil.getGradeAsShort(obj.getKodeGrade().trim()));
            stm.setShort(4, obj.getKodeEselon());//SIKDUtil.getEselonAsShort(obj.getKodeEselon()));
            stm.setInt(5, obj.getJumlah());
            stm.executeUpdate();
            return "Sukses Create Rincian Daftar Pegawai";
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    */
}
