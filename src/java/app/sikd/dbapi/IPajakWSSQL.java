/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.dbapi;

import app.sikd.entity.ws.AkunDTHSKPD_WS;
import app.sikd.entity.ws.BPHTB_WS;
import app.sikd.entity.ws.DTH_WS;
import app.sikd.entity.ws.Hiburan_WS;
import app.sikd.entity.ws.Hotel_WS;
import app.sikd.entity.ws.IMB_WS;
import app.sikd.entity.ws.IzinUsaha_WS;
import app.sikd.entity.ws.Kendaraan_WS;
import app.sikd.entity.ws.PajakDTHSKPD_WS;
import app.sikd.entity.ws.PajakDanRetribusi_WS;
import app.sikd.entity.ws.Restoran_WS;
import app.sikd.entity.ws.RincianBPHTB_WS;
import app.sikd.entity.ws.RincianDTHSKPD_WS;
import app.sikd.entity.ws.RincianHiburan_WS;
import app.sikd.entity.ws.RincianHotel_WS;
import app.sikd.entity.ws.RincianIMB_WS;
import app.sikd.entity.ws.RincianIzinUsaha_WS;
import app.sikd.entity.ws.RincianKendaraan_WS;
import app.sikd.entity.ws.RincianPajakDanRetribusi_WS;
import app.sikd.entity.ws.RincianRestoran_WS;
import app.sikd.entity.ws.SP2DKegiatan_WS;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author detra
 */
public interface IPajakWSSQL extends Serializable{
    public long getLastDTHIndex(Connection conn) throws SQLException;
    public long createDTH(DTH_WS dth, short iotype, Connection conn) throws SQLException;
//    public String updateDTH(DTH_WS dth, long dthIndex, Connection conn) throws SQLException;
    public void deleteDTH(String kodeSatker, short thn, short periode, Connection conn) throws SQLException;
    public void deleteDTH(String kodeSatker, short thn, Connection conn) throws SQLException;
//    public long getDTHIndex(String kodeSatker, short thn, short periode, Connection conn) throws SQLException;
//    public long getDTHIndex(String kodeSatker, short thn, Connection conn) throws SQLException;
    
    public long getLastRincianDTHSKPDIndex(Connection conn) throws SQLException;
    public long createRincianDTHSKPD(RincianDTHSKPD_WS rincian, long dthindex, Connection conn) throws SQLException ;
    
    public long getLastSp2DKegiatanIndex(Connection conn) throws SQLException;
    public long createSP2DKegiatan(SP2DKegiatan_WS kegiatan, long rinciandthindex, Connection conn) throws SQLException;
//    public void deleteSP2DKegiatan(long dthIndex, String kodeUrusanProgram, String kodeUrusanPelaksana, String kodeSKPD, Connection conn) throws SQLException;
    
    public long getLastAkunDTHIndex(Connection conn) throws SQLException;
    public void createAkunDTHSKPD(AkunDTHSKPD_WS akun, long rinciandthskpdindex, Connection conn) throws SQLException;
    public void createPajakDTHSKPD(PajakDTHSKPD_WS pajak, long rinciandthskpdindex, Connection conn) throws SQLException;
    
    
    public boolean cekFinalDTH(String kodeSatker, short tahun, short periode, Connection conn) throws SQLException;
    public boolean cekFinalDTH(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public String getDTHInfo(String kodeSatker, short tahun, short periode, Connection conn) throws SQLException ;
    
    
    public boolean cekFinalKendaraan(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public long createKendaraan(Kendaraan_WS kendaraan, Connection conn) throws SQLException;
    public void deleteKendaraan(String kodeSatker, String kodePemda, short thn, Connection conn) throws SQLException ;
    public String createRincianKendaraan(RincianKendaraan_WS rincian, long indexKendaraan, Connection conn) throws SQLException;
    public boolean cekFinalHotel(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public long createHotel(Hotel_WS hotel, Connection conn) throws SQLException;
    public void deleteHotel(String kodeSatker, String kodePemda, short thn, Connection conn) throws SQLException;
    public String createRincianHotel(RincianHotel_WS rincian, long indexHotel, Connection conn) throws SQLException ;
    public boolean cekFinalHiburan(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public long createHiburan(Hiburan_WS hiburan, Connection conn) throws SQLException;
    public void deleteHiburan(String kodeSatker, String kodePemda, short thn, Connection conn) throws SQLException;
    public String createRincianHiburan(RincianHiburan_WS rincian, long indexHiburan, Connection conn) throws SQLException;
    public boolean cekFinalIzinUsaha(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public long createIzinUsaha(IzinUsaha_WS usaha, Connection conn) throws SQLException;
    public void deleteIzinUsaha(String kodeSatker, String kodePemda, short thn, Connection conn) throws SQLException;
    public String createRincianIzinUsaha(RincianIzinUsaha_WS rincian, long indexIzinUsaha, Connection conn) throws SQLException;
    public boolean cekFinalBPHTB(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public long createBPHTB(BPHTB_WS bphtb, Connection conn) throws SQLException;
    public void deleteBPHTB(String kodeSatker, String kodePemda, short thn, Connection conn) throws SQLException;
    public String createRincianBPHTB(RincianBPHTB_WS rincian, long indexBPHTB, Connection conn) throws SQLException;
    public boolean cekFinalIMB(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public long createIMB(IMB_WS imb, Connection conn) throws SQLException;
    public void deleteIMB(String kodeSatker, String kodePemda, short thn, Connection conn) throws SQLException;
    public String createRincianIMB(RincianIMB_WS rincian, long indexIMB, Connection conn) throws SQLException;
    public boolean cekFinalRestoran(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public long createRestoran(Restoran_WS restoran, Connection conn) throws SQLException;
    public void deleteRestoran(String kodeSatker, String kodePemda, short thn, Connection conn) throws SQLException;
    public String createRincianRestoran(RincianRestoran_WS rincian, long indexRestoran, Connection conn) throws SQLException;
    
    
    public boolean cekFinalPajakDanRetribusiDaerah(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public void deletePajakDanRetribusiDaerah(String kodeSatker, short thn, Connection conn) throws SQLException;
    public long createPajakDanRetribusiDaerah(PajakDanRetribusi_WS obj, Connection conn) throws SQLException ;
    public String createRincianPajakDanRetribusiDaerah(RincianPajakDanRetribusi_WS rincian, long indexPajak, Connection conn) throws SQLException ;
    
    
    
}
