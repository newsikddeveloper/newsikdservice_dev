/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.dbapi;

import app.sikd.entity.ws.DaftarPegawai_WS;
import app.sikd.entity.ws.RincianDaftarPegawai_WS;
import app.sikd.entity.ws.RincianPegawai_WS;
import app.sikd.entity.ws.SKPDPegawai_WS;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author detra
 */
public interface INonFinansialWSSQL {
    
    public long getLastDaftarPegawaiIndex(Connection conn) throws SQLException;
    public boolean cekFinalDaftarPegawai(String kodeSatker, short tahun, short bulanGaji, short jenisData, Connection conn) throws SQLException;
    public String createDaftarPegawai(DaftarPegawai_WS obj, Connection conn) throws SQLException;
//    public String updateDaftarPegawai(DaftarPegawai_WS obj, long pegawaiIndex, Connection conn) throws SQLException;
    public void deleteDaftarPegawai(String kodeSatker, short thn, short bulanGaji, short jenisData, Connection conn) throws SQLException;
    public long getDaftarPegawaiIndex(String kodeSatker, String kodePemda, short thn, Connection conn) throws SQLException;
    public long createSKPDPegawai(long indexPegawai, SKPDPegawai_WS obj, Connection conn) throws SQLException;
    public String createRincianPegawai(long indexPegawai, RincianPegawai_WS obj, Connection conn) throws SQLException;
//    public String createRincianDaftarPegawai(long indexPegawai, RincianDaftarPegawai_WS obj, Connection conn) throws SQLException;
    
}
