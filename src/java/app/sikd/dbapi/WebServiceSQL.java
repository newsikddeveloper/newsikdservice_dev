package app.sikd.dbapi;

import app.sikd.dbapi.apbd.IAPBDConstants;
import app.sikd.entity.ws.Apbd_WS;
import app.sikd.entity.ws.ArusKasSaldo_WS;
import app.sikd.entity.ws.ArusKas_WS;
import app.sikd.entity.ws.ArusKeluarInvestasi_WS;
import app.sikd.entity.ws.ArusKeluarNonAnggaran_WS;
import app.sikd.entity.ws.ArusKeluarOperasi_WS;
import app.sikd.entity.ws.ArusKeluarPembiayaan_WS;
import app.sikd.entity.ws.ArusMasukInvestasi_WS;
import app.sikd.entity.ws.ArusMasukNonAnggaran_WS;
import app.sikd.entity.ws.ArusMasukOperasi_WS;
import app.sikd.entity.ws.ArusMasukPembiayaan_WS;
import app.sikd.entity.ws.DefisitNonOperasional_WS;
import app.sikd.entity.ws.KegiatanAPBD_WS;
import app.sikd.entity.ws.KodeRekeningAPBD_WS;
import app.sikd.entity.ws.LaporanOperasionalAkunJenis_WS;
import app.sikd.entity.ws.LaporanOperasionalAkunKelompok_WS;
import app.sikd.entity.ws.LaporanOperasionalAkunObjek_WS;
import app.sikd.entity.ws.LaporanOperasionalAkunUtama_WS;
import app.sikd.entity.ws.LaporanOperasional_WS;
import app.sikd.entity.ws.NeracaAkunJenis_WS;
import app.sikd.entity.ws.NeracaAkunKelompok_WS;
import app.sikd.entity.ws.NeracaAkunObjek_WS;
import app.sikd.entity.ws.NeracaAkunUtama_WS;
import app.sikd.entity.ws.Neraca_WS;
import app.sikd.entity.ws.PerhitunganFihakKetiga_WS;
import app.sikd.entity.ws.PerubahanEkuitasDetail_WS;
import app.sikd.entity.ws.PerubahanEkuitas_WS;
import app.sikd.entity.ws.PerubahanSalDetail_WS;
import app.sikd.entity.ws.PerubahanSal_WS;
import app.sikd.entity.ws.PinjamanDaerah_WS;
import app.sikd.entity.ws.PosLuarBiasaOperasional_WS;
import app.sikd.entity.ws.RealisasiAPBD_WS;
import app.sikd.entity.ws.RealisasiKegiatanAPBD_WS;
import app.sikd.entity.ws.RealisasiKodeRekeningAPBD_WS;
import app.sikd.entity.ws.RincianPerhitunganFihakKetiga_WS;
import app.sikd.entity.ws.RincianPinjamanDaerah_WS;
import app.sikd.entity.ws.Skpd_WS;
import app.sikd.entity.ws.SurplusNonOperasional_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author detra
 * 
 */
public class WebServiceSQL implements IWebServiceSQL {

    /**
     * Metod getLastAPBDIndex
     * @param conn
     * @return long
     * @throws SQLException
     * berfungis untuk mengambil index terakhir dari tabel apbd
     */
    @Override
    public long getLastAPBDIndex(Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select max(" + IAPBDConstants.ATTR_APBD_INDEX + ") id from "
                    + IAPBDConstants.TABLE_APBD);
            if (rs.next()) {
                return rs.getLong("id");
            }
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     * method getLastKegiatanIndex
     * @param conn
     * @return long
     * @throws SQLException
     * berfungsi untuk mengambil index terakhir dari tabel kegiatanapbd
     */
    @Override
    public long getLastKegiatanIndex(Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select max(" + IAPBDConstants.ATTR_KEGIATAN_INDEX + ") id from "
                    + IAPBDConstants.TABLE_KEGIATAN_APBD);
            if (rs.next()) {
                return rs.getLong("id");
            }
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method cekStatusDataAPBD
     * @param kodeSatker
     * @param tahun
     * @param kodeData
     * @param jenisCOA
     * @param conn
     * @return short
     * @throws SQLException
     * berfungsi untuk memeriksa status data apbd berdasarkan kodesatket, tahun, kodedata, dan jenisCOA
     */
    @Override
    public short cekStatusDataAPBD(String kodeSatker, short tahun, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select statusdata from " + IAPBDConstants.TABLE_APBD + " where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + " = ? and "
                    + IAPBDConstants.ATTR_KODE_DATA + "=? and "
                    + IAPBDConstants.ATTR_JENIS_COA + "= ? ";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, tahun);
            stm.setShort(3, kodeData);
            stm.setShort(4, jenisCOA);
            
            rs = stm.executeQuery();
            short status = 0;
            if (rs.next()) {
                status = rs.getShort("statusdata");
            }
            return status;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method getApbdInfo
     * @param kodeSatker
     * @param tahun
     * @param kodeData
     * @param jenisCOA
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengambil informasi apbd berdasarkan kodesatker, tahun, kodedata, dan jenisCOA
     */
    @Override
    public String getApbdInfo(String kodeSatker, short tahun, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select * from " + IAPBDConstants.TABLE_APBD + " where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "='" + kodeSatker + "' and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + " = "+tahun+" and "
                    + IAPBDConstants.ATTR_KODE_DATA + "=" + kodeData + " and "
                    + IAPBDConstants.ATTR_JENIS_COA + "= " + jenisCOA + " ";
            stm = conn.createStatement();
            
            rs = stm.executeQuery(sql);
            String result = "";
            if (rs.next()) {
                result = "Informasi APBD Pemerintah ";
                result+=rs.getString("namapemda")+"\n";
                result+="Tahun Anggaran " + rs.getShort("tahunanggaran")+"\n \n";
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                ParsePosition pos = new ParsePosition(0);
                String tgl = "";
                if(rs.getDate("tglpengiriman") != null) tgl = sdf.format(rs.getDate("tglpengiriman"));
                
                result+="Jenis Data " + (rs.getShort("kodedata")==0?"Apbd Murni":"Apbd Perubahan") + "\n";
                result+="Jenis COA " + (rs.getShort("jeniscoa")==2?"PMDN 64":"PMDN 13") + "\n";
                result+="Tanggal Pengiriman "  + tgl + "\n";
                result+="Nama Aplikasi " + rs.getString("namaaplikasi") + "\n";
                result+="Pengembang Aplikasi " + rs.getString("pengembangaplikasi") + "\n";
                String status = "Belum Final";
                short stat = rs.getShort("statusdata");
                if(stat==1) status="Siap Verifikasi";
                else if (stat==2) status = "Proses Verifikasi";
                else if(stat==3) status = "Selesai Verifikasi";
                result+="Status Data " + status;
            }
            return result;
        } catch (SQLException ex) {
            System.out.println("ex "  + ex.getMessage());
//            ex.printStackTrace();
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method getAPBD
     * @param kodeSatker
     * @param tahun
     * @param kodeData
     * @param jenisCOA
     * @param userName
     * @param pass
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengambil data APBD berdasarkan kodeSatker, tahun, kodeData, jenisCOA, username, password
     */
    @Override
    public Apbd_WS getApbd(String kodeSatker, short tahun, short kodeData, short jenisCOA, String userName, String pass, Connection conn) throws SQLException{
        PreparedStatement stm = null, stmA = null;
        ResultSet rs = null, rsA = null;
        Apbd_WS result = new Apbd_WS();
        String sql = "select * from apbd where kodesatker=? and tahunanggaran=? and kodedata=? and jeniscoa=? ";
        try{
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker);
            stm.setShort(2, tahun);
            stm.setShort(3, kodeData);
            stm.setShort(4, jenisCOA);
            rs = stm.executeQuery();
            long idApbd = 0;
            if( rs.next() ){
                idApbd = rs.getLong("apbdindex");
                result = new Apbd_WS(kodeSatker, rs.getString("kodepemda"), rs.getString("namapemda"), tahun, kodeData, jenisCOA, rs.getShort("statusdata"), 
                        rs.getString("nomorperda"), rs.getDate("tanggalperda")==null?null:new java.util.Date(rs.getDate("tanggalperda").getTime()), userName, pass, rs.getString("namaaplikasi"), rs.getString("pengembangaplikasi"));
            }
            stm = conn.prepareStatement("select k.* from kegiatanapbd k  where apbdindex=?");
            stm.setLong(1, idApbd);
            rs = stm.executeQuery();
            List<KegiatanAPBD_WS> kegs = new ArrayList();
            while(rs.next()){
                long kegId = rs.getLong("kegiatanindex");
                stmA = conn.prepareStatement("select * from koderekapbd where kegiatanindex=?");
                stmA.setLong(1, kegId);
                rsA = stmA.executeQuery();
                List<KodeRekeningAPBD_WS> akuns = new ArrayList();
                while(rsA.next()){
                    akuns.add(new KodeRekeningAPBD_WS(rsA.getString("kodeakunutama"), rsA.getString("namaakunutama"), rsA.getString("kodeakunkelompok"), rsA.getString("namaakunkelompok"), rsA.getString("kodeakunjenis"), rsA.getString("namaakunjenis"), rsA.getString("kodeakunobjek"), rsA.getString("namaakunobjek"), rsA.getString("kodeakunrincian"), rsA.getString("namaakunrincian"), rsA.getString("kodeakunsub"), rsA.getString("namaakunsub"), rsA.getDouble("nilaianggaran")));
                }
                kegs.add(new KegiatanAPBD_WS(rs.getString("kodeurusanprogram"), rs.getString("namaurusanprogram"), rs.getString("kodeurusanpelaksana"), rs.getString("namaurusanpelaksana"), rs.getString("kodeskpd"), rs.getString("namaskpd"), rs.getString("kodeprogram"), rs.getString("namaprogram"), rs.getString("kodekegiatan"), rs.getString("namakegiatan"), rs.getString("kodeFungsi"), rs.getString("namafungsi")));
                KodeRekeningAPBD_WS[] ss = new KodeRekeningAPBD_WS[akuns.size()];
                kegs.get(kegs.size()-1).setKodeRekenings(akuns.toArray(ss));
            }
            result.setKegiatans(kegs);
            return result;
        } catch (SQLException ex){            
            throw new SQLException(ex.getMessage());
        } catch (SIKDServiceException ex) {             
            Logger.getLogger(WebServiceSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException(ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( rsA!=null ) rsA.close();
            if( stm!=null ) stm.close();
            if( stmA!=null ) stmA.close();
        }
        
    }

    /**
     * method createAPBD
     * @param apbd
     * @param iotype
     * @param conn
     * @return long
     * @throws SQLException
     * berfungsi untuk insert data APBD
     */
    @Override
    public long createAPBD(Apbd_WS apbd, short iotype, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {            
            String sql = "with zz as ( ";
            sql = sql +
                    "insert into " + IAPBDConstants.TABLE_APBD + "("
                    + IAPBDConstants.ATTR_KODE_SATKER + ", "
                    + IAPBDConstants.ATTR_KODE_PEMDA + ", "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + ", "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + ", "
                    + IAPBDConstants.ATTR_TANGGAL_PENGIRIMAN + ", "
                    + IAPBDConstants.ATTR_KODE_DATA 
                    + ", statusdata,jeniscoa,nomorperda,tanggalperda,namaaplikasi,pengembangaplikasi, iotype ) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            sql = sql + " returning * ) select max(apbdindex) as index from zz";            
            stm = conn.prepareStatement(sql);
            stm.setString(1, apbd.getKodeSatker().trim());
            stm.setString(2, apbd.getKodePemda().trim());
            stm.setString(3, apbd.getNamaPemda().trim());
            stm.setShort(4, apbd.getTahunAnggaran());
//            java.sql.Date da = new java.sql.Date(new java.util.Date().getTime());
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            stm.setTimestamp(5, time);
            stm.setShort(6, apbd.getKodeData());
            stm.setShort(7, apbd.getStatusData());
            stm.setShort(8, apbd.getJenisCOA());
            stm.setString(9, apbd.getNomorPerda());
            if(apbd.getTanggalPerda()!=null)
            stm.setDate(10, new java.sql.Date(apbd.getTanggalPerda().getTime()));
            else stm.setNull(10, Types.DATE);
            stm.setString(11, apbd.getNamaAplikasi());
            stm.setString(12, apbd.getPengembangAplikasi());
            stm.setShort(13, iotype);
            rs = stm.executeQuery();
            long index = 0;
            if( rs.next()){
                index = rs.getLong("index");
            }
//            stm.executeUpdate();
            return index;//"Sukses Create APBD";
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } catch (SIKDServiceException ex) {
            Logger.getLogger(WebServiceSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method updateAPBD
     * @param apbd
     * @param apbdIndex
     * @param conn
     * @return String
     * @throws SQLException
     * berfungsi untuk melakukan update data APBD berdasarkan index dari apbd
     */
    @Override
    public String updateAPBD(Apbd_WS apbd, long apbdIndex, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "update " + IAPBDConstants.TABLE_APBD + " set "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=?, "
                    + IAPBDConstants.ATTR_KODE_PEMDA + "=?, "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + "=?, "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=?, "
                    + IAPBDConstants.ATTR_TANGGAL_PENGIRIMAN + "=?, "
                    + IAPBDConstants.ATTR_KODE_DATA + "=?, statusdata= ?, "
                    + IAPBDConstants.ATTR_JENIS_COA + "=?, nomorperda= ?, tanggalperda=?, namaaplikasi=?, pengembangaplikasi=? "
                    + " WHERE "
                    + IAPBDConstants.ATTR_APBD_INDEX + "=?";
            
            stm = conn.prepareStatement(sql);
            stm.setString(1, apbd.getKodeSatker().trim());
            stm.setString(2, apbd.getKodePemda().trim());
            stm.setString(3, apbd.getNamaPemda().trim());
            stm.setShort(4, apbd.getTahunAnggaran());
//            java.sql.Date da = new java.sql.Date(new java.util.Date().getTime());
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            stm.setTimestamp(5, time);
            stm.setShort(6, apbd.getKodeData());
            stm.setShort(7, apbd.getStatusData());
            stm.setShort(8, apbd.getJenisCOA());
            stm.setString(9, apbd.getNomorPerda());
            if( apbd.getTanggalPerda() != null )
            stm.setDate(10, new java.sql.Date(apbd.getTanggalPerda().getTime()));
            else stm.setNull(10, Types.DATE);
            stm.setString(11, apbd.getNamaAplikasi());
            stm.setString(12, apbd.getPengembangAplikasi());
            stm.setLong(13, apbdIndex);
            stm.executeUpdate();
            return "Sukses Update APBD";
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } catch (SIKDServiceException ex) {
            Logger.getLogger(WebServiceSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     * method deleteAPBD
     * @param kodeSatker
     * @param kodePemda
     * @param thn
     * @param kodeData
     * @param jenisCOA
     * @param conn
     * @throws SQLException
     * berfungis untuk menghapus data APBD berdasarkan kodeSatker, kodePemda, tahun, kodeData, dan jenisCOA
     */
    @Override
    public void deleteAPBD(String kodeSatker, String kodePemda, short thn, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from " + IAPBDConstants.TABLE_APBD + " where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_KODE_PEMDA + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? and "
                    + IAPBDConstants.ATTR_KODE_DATA + " =? and "
                    + IAPBDConstants.ATTR_JENIS_COA + "= ? ";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setString(2, kodePemda.trim());
            stm.setShort(3, thn);
            stm.setShort(4, kodeData);
            stm.setShort(5, jenisCOA);
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     * method getAPBDIndex
     * @param kodeSatker
     * @param kodePemda
     * @param thn
     * @param kodeData
     * @param kodeSkpd
     * @param jenisCOA
     * @param conn
     * @return long
     * @throws SQLException
     * berfungsi untuk mengambil index dari APBD berdasarkan kodeDatker, kodePemda, tahun, kodeData, kodeSkpd, dan jenisCOA
     */
    @Override
    public long getAPBDIndex(String kodeSatker, String kodePemda, short thn, short kodeData, String kodeSkpd, short jenisCOA, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {            
            String sql = "select " + IAPBDConstants.ATTR_APBD_INDEX + " from " + IAPBDConstants.TABLE_APBD + " where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_KODE_PEMDA + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? and "
                    + IAPBDConstants.ATTR_KODE_DATA + " =? and "
                    + IAPBDConstants.ATTR_JENIS_COA + " =? and "
                    + IAPBDConstants.ATTR_APBD_INDEX + " in ( select "
                    + IAPBDConstants.ATTR_APBD_INDEX + " FROM "
                    + IAPBDConstants.TABLE_KEGIATAN_APBD + " where "
                    + IAPBDConstants.ATTR_KODE_SKPD + " =?) ";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setString(2, kodePemda.trim());
            stm.setShort(3, thn);
            stm.setShort(4, kodeData);
            stm.setShort(5, jenisCOA);
            stm.setString(6, kodeSkpd.trim());
            rs = stm.executeQuery();
            long idApbd = 0;
            if( rs.next() ) idApbd = rs.getLong(IAPBDConstants.ATTR_APBD_INDEX);
            return idApbd;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if( rs!= null ) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     * method getAPBDIndex
     * @param kodeSatker
     * @param kodePemda
     * @param thn
     * @param kodeData
     * @param jenisCOA
     * @param conn
     * @return long
     * @throws SQLException
     * berfungsi untuk index dari APBD berdasarkan kodeSatker, kodePemda, tahun, kodeData, jenisCOA
     */
    @Override
    public long getAPBDIndex(String kodeSatker, String kodePemda, short thn, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {            
            String sql = "select " + IAPBDConstants.ATTR_APBD_INDEX + " from " + IAPBDConstants.TABLE_APBD + " where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_KODE_PEMDA + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? and "
                    + IAPBDConstants.ATTR_KODE_DATA + " =? and "
                    + IAPBDConstants.ATTR_JENIS_COA + " =?";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setString(2, kodePemda.trim());
            stm.setShort(3, thn);
            stm.setShort(4, kodeData);            
            stm.setShort(5, jenisCOA);            
            rs = stm.executeQuery();
            long idApbd = 0;
            if( rs.next() ) idApbd = rs.getLong(IAPBDConstants.ATTR_APBD_INDEX);
            return idApbd;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if( rs!= null ) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method craeteKegiatanAPBD
     * @param indexApbd
     * @param kegiatan
     * @param conn
     * @return long
     * @throws SQLException
     * berfungsi untuk insert data ke dalam tabel kegiatanAPBD
     */
    @Override
    public long createKegiatanAPBD(long indexApbd, KegiatanAPBD_WS kegiatan, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
        ResultSet rs = null;
        try {
            String sql = "with zz as ( ";
            sql = sql + 
                    "insert into " + IAPBDConstants.TABLE_KEGIATAN_APBD + "("
                    + IAPBDConstants.ATTR_APBD_INDEX + ", "
                    + IAPBDConstants.ATTR_KODE_URUSAN_PROGRAM + ", "
                    + IAPBDConstants.ATTR_NAMA_URUSAN_PROGRAM + ", "
                    + IAPBDConstants.ATTR_KODE_URUSAN_PELAKSANA + ", "
                    + IAPBDConstants.ATTR_NAMA_URUSAN_PELAKSANA + ", "
                    + IAPBDConstants.ATTR_KODE_SKPD + ", "
                    + IAPBDConstants.ATTR_NAMA_SKPD + ", "
                    + IAPBDConstants.ATTR_KODE_PROGRAM + ", "
                    + IAPBDConstants.ATTR_NAMA_PROGRAM + ", "
                    + IAPBDConstants.ATTR_KODE_KEGIATAN + ", "
                    + IAPBDConstants.ATTR_NAMA_KEGIATAN + ", "
                    + IAPBDConstants.ATTR_KODE_FUNGSI + ", "
                    + IAPBDConstants.ATTR_NAMA_FUNGSI + ") "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            sql = sql + " returning * ) select max(kegiatanindex) as index from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexApbd);
            pstm.setString(2, kegiatan.getKodeUrusanProgram().trim());
            pstm.setString(3, kegiatan.getNamaUrusanProgram().trim());
            pstm.setString(4, kegiatan.getKodeUrusanPelaksana().trim());
            pstm.setString(5, kegiatan.getNamaUrusanPelaksana().trim());
            pstm.setString(6, kegiatan.getKodeSKPD().trim());
            pstm.setString(7, kegiatan.getNamaSKPD().trim());
            pstm.setString(8, kegiatan.getKodeProgram().trim());
            pstm.setString(9, kegiatan.getNamaProgram().trim());
            pstm.setString(10, kegiatan.getKodeKegiatan().trim());
            pstm.setString(11, kegiatan.getNamaKegiatan().trim());
            pstm.setString(12, kegiatan.getKodeFungsi().trim());
            pstm.setString(13, kegiatan.getNamaFungsi().trim());
            rs= pstm.executeQuery();
            long index = 0;
            if(rs.next()) index = rs.getLong("index");
//            pstm.executeUpdate();
            return index;//"Sukses Create Kegiatan APBD";
        } catch (SIKDServiceException | SQLException ex) {
//            ex.printStackTrace();
            throw new SQLException(ex.getMessage());
        }
        finally{
            if(rs!=null) rs.close();
            if( pstm!= null ) pstm.close();
        }
    }
    
    /**
     * method deleteKegiatanAPBD
     * @param indexApbd
     * @param kodePelaksanaProgram
     * @param kodeSKPD
     * @param conn
     * @throws SQLException
     * berfungsi untuk menghapus data kegiatan APBD berdasarkan indexApbd, kodePelaksanaProgram, kodeSKPD
     * menghapus data per kodeskpd
     */
    @Override
    public void deleteKegiatanAPBD(long indexApbd, String kodePelaksanaProgram,  String kodeSKPD, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from " + IAPBDConstants.TABLE_KEGIATAN_APBD + " where "
                    + IAPBDConstants.ATTR_APBD_INDEX + "=? and "
                    + IAPBDConstants.ATTR_KODE_URUSAN_PELAKSANA + " = ? and "
                    + IAPBDConstants.ATTR_KODE_SKPD + " = ? ";
            stm = conn.prepareStatement(sql);
            stm.setLong(1, indexApbd);
            stm.setString(2, kodePelaksanaProgram);
            stm.setString(3, kodeSKPD);
            stm.executeUpdate();
            
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
        finally{
            if( stm!= null ) stm.close();
        }
    }

    /**
     * method createRekeningAPBD
     * @param indexKegiatan
     * @param rekening
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data ke dalam tabel rekeningapbd
     */
    @Override
    public String createRekeningAPBD(long indexKegiatan, KodeRekeningAPBD_WS rekening, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "insert into " + IAPBDConstants.TABLE_KODE_REKENING_APBD + "("
                    + IAPBDConstants.ATTR_KEGIATAN_INDEX + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_UTAMA + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_UTAMA + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_KELOMPOK + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_KELOMPOK + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_JENIS + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_JENIS + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_OBJEK + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_OBJEK + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_RINCIAN + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_RINCIAN + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_SUB + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_SUB + ", "
                    + IAPBDConstants.ATTR_NILAI_ANGGARAN + ") "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);
            stm.setLong(1, indexKegiatan);
            stm.setString(2, rekening.getKodeAkunUtama().trim());
            stm.setString(3, rekening.getNamaAkunUtama().trim());
            stm.setString(4, rekening.getKodeAkunKelompok().trim());
            stm.setString(5, rekening.getNamaAkunKelompok().trim());
            stm.setString(6, rekening.getKodeAkunJenis().trim());
            stm.setString(7, rekening.getNamaAkunJenis().trim());
            stm.setString(8, rekening.getKodeAkunObjek().trim());
            stm.setString(9, rekening.getNamaAkunObjek().trim());
            stm.setString(10, rekening.getKodeAkunRincian().trim());
            stm.setString(11, rekening.getNamaAkunRincian().trim());
            stm.setString(12, rekening.getKodeAkunSub());
            stm.setString(13, rekening.getNamaAkunSub());
            stm.setDouble(14, rekening.getNilaiAnggaran());
            stm.executeUpdate();
            return "Sukses create Rekening APBD";
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     * method getLastRealisasiAPBDIndex
     * @param conn
     * @return long
     * @throws SQLException
     * berfungsi untuk mengambil index terakhir dari table realisasiapbd
     */
    @Override
    public long getLastRealisasiAPBDIndex(Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select max(" + IAPBDConstants.ATTR_APBD_INDEX + ") id from "
                    + IAPBDConstants.TABLE_REALISASI_APBD);
            if (rs.next()) {
                return rs.getLong("id");
            }
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     * method getLastRealisasiKegiatanIndex
     * @param conn
     * @return long
     * @throws SQLException
     * berfungsi untuk mengambil index terakhir dari tabel realisasikegiatanapbd
     */
    @Override
    public long getLastRealisasiKegiatanIndex(Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select max(" + IAPBDConstants.ATTR_KEGIATAN_INDEX + ") id from "
                    + IAPBDConstants.TABLE_REALISASI_KEGIATAN_APBD);
            if (rs.next()) {
                return rs.getLong("id");
            }
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     * method createRealisasiAPBD
     * @param apbd
     * @param iotype
     * @param conn
     * @return long
     * @throws SQLException
     * berfungsi untuk insert data realisasi apbd
     */
    @Override
    public long createRealisasiAPBD(RealisasiAPBD_WS apbd, short iotype, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {       
            String sql = 
                    "with zz as( ";
            sql = sql + " insert into " + IAPBDConstants.TABLE_REALISASI_APBD + "("
                    + IAPBDConstants.ATTR_KODE_SATKER + ", "
                    + IAPBDConstants.ATTR_KODE_PEMDA + ", "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + ", "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + ", "
                    + IAPBDConstants.ATTR_BULAN + ", "
                    + IAPBDConstants.ATTR_TANGGAL_PENGIRIMAN + ", "
                    + IAPBDConstants.ATTR_KODE_DATA +", "
                    + IAPBDConstants.ATTR_STATUS_DATA +", "
                    + IAPBDConstants.ATTR_JENIS_COA +", "
                    + IAPBDConstants.ATTR_NOMOR_PERDA +", "
                    + IAPBDConstants.ATTR_TANGGAL_PERDA +", "
                    + IAPBDConstants.ATTR_NAMA_APLIKASI +", "
                    + IAPBDConstants.ATTR_PENGEMBANG_APLIKASI +", iotype "
                    + ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            sql= sql + " returning * ) select max(apbdindex) as index from zz";
            stm = conn.prepareStatement(sql);
            stm.setString(1, apbd.getKodeSatker().trim());
            stm.setString(2, apbd.getKodePemda().trim());
            stm.setString(3, apbd.getNamaPemda().trim());
            stm.setShort(4, apbd.getTahunAnggaran());
            stm.setShort(5, apbd.getPeriode());
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            stm.setTimestamp(6, time);
            stm.setShort(7, apbd.getKodeData());
            stm.setShort(8, apbd.getStatusData());
            stm.setShort(9, apbd.getJenisCOA());
            stm.setString(10, apbd.getNomorPerda());
            if( apbd.getTanggalPerda() != null )
                stm.setDate(11, new Date(apbd.getTanggalPerda().getTime()));
            else stm.setNull(11, Types.DATE);
            stm.setString(12, apbd.getNamaAplikasi());
            stm.setString(13, apbd.getPengembangAplikasi());
            stm.setShort(14, iotype);
            rs = stm.executeQuery();
            long index = 0;
            if(rs.next()){
                index = rs.getLong("index");
            }
//            stm.executeUpdate();
//            System.out.println("new index " + index);
//            long index = getLastRealisasiAPBDIndex(conn);
//            System.out.println("selesai insert lra");
            return index;
        } catch (SIKDServiceException | SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method updateRealisasiAPBD
     * @param indexLRA
     * @param apbd
     * @param conn
     * @throws SQLException
     * berfungsi untuk melakukan perubahan data ke dalam tabel realisasi apbd
     */
    @Override
    public void updateRealisasiAPBD(long indexLRA, RealisasiAPBD_WS apbd, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {       
            String sql = " update " + IAPBDConstants.TABLE_REALISASI_APBD + " set "
                    + IAPBDConstants.ATTR_TANGGAL_PENGIRIMAN + " = ? , "
                    + IAPBDConstants.ATTR_STATUS_DATA +" = ? , "
                    + IAPBDConstants.ATTR_NOMOR_PERDA +"= ? , "
                    + IAPBDConstants.ATTR_TANGGAL_PERDA +"= ? "
                    + " where apbdindex = ?";
            stm = conn.prepareStatement(sql);
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            stm.setTimestamp(1, time);
            stm.setShort(2, apbd.getStatusData());
            stm.setString(3, apbd.getNomorPerda());
            if( apbd.getTanggalPerda() != null )
                stm.setDate(4, new Date(apbd.getTanggalPerda().getTime()));
            else stm.setNull(4, Types.DATE);
            stm.setLong(5, indexLRA);
            stm.executeUpdate();
            
        } catch (SIKDServiceException | SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method cekStatusDataRealisasiAPBD
     * @param kodeSatker
     * @param tahun
     * @param kodeData
     * @param periode
     * @param jenisCOA
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengambil status data dari realisasi apbd berdasarkan kodeSatker, tahun, kodeData, periode, jenisCOA
     */
    @Override
    public short cekStatusDataRealisasiAPBD(String kodeSatker, short tahun, short kodeData, short periode, short jenisCOA, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select statusdata from " + IAPBDConstants.TABLE_REALISASI_APBD + " WHERE "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? and "
                    + IAPBDConstants.ATTR_BULAN + "=? and "
                    + IAPBDConstants.ATTR_KODE_DATA + "=? and "
                    + IAPBDConstants.ATTR_JENIS_COA + "= ? ";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, tahun);
            stm.setShort(3, periode);
            stm.setShort(4, kodeData);
            stm.setShort(5, jenisCOA);
            rs = stm.executeQuery();
            short status = 0;
            if (rs.next()) {
                status = rs.getShort("statusdata");
            }
            return status;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method getLRAIndo
     * @param kodeSatker
     * @param tahun
     * @param periode
     * @param kodeData
     * @param jenisCOA
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengambil informasi data Realisasi APBD berdasarkan kodeSatket, tahun, periode, kodeData, jenisCOA
     */
    @Override
    public String getLRAInfo(String kodeSatker, short tahun, short periode, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select * from " + IAPBDConstants.TABLE_REALISASI_APBD + " WHERE "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? and "
                    + IAPBDConstants.ATTR_BULAN + "=? and "
                    + IAPBDConstants.ATTR_KODE_DATA + "=? and "
                    + IAPBDConstants.ATTR_JENIS_COA + "= ? ";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, tahun);
            stm.setShort(3, periode);
            stm.setShort(4, kodeData);
            stm.setShort(5, jenisCOA);
            
            rs = stm.executeQuery();
            String result = "";
            if (rs.next()) {
                result = "Informasi Realisasi APBD Pemerintah ";
                result+=rs.getString("namapemda") + "\n";
                result+="Tahun Anggaran " + rs.getShort("tahunanggaran")+"\n \n";
                result+="Periode "+ rs.getShort("bulan");
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                ParsePosition pos = new ParsePosition(0);
                String tgl = "";
                if(rs.getDate("tglpengiriman") != null) tgl = sdf.format(rs.getDate("tglpengiriman"));
                
                result+="Jenis Data : " + (rs.getShort("kodedata")==0?"Apbd Murni":"Apbd Perubahan") + "\n";
                result+="Jenis COA : " + (rs.getShort("jeniscoa")==2?"PMDN 64":"PMDN 13") + "\n";
                result+="Tanggal Pengiriman : "  + tgl + "\n";
                result+="Nama Aplikasi : " + rs.getString("namaaplikasi") + "\n";
                result+="Pengembang Aplikasi : " + rs.getString("pengembangaplikasi") + "\n";
                String status = "Belum Final";
                short stat = rs.getShort("statusdata");
                if(stat==1) status="Siap Verifikasi";
                else if (stat==2) status = "Proses Verifikasi";
                else if(stat==3) status = "Selesai Verifikasi";
                result+="Status Data : " + status;
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method getRealisasiApbd
     * @param kodeSatker
     * @param tahun
     * @param bulan
     * @param kodeData
     * @param jenisCOA
     * @param userName
     * @param pass
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengambil data realisasi apbd berdasarkan kodeSatker, tahun, bulan, kodeData, jenisCOA
     */
    public RealisasiAPBD_WS getRealisasiApbd(String kodeSatker, short tahun, short bulan, short kodeData, short jenisCOA, String userName, String pass, Connection conn) throws SQLException{
        PreparedStatement stm = null, stmA = null;
        ResultSet rs = null, rsA = null;
        RealisasiAPBD_WS result = new RealisasiAPBD_WS();
        String sql = "select * from realisasiapbd where kodesatker=? and tahunanggaran=? and bulan=? and kodedata=? and jeniscoa=? ";
        try{
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker);
            stm.setShort(2, tahun);
            stm.setShort(3, bulan);
            stm.setShort(4, kodeData);
            stm.setShort(5, jenisCOA);
            rs = stm.executeQuery();
            long idApbd = 0;
            if( rs.next() ){
                idApbd = rs.getLong("apbdindex");
                result = new RealisasiAPBD_WS(kodeSatker, rs.getString("kodepemda"), rs.getString("namapemda"), tahun, bulan, kodeData, jenisCOA, rs.getShort("statusdata"), 
                        rs.getString("nomorperda"), rs.getDate("tanggalperda")==null?null:new java.util.Date(rs.getDate("tanggalperda").getTime()), userName, pass, rs.getString("namaaplikasi"), rs.getString("pengembangaplikasi"));
            }
            stm = conn.prepareStatement("select k.* from realisasikegiatanapbd k  where apbdindex=?");
            stm.setLong(1, idApbd);
            rs = stm.executeQuery();
            List<RealisasiKegiatanAPBD_WS> kegs = new ArrayList();
            while(rs.next()){
                long kegId = rs.getLong("kegiatanindex");
                stmA = conn.prepareStatement("select * from realisasikoderekapbd where kegiatanindex=?");
                stmA.setLong(1, kegId);
                rsA = stmA.executeQuery();
                List<RealisasiKodeRekeningAPBD_WS> akuns = new ArrayList();
                while(rsA.next()){
                    akuns.add(new RealisasiKodeRekeningAPBD_WS(rsA.getString("kodeakunutama"), rsA.getString("namaakunutama"), rsA.getString("kodeakunkelompok"), rsA.getString("namaakunkelompok"), rsA.getString("kodeakunjenis"), rsA.getString("namaakunjenis"), rsA.getString("kodeakunobjek"), rsA.getString("namaakunobjek"), rsA.getString("kodeakunrincian"), rsA.getString("namaakunrincian"), rsA.getString("kodeakunsub"), rsA.getString("namaakunsub"), rsA.getDouble("nilaianggaran")));
                }
                kegs.add(new RealisasiKegiatanAPBD_WS(rs.getString("kodeurusanprogram"), rs.getString("namaurusanprogram"), rs.getString("kodeurusanpelaksana"), rs.getString("namaurusanpelaksana"), rs.getString("kodeskpd"), rs.getString("namaskpd"), rs.getString("kodeprogram"), rs.getString("namaprogram"), rs.getString("kodekegiatan"), rs.getString("namakegiatan"), rs.getString("kodeFungsi"), rs.getString("namafungsi")));
                kegs.get(kegs.size()-1).setKodeRekenings(akuns);
            }
            result.setKegiatans(kegs);
            return result;
        } catch (SQLException ex){            
            throw new SQLException(ex.getMessage());
        } catch (SIKDServiceException ex) {             
            Logger.getLogger(WebServiceSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException(ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( rsA!=null ) rsA.close();
            if( stm!=null ) stm.close();
            if( stmA!=null ) stmA.close();
        }
        
    }
    
    /**
     * method updateRealisasiAPBDPerPeriode
     * @param apbd
     * @param indexApbd
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk merubah data realisasi apbd per periode
     */
    @Override
    public String updateRealisasiAPBDPerPeriode(RealisasiAPBD_WS apbd, long indexApbd, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "update " + IAPBDConstants.TABLE_REALISASI_APBD + " set "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=?, "
                    + IAPBDConstants.ATTR_KODE_PEMDA + "=?, "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + "=?, "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=?, "
                    + IAPBDConstants.ATTR_BULAN + "=?, "
                    + IAPBDConstants.ATTR_TANGGAL_PENGIRIMAN + "=?, "
                    + IAPBDConstants.ATTR_KODE_DATA + "=?, "
                    + "statusdata=?, jeniscoa=?, " 
                    + "nomorperda=?, " 
                    + "tanggalperda=?, " 
                    + "namaaplikasi=?, " 
                    + "pengembangaplikasi=? "
                    + "where "
                    + IAPBDConstants.ATTR_APBD_INDEX + " =?";
            stm = conn.prepareStatement(sql);
            stm.setString(1, apbd.getKodeSatker().trim());
            stm.setString(2, apbd.getKodePemda().trim());
            stm.setString(3, apbd.getNamaPemda().trim());
            stm.setShort(4, apbd.getTahunAnggaran());
            stm.setShort(5, apbd.getPeriode());
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            stm.setTimestamp(6, time);
            stm.setShort(7, apbd.getKodeData());
            stm.setShort(8, apbd.getStatusData());
            stm.setShort(9, apbd.getJenisCOA());
            stm.setString(10, apbd.getNomorPerda());
            if( apbd.getTanggalPerda() != null )
            stm.setDate(11, new java.sql.Date(apbd.getTanggalPerda().getTime()));
            else stm.setNull(11, Types.DATE);
            stm.setString(12, apbd.getNamaAplikasi());
            stm.setString(13, apbd.getPengembangAplikasi());            
            stm.setLong(14, indexApbd);
            stm.executeUpdate();
            return "Sukses Update Realisasi APBD";
        } catch (SIKDServiceException | SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method deleteRealisasiAPBD
     * @param kodeSatker
     * @param kodePemda
     * @param thn
     * @param kodeData
     * @param jenisCOA
     * @param conn
     * @throws SQLException
     * berfungsi untuk menghapus data realisasi APBD berdasarkan kodeSatker, kodePemda, tahun, kodeData, jenisCOA
     */
    @Override
    public void deleteRealisasiAPBD(String kodeSatker, String kodePemda, short thn, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from " + IAPBDConstants.TABLE_REALISASI_APBD + " where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_KODE_PEMDA + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? and "                    
                    + IAPBDConstants.ATTR_KODE_DATA + " =? and "
                    + IAPBDConstants.ATTR_JENIS_COA + "= ? ";
            
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setString(2, kodePemda.trim());
            stm.setShort(3, thn);            
            stm.setShort(4, kodeData);
            stm.setShort(5, jenisCOA);
            stm.executeUpdate();
            
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     * method deleteRealisasiAPBDPerPeriode
     * @param kodeSatker
     * @param kodePemda
     * @param thn
     * @param periode
     * @param kodeData
     * @param jenisCOA
     * @param conn
     * @throws SQLException
     * berfungsi untuk menghapus data Realisasi APBD per periode berdasarkan kodeSatker, kodePemda, tahun, periode, kodeData, jenisCOA
     */
    @Override
    public void deleteRealisasiAPBDPerPeriode(String kodeSatker, String kodePemda, short thn, short periode, short kodeData, short jenisCOA,Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from " + IAPBDConstants.TABLE_REALISASI_APBD + " where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_KODE_PEMDA + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? and "                   
                    + IAPBDConstants.ATTR_BULAN + "=? and "
                    + IAPBDConstants.ATTR_KODE_DATA + " =? and "
                    + IAPBDConstants.ATTR_JENIS_COA + "= ? ";            
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setString(2, kodePemda.trim());
            stm.setShort(3, thn);
            stm.setShort(4, periode);
            stm.setShort(5, kodeData);
            stm.setShort(6, jenisCOA);
            stm.executeUpdate();            
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     * method deleteRealisasiAPBDPerPeriode
     * @param kodeSatker
     * @param kodePemda
     * @param thn
     * @param periode
     * @param jenisCOA
     * @param conn
     * @throws SQLException
     * berfungsi untuk menghapus data realisasi apbd per periode berdasarkan kodeSatker, kodePemda, tahun, periode, jenisCOA
     */
    @Override
    public void deleteRealisasiAPBDPerPeriode(String kodeSatker, String kodePemda, short thn, short periode, short jenisCOA,Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from " + IAPBDConstants.TABLE_REALISASI_APBD + " where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_KODE_PEMDA + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? and "                   
                    + IAPBDConstants.ATTR_BULAN + "=? and "                    
                    + IAPBDConstants.ATTR_JENIS_COA + "= ? ";            
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setString(2, kodePemda.trim());
            stm.setShort(3, thn);
            stm.setShort(4, periode);
            stm.setShort(5, jenisCOA);
            stm.executeUpdate();            
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     * method getRealisasiAPBDIndexPerPeriode
     * @param kodeSatker
     * @param kodePemda
     * @param thn
     * @param periode
     * @param kodeData
     * @param kodeSkpd
     * @param jenisCOA
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengambil index realisasi apbd per periode berdasarkan kodeSatker, kodePEmda, tahun, periode, kodeData, kodeSKPD, jenisCOA
     */
    @Override
    public long getRealisasiAPBDIndexPerPeriode(String kodeSatker, String kodePemda, short thn, short periode, short kodeData, String kodeSkpd, short jenisCOA, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {            
            String sql = "select " + IAPBDConstants.ATTR_APBD_INDEX + " from " + IAPBDConstants.TABLE_REALISASI_APBD + " where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_KODE_PEMDA + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? and "
                    + IAPBDConstants.ATTR_BULAN + "=? and "
                    + IAPBDConstants.ATTR_KODE_DATA + " =? and "
                    + IAPBDConstants.ATTR_JENIS_COA + " =? and "
                    + IAPBDConstants.ATTR_APBD_INDEX + " in ( select "
                    + IAPBDConstants.ATTR_APBD_INDEX + " FROM "
                    + IAPBDConstants.TABLE_REALISASI_KEGIATAN_APBD + " where "
                    + IAPBDConstants.ATTR_KODE_SKPD + " =?) ";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setString(2, kodePemda.trim());
            stm.setShort(3, thn);
            stm.setShort(4, periode);
            stm.setShort(5, kodeData);
            stm.setShort(6, jenisCOA);
            stm.setString(7, kodeSkpd.trim());
            rs = stm.executeQuery();
            long idApbd = 0;
            if( rs.next() ) idApbd = rs.getLong(IAPBDConstants.ATTR_APBD_INDEX);
            return idApbd;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if( rs!= null ) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method getRealisasiAPBDIndex
     * @param kodeSatker
     * @param kodePemda
     * @param thn
     * @param kodeData
     * @param jenisCOA
     * @param conn
     * @return long
     * @throws SQLException
     * berfungsi untuk mengambil index realisasi apbd berdasarkan kodeSatker, kodePemda, tahun, kodeData, jenisCOA
     */
    @Override
    public long getRealisasiAPBDIndex(String kodeSatker, String kodePemda, short thn, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {            
            String sql = "select " + IAPBDConstants.ATTR_APBD_INDEX + " from " + IAPBDConstants.TABLE_REALISASI_APBD + " where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_KODE_PEMDA + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? and "
                    + IAPBDConstants.ATTR_KODE_DATA + " =? and "
                    + IAPBDConstants.ATTR_JENIS_COA + " =?";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setString(2, kodePemda.trim());
            stm.setShort(3, thn);
            stm.setShort(4, kodeData);
            stm.setShort(5, jenisCOA);
            rs = stm.executeQuery();
            long idApbd = 0;
            if( rs.next() ) idApbd = rs.getLong(IAPBDConstants.ATTR_APBD_INDEX);
            return idApbd;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if( rs!= null ) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method createRealisasiKegiatanAPBD
     * @param indexApbd
     * @param kegiatan
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data ke dalam table realisasikegiatanapbd
     */
    @Override
    public long createRealisasiKegiatanAPBD(long indexApbd, RealisasiKegiatanAPBD_WS kegiatan, Connection conn) throws SQLException {
        PreparedStatement stm=null;
        ResultSet rs = null;
        try {
            String sql = 
                    "with rkegs as ( ";
            sql = sql + " insert into " + IAPBDConstants.TABLE_REALISASI_KEGIATAN_APBD + "("
                    + IAPBDConstants.ATTR_APBD_INDEX + ", "
                    + IAPBDConstants.ATTR_KODE_URUSAN_PROGRAM + ", "
                    + IAPBDConstants.ATTR_NAMA_URUSAN_PROGRAM + ", "
                    + IAPBDConstants.ATTR_KODE_URUSAN_PELAKSANA + ", "
                    + IAPBDConstants.ATTR_NAMA_URUSAN_PELAKSANA + ", "
                    + IAPBDConstants.ATTR_KODE_SKPD + ", "
                    + IAPBDConstants.ATTR_NAMA_SKPD + ", "
                    + IAPBDConstants.ATTR_KODE_PROGRAM + ", "
                    + IAPBDConstants.ATTR_NAMA_PROGRAM + ", "
                    + IAPBDConstants.ATTR_KODE_KEGIATAN + ", "
                    + IAPBDConstants.ATTR_NAMA_KEGIATAN + ", "
                    + IAPBDConstants.ATTR_KODE_FUNGSI + ", "
                    + IAPBDConstants.ATTR_NAMA_FUNGSI + ") "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            sql = sql + " returning * ) select max(kegiatanindex) as index from rkegs";
            stm = conn.prepareStatement(sql);
            stm.setLong(1, indexApbd);
            stm.setString(2, kegiatan.getKodeUrusanProgram());
            stm.setString(3, kegiatan.getNamaUrusanProgram());
            stm.setString(4, kegiatan.getKodeUrusanPelaksana());
            stm.setString(5, kegiatan.getNamaUrusanPelaksana());
            stm.setString(6, kegiatan.getKodeSKPD());
            stm.setString(7, kegiatan.getNamaSKPD());
            stm.setString(8, kegiatan.getKodeProgram());
            stm.setString(9, kegiatan.getNamaProgram());
            stm.setString(10, kegiatan.getKodeKegiatan());
            stm.setString(11, kegiatan.getNamaKegiatan());
            stm.setString(12, kegiatan.getKodeFungsi());
            stm.setString(13, kegiatan.getNamaFungsi());
            rs= stm.executeQuery();
            long index1 = 0;
            if(rs.next()){
                index1 = rs.getLong("index");
            }
//            stm.executeUpdate();
            
//            long index1=getLastRealisasiKegiatanIndex(conn);
//            System.out.println("selesai insert kegiatan lra");
            return index1;//"Sukses Create Realisasi Kegiatan APBD";
        } catch (SIKDServiceException | SQLException ex) {
            throw new SQLException("SQL: Gagal Create Realisaasi Kegiatan APBD " + ex.getMessage());
        }
        finally{
            if(rs!=null) rs.close();
            if(stm!=null) stm.close();

        }
    }

    /**
     * method createRealisasiRekeningAPBD
     * @param indexKegiatan
     * @param rekening
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data ke dalam tabel realisasirekeningapbd
     */
    @Override
    public String createRealisasiRekeningAPBD(long indexKegiatan, RealisasiKodeRekeningAPBD_WS rekening, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "insert into " + IAPBDConstants.TABLE_REALISASI_KODE_REKENING_APBD + "("
                    + IAPBDConstants.ATTR_KEGIATAN_INDEX + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_UTAMA + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_UTAMA + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_KELOMPOK + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_KELOMPOK + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_JENIS + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_JENIS + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_OBJEK + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_OBJEK + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_RINCIAN + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_RINCIAN + ", "
                    + IAPBDConstants.ATTR_KODE_AKUN_SUB + ", "
                    + IAPBDConstants.ATTR_NAMA_AKUN_SUB + ", "
                    + IAPBDConstants.ATTR_NILAI_ANGGARAN + ") "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);
            stm.setLong(1, indexKegiatan);
            stm.setString(2, rekening.getKodeAkunUtama().trim());
            stm.setString(3, rekening.getNamaAkunUtama().trim());
            stm.setString(4, rekening.getKodeAkunKelompok().trim());
            stm.setString(5, rekening.getNamaAkunKelompok().trim());
            stm.setString(6, rekening.getKodeAkunJenis().trim());
            stm.setString(7, rekening.getNamaAkunJenis().trim());
            stm.setString(8, rekening.getKodeAkunObjek().trim());
            stm.setString(9, rekening.getNamaAkunObjek().trim());
            stm.setString(10, rekening.getKodeAkunRincian().trim());
            stm.setString(11, rekening.getNamaAkunRincian().trim());
            stm.setString(12, rekening.getKodeAkunSub());
            stm.setString(13, rekening.getNamaAkunSub());
            stm.setDouble(14, rekening.getNilaiAnggaran());
            stm.executeUpdate();
//            System.out.println("selesai insert rekening lra");
            return "Sukses create Realisasi Rekening APBD";
        } catch (SQLException ex) {
            throw new SQLException("SQL: Gagal Create Realisaasi Rekening APBD " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method cekStatusDataPerhitunganFihakKetiga
     * @param kodeSatker
     * @param tahun
     * @param conn
     * @return
     * @throws SQLException
     * berfungis untuk mengambil status data dari Perhitungan Fihak Ketiga
     */
    @Override
    public short cekStatusDataPerhitunganFihakKetiga(String kodeSatker, short tahun, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select statusdata from pfk where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + " = ? ";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, tahun);
            
            rs = stm.executeQuery();
            short status = 0;
            if (rs.next()) {
                status = rs.getShort("statusdata");
            }
            return status;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method getPFKInfo
     * @param kodeSatker
     * @param tahun
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengambil informasi tentang Perhitungan Fihak Ketiga
     */
    @Override
    public String getPFKInfo(String kodeSatker, short tahun, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select * from pfk WHERE "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? " ;
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, tahun);
            
            rs = stm.executeQuery();
            String result = "";
            if (rs.next()) {
                result = "Informasi Perhitungan Pihak Ketiga Pemerintah ";
                result+=rs.getString("namapemda") + "\n";
                result+="Tahun Anggaran " + rs.getShort("tahunanggaran")+"\n \n";
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                ParsePosition pos = new ParsePosition(0);
                String tgl = "";
                if(rs.getDate("tanggalpengiriman") != null) tgl = sdf.format(rs.getDate("tanggalpengiriman"));
                
                result+="Tanggal Pengiriman : "  + tgl + "\n";
                result+="Nama Aplikasi : " + rs.getString("namaaplikasi") + "\n";
                result+="Pengembang Aplikasi : " + rs.getString("pengembangaplikasi") + "\n";
                String status = "Belum Final";
                short stat = rs.getShort("statusdata");
                if(stat==1) status="Siap Verifikasi";
                else if (stat==2) status = "Proses Verifikasi";
                else if(stat==3) status = "Selesai Verifikasi";
                result+="Status Data : " + status;
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *method createPerhitunganFihakKetiga
     * @param pfk
     * @param iotype
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data Perhitungan Fihak Ketiga
     */
    @Override
    public long createPerhitunganFihakKetiga(PerhitunganFihakKetiga_WS pfk, short iotype, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql +
                    "insert into pfk("
                    + IAPBDConstants.ATTR_KODE_SATKER + ", "
                    + IAPBDConstants.ATTR_KODE_PEMDA + ", "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + ", "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN +  ", "
                    + " tanggalpengiriman, statusdata, namaaplikasi, pengembangaplikasi, iotype) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) ";
            sql = sql + " returning * ) select max(pfkindex) as index from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, pfk.getKodeSatker().trim());
            pstm.setString(2, pfk.getKodePemda().trim());
            pstm.setString(3, pfk.getNamaPemda().trim());
            pstm.setShort(4, pfk.getTahunAnggaran());
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            pstm.setTimestamp(5, time);
            pstm.setShort(6, pfk.getStatusData());
            pstm.setString(7, pfk.getNamaAplikasi());
            pstm.setString(8, pfk.getPengembangAplikasi());
            pstm.setShort(9, iotype);
            rs = pstm.executeQuery();
            if( rs.next() ) return rs.getLong("index");
//            pstm.executeUpdate();
//            stm = conn.createStatement();
//            rs = stm.executeQuery("select max(pfkindex) id from pfk");
//            if( rs.next() ) return rs.getLong("id");
            return 0;
        } catch (SIKDServiceException | SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
            if (pstm != null) { pstm.close(); }
//            if (stm != null) { stm.close(); }
        }
    }
    
    /**
     * method createRincianPerhitunganFihakKetiga
     * @param pfkIndex
     * @param rincianpfk
     * @param conn
     * @throws SQLException
     * berfungsi untuk insert data rincian Perhitungan Fihak Ketiga
     */
    @Override
    public void createRincianPerhitunganFihakKetiga(long pfkIndex, RincianPerhitunganFihakKetiga_WS rincianpfk, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
//        ResultSet rs = null;
        try {
            String sql = "insert into rincianpfk VALUES (?, ?, ?, ?)";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, pfkIndex);
            pstm.setString(2, rincianpfk.getUraian().trim());
            pstm.setDouble(3, rincianpfk.getPungutan());
            pstm.setDouble(4, rincianpfk.getSetoran());
            pstm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
//            if (rs != null) { rs.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method getPerhitunganFihakKetiga
     * @param kodeSatker
     * @param tahunAnggaran
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengambil index dari PFK
     */
    @Override
    public long getPerhitunganFihakKetigaIndex(String kodeSatker, short tahunAnggaran, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select pfkindex id from pfk where kodesatker='" 
                    + kodeSatker + "' and tahunanggaran=" + tahunAnggaran);
            if( rs.next() ) return rs.getLong("id");
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
            if (stm != null) { stm.close(); }
        }
    }
    
    /**
     * method deletePerhitunganFihakKetiga
     * @param kodeSatker
     * @param year
     * @param conn
     * @throws SQLException
     * berfungsi untuk menghapus data PFK
     */
    @Override
    public void deletePerhitunganFihakKetiga(String kodeSatker, short year, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
//        Statement stm = null;
//        ResultSet rs = null;
        try {
            String sql = "delete from pfk where "
                    + IAPBDConstants.ATTR_KODE_SATKER + " = ? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + " = ?";
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, kodeSatker.trim());
            pstm.setShort(2, year);
            pstm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
//            if (rs != null) { rs.close(); }
            if (pstm != null) { pstm.close(); }
//            if (stm != null) { stm.close(); }
        }
    }
    
    /**
     * method cekStatusDataPinjamanDaerah
     * @param kodeSatker
     * @param tahun
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengambil status data Pinjaman daerah
     */
    @Override
    public short cekStatusDataPinjamanDaerah(String kodeSatker, short tahun, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select statusdata from pinjamandaerah where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + " = ? ";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, tahun);
            
            rs = stm.executeQuery();
            short status = 0;
            if (rs.next()) {
                status = rs.getShort("statusdata");
            }
            return status;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null)  rs .close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method gePinjamanDaerahInfo
     * @param kodeSatker
     * @param tahun
     * @param conn
     * @return String
     * @throws SQLException
     * berfungsi untuk mengambil informasi Pinjaman Daerah
     */
    @Override
    public String getPinjamanDaerahInfo(String kodeSatker, short tahun, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select * from pinjamandaerah WHERE "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? " ;
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, tahun);
            
            rs = stm.executeQuery();
            String result = "";
            if (rs.next()) {
                result = "Informasi Pinjaman Daerah Pemerintah ";
                result+=rs.getString("namapemda") + "\n";
                result+="Tahun Anggaran " + rs.getShort("tahunanggaran")+"\n \n";
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                ParsePosition pos = new ParsePosition(0);
                String tgl = "";
                if(rs.getDate("tanggalpengiriman") != null) tgl = sdf.format(rs.getDate("tanggalpengiriman"));
                
                result+="Tanggal Pengiriman : "  + tgl + "\n";
                result+="Nama Aplikasi : " + rs.getString("namaaplikasi") + "\n";
                result+="Pengembang Aplikasi : " + rs.getString("pengembangaplikasi") + "\n";
                String status = "Belum Final";
                short stat = rs.getShort("statusdata");
                if(stat==1) status="Siap Verifikasi";
                else if (stat==2) status = "Proses Verifikasi";
                else if(stat==3) status = "Selesai Verifikasi";
                result+="Status Data : " + status;
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * createPinjamanDaerah
     * @param pinjaman
     * @param iotype
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data Pinjaman Daerah
     */
    @Override
    public long createPinjamanDaerah(PinjamanDaerah_WS pinjaman, short iotype, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as( ";
            sql = sql +
                    "insert into pinjamandaerah("
                    + IAPBDConstants.ATTR_KODE_SATKER + ", "
                    + IAPBDConstants.ATTR_KODE_PEMDA + ", "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + ", "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + ", "
                    + " tanggalpengiriman, statusdata, namaaplikasi, pengembangaplikasi, iotype) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            sql = sql + " returning * ) select max (pinjamanindex) as index from zz";
            stm = conn.prepareStatement(sql);
            stm.setString(1, pinjaman.getKodeSatker().trim());
            stm.setString(2, pinjaman.getKodePemda().trim());
            stm.setString(3, pinjaman.getNamaPemda().trim());
            stm.setShort(4, pinjaman.getTahunAnggaran());
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            stm.setTimestamp(5, time);
            stm.setShort(6, pinjaman.getStatusData());
            stm.setString(7, pinjaman.getNamaAplikasi());
            stm.setString(8, pinjaman.getPengembangAplikasi());
            stm.setShort(9, iotype);
//            stm.executeUpdate();
            rs = stm.executeQuery();
            if(rs.next()) return rs.getLong("index");
            return 0;
//            return "Sukses Create Pinjaman Daerah";
        } catch (SIKDServiceException | SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method updatePinjamanDaerah
     * @param pinjaman
     * @param pinjamanIndex
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengubah data PinjamanDaerah
     */
    @Override
    public String updatePinjamanDaerah(PinjamanDaerah_WS pinjaman, long pinjamanIndex, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "update pinjamandaerah set "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=?, "
                    + IAPBDConstants.ATTR_KODE_PEMDA + "=?, "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + "=?, "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "= ? WHERE "
                    + " pinjamanindex=?";
            stm = conn.prepareStatement(sql);
            stm.setString(1, pinjaman.getKodeSatker().trim());
            stm.setString(2, pinjaman.getKodePemda().trim());
            stm.setString(3, pinjaman.getNamaPemda().trim());
            stm.setShort(4, pinjaman.getTahunAnggaran());
            stm.executeUpdate();
            return "Sukses Update Pinjaman Daerah";
        } catch (SIKDServiceException | SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     * method deletePinjamanDaerah
     * @param kodeSatker
     * @param thn
     * @param conn
     * @throws SQLException
     * berfungsi untuk menghapus data Pinjaman Daerah
     */
    @Override
    public void deletePinjamanDaerah(String kodeSatker, short thn, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from pinjamandaerah where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? ";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, thn);
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     * methid getPinjamanDaerahIndex
     * @param kodeSatker
     * @param thn
     * @param conn
     * @return long
     * @throws SQLException
     * berfungsi untuk mengambil index dari Pinjaman Daerah
     */
    @Override
    public long getPinjamanDaerahIndex(String kodeSatker, short thn, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {            
            String sql = "select pinjamanindex from pinjamandaerah where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=?";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, thn);
            rs = stm.executeQuery();
            long id = 0;
            if( rs.next() ) id = rs.getLong("pinjamanindex");
            return id;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if( rs!= null ) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method getLastPinjamanDaerahIndex
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengambil index terakhir data PinjamanDaerah
     */
    @Override
    public long getLastPinjamanDaerahIndex(Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {            
            String sql = "select max(pinjamanindex) id from pinjamandaerah";
            stm = conn.createStatement();
            rs = stm.executeQuery(sql);
            long id = 0;
            if( rs.next() ) id = rs.getLong("id");
            return id;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if( rs!= null ) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method craeteRincianPinjamanDaerah
     * @param rincian
     * @param indexPinjaman
     * @param conn
     * @return String
     * @throws SQLException
     * berfungsi untuk insert data rincian Pinjaman Daerah
     */
    @Override
    public String createRincianPinjamanDaerah(RincianPinjamanDaerah_WS rincian, long indexPinjaman, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "insert into rincianpinjamandaerah VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);
            stm.setLong(1, indexPinjaman);
            stm.setString(2, rincian.getSumber().trim());
            stm.setString(3, rincian.getDasarHukum().trim());
            if( rincian.getTanggalPerjanjian() != null ) stm.setDate(4, new java.sql.Date(rincian.getTanggalPerjanjian().getTime()));
            else stm.setNull(4, Types.DATE);
            stm.setDouble(5, rincian.getJumlahPinjaman());
            stm.setDouble(6, rincian.getJangkaWaktu());
            stm.setDouble(7, rincian.getBunga());
            stm.setString(8, rincian.getTujuan().trim());
            stm.setDouble(9, rincian.getBayarPokok());
            stm.setDouble(10, rincian.getBayarBunga());
            stm.setDouble(11, rincian.getSisaPokok());
            stm.setDouble(12, rincian.getSisaBunga());
            stm.executeUpdate();
            return "Sukses Create Rincian Pinjaman Daerah";
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     * method deleteRincianPinjamanDaerah
     * @param kodeSatker
     * @param thn
     * @param conn
     * @throws SQLException
     * berfungsi untuk menghapus data Pinjaman Daerah
     */
    @Override
    public void deleteRincianPinjamanDaerah(String kodeSatker, short thn, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from rincianpinjamandaerah where pinjamanindex in("
                    + "select pinjamanindex from pinjamandaerah where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=?)";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, thn);
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *method cekStatusDataNeraca
     * @param kodeSatker
     * @param tahun
     * @param semester
     * @param judulNeraca
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengambil status data neraca
     */
    @Override
    public short cekStatusDataNeraca(String kodeSatker, short tahun, short semester, String judulNeraca, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select statusdata from neraca where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + " = ? and "
                    + " semester = ? and "
                    + " judulneraca=?";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, tahun);
            stm.setShort(3, semester);
            stm.setString(4, judulNeraca);
            
            rs = stm.executeQuery();
            short status = 0;
            if (rs.next()) {
                status = rs.getShort("statusdata");
            }
            return status;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method getNeracaInfo
     * @param kodeSatker
     * @param tahun
     * @param semester
     * @param judulNeraca
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengambil informasi neraca
     */
    @Override
    public String getNeracaInfo(String kodeSatker, short tahun, short semester, String judulNeraca, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select * from neraca WHERE "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? "
                    + " and semester= ?"
                    + "and judulneraca=?" ;
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, tahun);
            stm.setShort(3, semester);
            stm.setString(4, judulNeraca);
            
            rs = stm.executeQuery();
            String result = "";
            if (rs.next()) {
                result = "Informasi Neraca Pemerintah ";
                result+=rs.getString("namapemda") + "\n";
                result+="Tahun Anggaran " + rs.getShort("tahunanggaran")+"\n";
                result+="Semester " + rs.getShort("semester")+"\n \n";
                result+="Judul Neraca : " + rs.getString("judulneraca")+"\n ";
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                ParsePosition pos = new ParsePosition(0);
                String tgl = "";
                if(rs.getDate("tanggalpengiriman") != null) tgl = sdf.format(rs.getDate("tanggalpengiriman"));
                
                result+="Tanggal Pengiriman : "  + tgl + "\n";
                result+="Nama Aplikasi : " + rs.getString("namaaplikasi") + "\n";
                result+="Pengembang Aplikasi : " + rs.getString("pengembangaplikasi") + "\n";
                String status = "Belum Final";
                short stat = rs.getShort("statusdata");
                if(stat==1) status="Siap Verifikasi";
                else if (stat==2) status = "Proses Verifikasi";
                else if(stat==3) status = "Selesai Verifikasi";
                result+="Status Data : " + status;
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method createNeraca
     * @param neraca
     * @param iotype
     * @param conn
     * @return
     * @throws SQLException
     * 
     *berfungsi untuk insert data neraca
      */
    @Override
    public long createNeraca(Neraca_WS neraca, short iotype, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql +
                    "insert into neraca ("
                    + IAPBDConstants.ATTR_KODE_SATKER + ", "
                    + IAPBDConstants.ATTR_KODE_PEMDA + ", "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + ", "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + ", "
                    + "semester, judulneraca, tanggalpengiriman, statusdata, namaaplikasi, pengembangaplikasi, iotype"
                    + ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            sql = sql + " returning * ) select max(neracaindex) as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, neraca.getKodeSatker().trim());
            pstm.setString(2, neraca.getKodePemda().trim());
            pstm.setString(3, neraca.getNamaPemda().trim());
            pstm.setShort(4, neraca.getTahunAnggaran());
            pstm.setShort(5, neraca.getSemester());
            pstm.setString(6, neraca.getJudulNeraca().trim());
             Timestamp time = new Timestamp(new java.util.Date().getTime());
            pstm.setTimestamp(7, time);
            pstm.setShort(8, neraca.getStatusData());
            pstm.setString(9, neraca.getNamaAplikasi());
            pstm.setString(10, neraca.getPengembangAplikasi());
            pstm.setShort(11, iotype);
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
//                    stm.executeQuery("select max(neracaindex) id from neraca");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SIKDServiceException | SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method getNeracaIndex
     * @param kodeSatker
     * @param year
     * @param semester
     * @param judulNeraca
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengambil index dari  neraca
     */
    @Override
    public long getNeracaIndex(String kodeSatker, short year, short semester, String judulNeraca, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            
            stm = conn.createStatement();
            rs = stm.executeQuery("select neracaindex id from neraca where kodesatker='" + kodeSatker +"' and tahunanggaran =" + year 
                    +" and semester =" + semester 
                    + " and judulNeraca='"+judulNeraca+"'");
            if(rs.next()) return rs.getLong("id");
            return 0; 
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
            if (stm != null) { stm.close(); }
        }
    }
    
    /**
     * method deleteNeraca
     * @param kodeSatker
     * @param thn
     * @param semester
     * @param judulNeraca
     * @param conn
     * @throws SQLException
     * berfungsi untuk menghapus data neraca
     */
    @Override
    public void deleteNeraca(String kodeSatker, short thn, short semester, String judulNeraca, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from neraca where kodesatker = ? and tahunanggaran=? and semester=? and judulneraca = ?";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, thn);
            stm.setShort(3, semester);
            stm.setString(4, judulNeraca.trim());
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }    

    /**
     * method createNeracaAkunUtama
     * @param indexNeraca
     * @param akunUtama
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data akun utama neraca
     */
    @Override
    public long createNeracaAkunUtama(long indexNeraca, NeracaAkunUtama_WS akunUtama, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql +
                    " insert into neracakodrekutama (neracaindex, kodeakunutama, namaakunutama, nilai)"
                    + " VALUES (?, ?, ?, ?)";
            sql = sql + " returning * ) select max(akunutamaindex) as id from zz"; 
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexNeraca);
            pstm.setString(2, akunUtama.getKodeAkun().trim());
            pstm.setString(3, akunUtama.getNamaAkun().trim());
            pstm.setDouble(4, akunUtama.getNilai());
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
                    //stm.executeQuery("select max(akunutamaindex) id from neracakodrekutama");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
            
        }
    }

    /**
     * method createNeracaAkunKelompok
     * @param indexAkunUtama
     * @param akunKelompok
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data akun kelompok neraca
     */
    @Override
    public long createNeracaAkunKelompok(long indexAkunUtama, NeracaAkunKelompok_WS akunKelompok, Connection conn) throws SQLException {        
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql +
                    " insert into neracakodrekkelompok (kodrekutamaindex, kodeakunkelompok, namaakunkelompok, nilai)"
                    + " VALUES (?, ?, ?, ?)";
            sql = sql + " returning * ) select max(akunkelompokindex) as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexAkunUtama);
            pstm.setString(2, akunKelompok.getKodeAkun().trim());
            pstm.setString(3, akunKelompok.getNamaAkun().trim());
            pstm.setDouble(4, akunKelompok.getNilai());
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
//                    stm.executeQuery("select max(akunkelompokindex) id from neracakodrekkelompok");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * createNeracaAkunJenis
     * @param indexAkunKelompok
     * @param akunJenis
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data neraca akun jenis
     */
    @Override
    public long createNeracaAkunJenis(long indexAkunKelompok, NeracaAkunJenis_WS akunJenis, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql +
                    " insert into neracakodrekjenis (kodrekkelompokindex, kodeakunjenis, namaakunjenis, nilai)"
                    + " VALUES (?, ?, ?, ?)";
            sql = sql + " returning * ) select max(akunjenisindex) as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexAkunKelompok);
            pstm.setString(2, akunJenis.getKodeAkun().trim());
            pstm.setString(3, akunJenis.getNamaAkun().trim());
            pstm.setDouble(4, akunJenis.getNilai());
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
//                    stm.executeQuery("select max(akunjenisindex) id from neracakodrekjenis");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method createNeracaAkunObjek
     * @param indexAkunJenis
     * @param akunObjek
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data neraca akun objek
     */
    @Override
    public long createNeracaAkunObjek(long indexAkunJenis, NeracaAkunObjek_WS akunObjek, Connection conn) throws SQLException {        
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql +
                    " insert into neracakodrekobjek (kodrekjenisindex, kodeakunobjek, namaakunobjek, nilai)"
                    + " VALUES (?, ?, ?, ?)";
            sql = sql + " returning * ) select max(akunobjekindex) as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexAkunJenis);
            pstm.setString(2, akunObjek.getKodeAkun().trim());
            pstm.setString(3, akunObjek.getNamaAkun().trim());
            pstm.setDouble(4, akunObjek.getNilai());
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
//                    stm.executeQuery("select max(akunobjekindex) id from neracakodrekobjek");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method cekStatusDataArusKas
     * @param kodeSatker
     * @param tahun
     * @param judulArusKas
     * @param conn
     * @return
     * @throws SQLException
     * berfungis untuk mengambil data status arus kas
     */
    @Override
    public short cekStatusDataArusKas(String kodeSatker, short tahun, String judulArusKas, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select statusdata from aruskas where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + " = ? and "
                    + " judularuskas=?";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, tahun);
            stm.setString(3, judulArusKas);
            
            rs = stm.executeQuery();
            short status = 0;
            if (rs.next()) {
                status = rs.getShort("statusdata");
            }
            return status;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method createArusKas
     * @param arusKas
     * @param iotype
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data arus kas
     */
    @Override
    public long createArusKas(ArusKas_WS arusKas, short iotype, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql +
                    "insert into aruskas ("
                    + IAPBDConstants.ATTR_KODE_SATKER + ", "
                    + IAPBDConstants.ATTR_KODE_PEMDA + ", "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + ", "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + ", "
                    + "judularuskas, tglpengiriman, statusdata, namaaplikasi, pengembangaplikasi, iotype"
                    + ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            sql = sql + " returning * ) select max(aruskasindex) as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, arusKas.getKodeSatker().trim());
            pstm.setString(2, arusKas.getKodePemda().trim());
            pstm.setString(3, arusKas.getNamaPemda().trim());
            pstm.setShort(4, arusKas.getTahunAnggaran());
            pstm.setString(5, arusKas.getJudulArusKas().trim());
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            pstm.setTimestamp(6, time);
            pstm.setShort(7, arusKas.getStatusData());
            pstm.setString(8, arusKas.getNamaAplikasi());
            pstm.setString(9, arusKas.getPengembangAplikasi());
            pstm.setShort(10, iotype);
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
//                    stm.executeQuery("select max(aruskasindex) id from aruskas");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SIKDServiceException | SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method getArusKasInfo
     * @param kodeSatker
     * @param tahun
     * @param judulArusKas
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengambil informasi arus kas
     */
    @Override
    public String getArusKasInfo(String kodeSatker, short tahun, String judulArusKas, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select * from aruskas WHERE "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? "
                    + "and judularuskas=?" ;
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, tahun);
            stm.setString(3, judulArusKas);
            
            rs = stm.executeQuery();
            String result = "";
            if (rs.next()) {
                result = "Informasi Arus Kas Pemerintah ";
                result+=rs.getString("namapemda")+"\n";
                result+="Tahun Anggaran " + rs.getShort("tahunanggaran")+"\n \n";
                result+="Judul Arus Kas : " + rs.getString("judularuskas")+"\n ";
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                ParsePosition pos = new ParsePosition(0);
                String tgl = "";
                if(rs.getDate("tglpengiriman") != null) tgl = sdf.format(rs.getDate("tglpengiriman"));
                
                result+="Tanggal Pengiriman : "  + tgl + "\n";
                result+="Nama Aplikasi : " + rs.getString("namaaplikasi") + "\n";
                result+="Pengembang Aplikasi : " + rs.getString("pengembangaplikasi") + "\n";
                String status = "Belum Final";
                short stat = rs.getShort("statusdata");
                if(stat==1) status="Siap Verifikasi";
                else if (stat==2) status = "Proses Verifikasi";
                else if(stat==3) status = "Selesai Verifikasi";
                result+="Status Data : " + status;
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null) rs.next();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method getArusKasIndex
     * @param kodeSatker
     * @param year
     * @param judulArusKas
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengambil index arus kas
     */
    @Override
    public long getArusKasIndex(String kodeSatker, short year, String judulArusKas, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            
            stm = conn.createStatement();
            rs = stm.executeQuery("select aruskasindex id from aruskas where kodesatker='" + kodeSatker +"' and tahunanggaran =" + year + " and judularuskas='"+judulArusKas+"'");
            if(rs.next()) return rs.getLong("id");
            return 0; 
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
            if (stm != null) { stm.close(); }
        }
    }
    
    /**
     * method deleteArusKas
     * @param kodeSatker
     * @param thn
     * @param judulArusKas
     * @param conn
     * @throws SQLException
     * berfungsi untuk menghapus data arus kas
     */
    @Override
    public void deleteArusKas(String kodeSatker, short thn, String judulArusKas, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from aruskas where kodesatker = ? and tahunanggaran=? and judularuskas = ?";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, thn);
            stm.setString(3, judulArusKas.trim());
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    /**
     * method createArusKasSaldo
     * @param indexArusKas
     * @param arusKasSaldo
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data arus kas saldo
     */
    @Override
    public long createArusKasSaldo(long indexArusKas, ArusKasSaldo_WS arusKasSaldo, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql +
                    "insert into aruskassaldo (aruskasindex, kasbudawal, kasbudakhir, "
                    + "kasbendpengeluaranawal, kasbendpenerimaanawal, kaslainnya) "
                    + "VALUES (?, ?, ?, ?, ?, ?)";
            sql = sql + " returning * ) select max(saldoindex) as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexArusKas);
            pstm.setDouble(2, arusKasSaldo.getKasBUDAwal());
            pstm.setDouble(3, arusKasSaldo.getKasBUDAkhir());
            pstm.setDouble(4, arusKasSaldo.getKasBendaharaPengeluaranAwal());
            pstm.setDouble(5, arusKasSaldo.getKasBendaharaPenerimaanAwal());
            pstm.setDouble(6, arusKasSaldo.getKasLainnya());
            
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
//                    stm.executeQuery("select max(saldoindex) id from aruskassaldo");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method createArusKeluarInvestasi
     * @param indexArusKas
     * @param arusKeluarInvestasi
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data Arus kas Keluar investasi
     */
    @Override
    public long createArusKeluarInvestasi(long indexArusKas, ArusKeluarInvestasi_WS arusKeluarInvestasi, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql +
                    " insert into aruskeluarinvestasi (aruskasindex, kodeakun, namaakun, nilai)"
                    + " VALUES (?, ?, ?, ?)";
            sql = sql + " returning * ) select max(keluarinvestasiindex) as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexArusKas);
            pstm.setString(2, arusKeluarInvestasi.getKodeAkun().trim());
            pstm.setString(3, arusKeluarInvestasi.getNamaAkun().trim());
            pstm.setDouble(4, arusKeluarInvestasi.getNilai());
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
//                    stm.executeQuery("select max(keluarinvestasiindex) id from aruskeluarinvestasi");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method createArusKeluarNonAnggaran
     * @param indexArusKas
     * @param arusKeluar
     * @param conn
     * @return long
     * @throws SQLException
     * berfungsi untuk inesrt data arus keluar non anggaran
     */
    @Override
    public long createArusKeluarNonAnggaran(long indexArusKas, ArusKeluarNonAnggaran_WS arusKeluar, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql +
                    "insert into aruskeluarnonanggaran (aruskasindex, kodeakun, namaakun, nilai)"
                    + " VALUES (?, ?, ?, ?)";
            sql = sql + " returning * ) select max(keluarnonanggaranindex) as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexArusKas);
            pstm.setString(2, arusKeluar.getKodeAkun().trim());
            pstm.setString(3, arusKeluar.getNamaAkun().trim());
            pstm.setDouble(4, arusKeluar.getNilai());
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
                    //stm.executeQuery("select max(keluarnonanggaranindex) id from aruskeluarnonanggaran");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method createArusKeluarOperasi
     * @param indexArusKas
     * @param arusKeluar
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data arus keluar operasi
     */
    @Override
    public long createArusKeluarOperasi(long indexArusKas, ArusKeluarOperasi_WS arusKeluar, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql +
                    "insert into aruskeluaroperasi (aruskasindex, kodeakun, namaakun, nilai)"
                    + " VALUES (?, ?, ?, ?)";
            sql = sql + " returning * ) select max(keluaroperasiindex) as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexArusKas);
            pstm.setString(2, arusKeluar.getKodeAkun().trim());
            pstm.setString(3, arusKeluar.getNamaAkun().trim());
            pstm.setDouble(4, arusKeluar.getNilai());
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
//                    stm.executeQuery("select max(keluaroperasiindex) id from aruskeluaroperasi");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method createArusKeluarPembiayaan
     * @param indexArusKas
     * @param arusKeluar
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data arus keluar pembiayaan
     */
    @Override
    public long createArusKeluarPembiayaan(long indexArusKas, ArusKeluarPembiayaan_WS arusKeluar, Connection conn) throws SQLException {        
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as (";
            sql = sql + 
                    "insert into aruskeluarpembiayaan (aruskasindex, kodeakun, namaakun, nilai)"
                    + " VALUES (?, ?, ?, ?)";
            sql = sql + " returning * ) select max(keluarpembiayaanindex) as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexArusKas);
            pstm.setString(2, arusKeluar.getKodeAkun().trim());
            pstm.setString(3, arusKeluar.getNamaAkun().trim());
            pstm.setDouble(4, arusKeluar.getNilai());
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
//                    stm.executeQuery("select max(keluarpembiayaanindex) id from aruskeluarpembiayaan");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method createArusMasukInvestasi
     * @param indexArusKas
     * @param arusMasuk
     * @param conn
     * @return
     * @throws SQLException
     * berfungis untuk insert data arus masuk investasi
     */
    @Override
    public long createArusMasukInvestasi(long indexArusKas, ArusMasukInvestasi_WS arusMasuk, Connection conn) throws SQLException {        
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql +
                    " insert into arusmasukinvestasi (aruskasindex, kodeakun, namaakun, nilai)"
                    + " VALUES (?, ?, ?, ?)";
            sql = sql + " returning * ) select max(masukinvestasiindex) as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexArusKas);
            pstm.setString(2, arusMasuk.getKodeAkun().trim());
            pstm.setString(3, arusMasuk.getNamaAkun().trim());
            pstm.setDouble(4, arusMasuk.getNilai());
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
//                    stm.executeQuery("select max(masukinvestasiindex) id from arusmasukinvestasi");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method createArusMasukNonAnggaran
     * @param indexArusKas
     * @param arusMasuk
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data arus masuk non anggaran
     */
    @Override
    public long createArusMasukNonAnggaran(long indexArusKas, ArusMasukNonAnggaran_WS arusMasuk, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql =sql +
                    "insert into arusmasuknonanggaran (aruskasindex, kodeakun, namaakun, nilai)"
                    + " VALUES (?, ?, ?, ?)";
            sql = sql + " returning * ) select max(masuknonanggaranindex) as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexArusKas);
            pstm.setString(2, arusMasuk.getKodeAkun().trim());
            pstm.setString(3, arusMasuk.getNamaAkun().trim());
            pstm.setDouble(4, arusMasuk.getNilai());
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
//                    stm.executeQuery("select max(masuknonanggaranindex) id from arusmasuknonanggaran");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method createArusMasukOperasi
     * @param indexArusKas
     * @param arusMasuk
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data arus masuk operasi
     */
    @Override
    public long createArusMasukOperasi(long indexArusKas, ArusMasukOperasi_WS arusMasuk, Connection conn) throws SQLException {        
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql + 
                    "insert into arusmasukoperasi (aruskasindex, kodeakun, namaakun, nilai)"
                    + " VALUES (?, ?, ?, ?)";
            sql = sql + " returning * ) select max(masukoperasiindex) as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexArusKas);
            pstm.setString(2, arusMasuk.getKodeAkun().trim());
            pstm.setString(3, arusMasuk.getNamaAkun().trim());
            pstm.setDouble(4, arusMasuk.getNilai());
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
//                    stm.executeQuery("select max(masukoperasiindex) id from arusmasukoperasi");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method createArusMasukPembiayaan
     * @param indexArusKas
     * @param arusMasuk
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data arus masuk pembiayaan
     */
    @Override
    public long createArusMasukPembiayaan(long indexArusKas, ArusMasukPembiayaan_WS arusMasuk, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql +
                    " insert into arusmasukpembiayaan (aruskasindex, kodeakun, namaakun, nilai)"
                    + " VALUES (?, ?, ?, ?)";
            sql = sql + " returning * ) select max(masukpembiayaanindex) as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexArusKas);
            pstm.setString(2, arusMasuk.getKodeAkun().trim());
            pstm.setString(3, arusMasuk.getNamaAkun().trim());
            pstm.setDouble(4, arusMasuk.getNilai());
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
                    
//                    stm.executeQuery("select max(masukpembiayaanindex) id from arusmasukpembiayaan");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method cekStatusDataLaporanOperasional
     * @param kodeSatker
     * @param tahun
     * @param triwulan
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengambil status data LO
     */
    @Override
    public short cekStatusDataLaporanOperasional(String kodeSatker, short tahun, short triwulan, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select statusdata from lo where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + " = ? and "
                    + " triwulan = ?";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, tahun);
            stm.setShort(3, triwulan);
            
            rs = stm.executeQuery();
            short status = 0;
            if (rs.next()) {
                status = rs.getShort("statusdata");
            }
            return status;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method getLOInfo
     * @param kodeSatker
     * @param tahun
     * @param triwulan
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengambil informasi LO
     */
    @Override
    public String getLOInfo(String kodeSatker, short tahun, short triwulan, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select * from lo WHERE "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? and "
                    + " triwulan=? " ;
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, tahun);
            stm.setShort(3, triwulan);
            
            rs = stm.executeQuery();
            String result = "";
            if (rs.next()) {
                result = "Informasi Laporan Operasional Pemerintah ";
                result+=rs.getString("namapemda")+"\n";
                result+="Tahun Anggaran " + rs.getShort("tahunanggaran")+"\n";
                result+="Triwulan " + rs.getShort("triwulan")+"\n \n";
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                ParsePosition pos = new ParsePosition(0);
                String tgl = "";
                if(rs.getDate("tanggalpengiriman") != null) tgl = sdf.format(rs.getDate("tanggalpengiriman"));
                
                result+="Tanggal Pengiriman : "  + tgl + "\n";
                result+="Nama Aplikasi : " + rs.getString("namaaplikasi") + "\n";
                result+="Pengembang Aplikasi : " + rs.getString("pengembangaplikasi") + "\n";
                String status = "Belum Final";
                short stat = rs.getShort("statusdata");
                if(stat==1) status="Siap Verifikasi";
                else if (stat==2) status = "Proses Verifikasi";
                else if(stat==3) status = "Selesai Verifikasi";
                result+="Status Data : " + status;
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method createLaporanOperasional
     * @param obj
     * @param iotype
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data LO
     */
    @Override
    public long createLaporanOperasional(LaporanOperasional_WS obj, short iotype, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql +
                    "insert into lo ("
                    + IAPBDConstants.ATTR_KODE_SATKER + ", "
                    + IAPBDConstants.ATTR_KODE_PEMDA + ", "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + ", "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + ", triwulan, tanggalpengiriman, statusdata, namaaplikasi, pengembangaplikasi, iotype"
                    + ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            sql = sql + " returning * ) select max(loindex) as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, obj.getKodeSatker().trim());
            pstm.setString(2, obj.getKodePemda().trim());
            pstm.setString(3, obj.getNamaPemda().trim());
            pstm.setShort(4, obj.getTahunAnggaran());
            pstm.setShort(5, obj.getTriwulan());
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            pstm.setTimestamp(6, time);
            pstm.setShort(7, obj.getStatusData());
            pstm.setString(8, obj.getNamaAplikasi());
            pstm.setString(9, obj.getPengembangAplikasi());
            pstm.setShort(10, iotype);
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
//                    stm.executeQuery("select max(loindex) id from lo");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SIKDServiceException | SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method createLOAkunUtama
     * @param indexLO
     * @param akunUtama
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data Akun Utama LO
     */
    @Override
    public long createLOAkunUtama(long indexLO, LaporanOperasionalAkunUtama_WS akunUtama, Connection conn) throws SQLException {        
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql + 
                    " insert into opkodrekutama (loindex, kodeakunutama, namaakunutama, nilai)"
                    + " VALUES (?, ?, ?, ?)";
            sql = sql + " returning * ) select max(akunutamaindex) as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexLO);
            pstm.setString(2, akunUtama.getKodeAkun().trim());
            pstm.setString(3, akunUtama.getNamaAkun().trim());
            pstm.setDouble(4, akunUtama.getNilai());
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
//                    stm.executeQuery("select max(akunutamaindex) id from opkodrekutama");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }

    /**
     * method createLOAkunKelompok
     * @param indexAkunUtama
     * @param akunKelompok
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data Akun Kelompok LO
     */
    @Override
    public long createLOAkunKelompok(long indexAkunUtama, LaporanOperasionalAkunKelompok_WS akunKelompok, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql + 
                    "insert into opkodrekkelompok (kodrekutamaindex, kodeakunkelompok, namaakunkelompok, nilai)"
                    + " VALUES (?, ?, ?, ?)";
            sql = sql + " returning * ) select max(akunkelompokindex) as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexAkunUtama);
            pstm.setString(2, akunKelompok.getKodeAkun().trim());
            pstm.setString(3, akunKelompok.getNamaAkun().trim());
            pstm.setDouble(4, akunKelompok.getNilai());
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
//                    stm.executeQuery("select max(akunkelompokindex) id from opkodrekkelompok");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method createLOAkunJenis
     * @param indexAkunKelompok
     * @param akunJenis
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data Akun Jenis LO
     */
    @Override
    public long createLOAkunJenis(long indexAkunKelompok, LaporanOperasionalAkunJenis_WS akunJenis, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql +
                    " insert into opkodrekjenis (kodrekkelompokindex, kodeakunjenis, namaakunjenis, nilai)"
                    + " VALUES (?, ?, ?, ?) ";
            sql = sql + " returning * ) select max(akunjenisindex) as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexAkunKelompok);
            pstm.setString(2, akunJenis.getKodeAkun().trim());
            pstm.setString(3, akunJenis.getNamaAkun().trim());
            pstm.setDouble(4, akunJenis.getNilai());
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
//                    stm.executeQuery("select max(akunjenisindex) id from opkodrekjenis");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method createLOAkunObjek
     * @param indexAkunJenis
     * @param akunObjek
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data Akun Objek LO
     */
    @Override
    public long createLOAkunObjek(long indexAkunJenis, LaporanOperasionalAkunObjek_WS akunObjek, Connection conn) throws SQLException {
        
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql +
                    " insert into opkodrekobjek (kodrekjenisindex, kodeakunobjek, namaakunobjek, nilai)"
                    + " VALUES (?, ?, ?, ?)";
            sql = sql + " returning * ) select max(akunobjekindex) as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexAkunJenis);
            pstm.setString(2, akunObjek.getKodeAkun().trim());
            pstm.setString(3, akunObjek.getNamaAkun().trim());
            pstm.setDouble(4, akunObjek.getNilai());
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
//stm.executeQuery("select max(akunobjekindex) id from opkodrekobjek");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method createSurplusNonOP
     * @param indexLO
     * @param surplus
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data Surplus Non Op
     */
    @Override
    public long createSurplusNonOP(long indexLO, SurplusNonOperasional_WS surplus, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql + 
                    " insert into surplusnonop (loindex, penjualanasetnonlancar, kewajibanjangkapanjang, surpluslainnya)"
                    + " VALUES (?, ?, ?, ?)";
            sql = sql + " returning * ) select max(surplusnonopindex)  as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexLO);
            pstm.setDouble(2, surplus.getPenjualanAsetNonLancar());
            pstm.setDouble(3, surplus.getKewajibanJangkaPanjang());
            pstm.setDouble(4, surplus.getSurplusLainnya());
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
//                    stm.executeQuery("select max(surplusnonopindex) id from surplusnonop");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method createDefisitNonOP
     * @param indexLO
     * @param defisit
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data defisit non op
     */
    @Override
    public long createDefisitNonOP(long indexLO, DefisitNonOperasional_WS defisit, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql + " insert into defisitnonop (loindex, penjualanasetnonlancar, kewajibanjangkapanjang, defisitlainnya)"
                    + " VALUES (?, ?, ?, ?)";
            sql = sql + " returning * ) select max(defisitnonopindex)  as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexLO);
            pstm.setDouble(2, defisit.getPenjualanAsetNonLancar());
            pstm.setDouble(3, defisit.getKewajibanJangkaPanjang());
            pstm.setDouble(4, defisit.getDefisitLainnya());
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
//                    stm.executeQuery("select max(defisitnonopindex) id from defisitnonop");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method createLOPosLuarBiasa
     * @param indexLO
     * @param pos
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data pos luar biasa LO
     */
    @Override
    public long createLOPosLuarBiasa(long indexLO, PosLuarBiasaOperasional_WS pos, Connection conn) throws SQLException {
        
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql + " insert into posluarbiasaop (loindex, pendapatan, beban)"
                    + " VALUES (?, ?, ?)";
            sql = sql + " returning * ) select max(posluarbiasaindex)  as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexLO);
            pstm.setDouble(2, pos.getPendapatan());
            pstm.setDouble(3, pos.getBeban());
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
//                    stm.executeQuery("select max(posluarbiasaindex) id from posluarbiasaop");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method getLOIndex
     * @param kodeSatker
     * @param year
     * @param triwulan
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengambil index LO
     */
    @Override
    public long getLOIndex(String kodeSatker, short year, short triwulan, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            
            stm = conn.createStatement();
            rs = stm.executeQuery("select loindex id from lo where kodesatker='" + kodeSatker +"' and tahunanggaran =" + year 
            + " and triwulan =" + triwulan );
            if(rs.next()) return rs.getLong("id");
            return 0; 
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
            if (stm != null) { stm.close(); }
        }
    }
    
    /**
     * method deleteLO
     * @param kodeSatker
     * @param thn
     * @param triwulan
     * @param conn
     * @throws SQLException
     * berfungsi untuk menghapus data LO
     */
    @Override
    public void deleteLO(String kodeSatker, short thn, short triwulan, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from lo where kodesatker = ? and tahunanggaran=? and triwulan=?";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, thn);
            stm.setShort(3, triwulan);
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method cekStatusDataPerubahanSal
     * @param kodeSatker
     * @param tahun
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengambil status data LPSal
     */
    @Override
    public short cekStatusDataPerubahanSal(String kodeSatker, short tahun, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select statusdata from perubahansal where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + " = ? ";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, tahun);
            
            rs = stm.executeQuery();
            short status = 0;
            if (rs.next()) {
                status = rs.getShort("statusdata");
            }
            return status;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method getPerubahanSalInfo
     * @param kodeSatker
     * @param tahun
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengambil informasi LPSal
     */
    @Override
    public String getPerubahanSalInfo(String kodeSatker, short tahun, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select * from perubahansal WHERE "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? " ;
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, tahun);
            
            rs = stm.executeQuery();
            String result = "";
            if (rs.next()) {
                result = "Informasi Perubahan Sal Pemerintah ";
                result+=rs.getString("namapemda") + "\n";
                result+="Tahun Anggaran " + rs.getShort("tahunanggaran")+"\n \n";
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                ParsePosition pos = new ParsePosition(0);
                String tgl = "";
                if(rs.getDate("tanggalpengiriman") != null) tgl = sdf.format(rs.getDate("tanggalpengiriman"));
                
                result+="Tanggal Pengiriman : "  + tgl + "\n";
                result+="Nama Aplikasi : " + rs.getString("namaaplikasi") + "\n";
                result+="Pengembang Aplikasi : " + rs.getString("pengembangaplikasi") + "\n";
                String status = "Belum Final";
                short stat = rs.getShort("statusdata");
                if(stat==1) status="Siap Verifikasi";
                else if (stat==2) status = "Proses Verifikasi";
                else if(stat==3) status = "Selesai Verifikasi";
                result+="Status Data : " + status;
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * createPerubahanSal
     * @param obj
     * @param iotype
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data LPSal
     */
    @Override
    public long createPerubahanSal(PerubahanSal_WS obj, short iotype, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql + " insert into perubahansal ("
                    + IAPBDConstants.ATTR_KODE_SATKER + ", "
                    + IAPBDConstants.ATTR_KODE_PEMDA + ", "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + ", "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + ", "
                    + " tanggalpengiriman, statusdata, namaaplikasi, pengembangaplikasi, iotype) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            sql = sql + " returning * ) select max(salindex) as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, obj.getKodeSatker().trim());
            pstm.setString(2, obj.getKodePemda().trim());
            pstm.setString(3, obj.getNamaPemda().trim());
            pstm.setShort(4, obj.getTahunAnggaran());
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            pstm.setTimestamp(5, time);
            pstm.setShort(6, obj.getStatusData());
            pstm.setString(7, obj.getNamaAplikasi());
            pstm.setString(8, obj.getPengembangAplikasi());
            pstm.setShort(9, iotype);
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
//                    stm.executeQuery("select max(salindex) id from perubahansal");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SIKDServiceException | SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method createPerubahanSalDetail
     * @param indexSal
     * @param obj
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data detail LPSal
     */
    @Override
    public String createPerubahanSalDetail(long indexSal, PerubahanSalDetail_WS obj, Connection conn) throws SQLException {
        
        PreparedStatement pstm = null;
        try {
            String sql = "insert into perubahansaldetail VALUES (?, ?, ?, ?, ?, ?)";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexSal);
            pstm.setDouble(2, obj.getSalAwal());
            pstm.setDouble(3, obj.getPenggunaanSal());
            pstm.setDouble(4, obj.getSilpa());
            pstm.setDouble(5, obj.getKoreksi());
            pstm.setDouble(6, obj.getLainLain());
            pstm.executeUpdate();
            
            return "Sukses Membuat Perubahan Sal Detail";
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method deletePerubahanSal
     * @param kodeSatker
     * @param year
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk menghapus data LPSal
     */
    @Override
    public String deletePerubahanSal(String kodeSatker, short year, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
        try {
            String sql = "delete from perubahansal where kodesatker=? and tahunanggaran=?";
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, kodeSatker);
            pstm.setShort(2, year);
            pstm.executeUpdate();
            
            return "Sukses Menghapus Perubahan Sal";
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method cekStatusDataPerubahanEkuitas
     * @param kodeSatker
     * @param tahun
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengambil status data LPE
     */
    @Override
    public short cekStatusDataPerubahanEkuitas(String kodeSatker, short tahun, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select statusdata from perubahanekuitas where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + " = ? ";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, tahun);
            
            rs = stm.executeQuery();
            short status = 0;
            if (rs.next()) {
                status = rs.getShort("statusdata");
            }
            return status;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method getPerubahanEkuitasInfo
     * @param kodeSatker
     * @param tahun
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengambil informasi LPE
     */
    @Override
    public String getPerubahanEkuitasInfo(String kodeSatker, short tahun, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            String sql = "select * from perubahanekuitas WHERE "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? " ;
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, tahun);
            
            rs = stm.executeQuery();
            String result = "";
            if (rs.next()) {
                result = "Informasi Perubahan Ekuitas Pemerintah ";
                result+=rs.getString("namapemda") + "\n";
                result+="Tahun Anggaran " + rs.getShort("tahunanggaran")+"\n \n";
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                ParsePosition pos = new ParsePosition(0);
                String tgl = "";
                if(rs.getDate("tanggalpengiriman") != null) tgl = sdf.format(rs.getDate("tanggalpengiriman"));
                
                result+="Tanggal Pengiriman : "  + tgl + "\n";
                result+="Nama Aplikasi : " + rs.getString("namaaplikasi") + "\n";
                result+="Pengembang Aplikasi : " + rs.getString("pengembangaplikasi") + "\n";
                String status = "Belum Final";
                short stat = rs.getShort("statusdata");
                if(stat==1) status="Siap Verifikasi";
                else if (stat==2) status = "Proses Verifikasi";
                else if(stat==3) status = "Selesai Verifikasi";
                result+="Status Data : " + status;
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method createPerubahanEkuitas
     * @param obj
     * @param iotype
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data LPE
     */
    @Override
    public long createPerubahanEkuitas(PerubahanEkuitas_WS obj, short iotype, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
//        Statement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql + " insert into perubahanekuitas ("
                    + IAPBDConstants.ATTR_KODE_SATKER + ", "
                    + IAPBDConstants.ATTR_KODE_PEMDA + ", "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + ", "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + ", "
                    + " tanggalpengiriman, statusdata, namaaplikasi, pengembangaplikasi, iotype) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            sql = sql + " returning * ) select max(ekuitasindex) as id from zz";
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, obj.getKodeSatker().trim());
            pstm.setString(2, obj.getKodePemda().trim());
            pstm.setString(3, obj.getNamaPemda().trim());
            pstm.setShort(4, obj.getTahunAnggaran());
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            pstm.setTimestamp(5, time);
            pstm.setShort(6, obj.getStatusData());
            pstm.setString(7, obj.getNamaAplikasi());
            pstm.setString(8, obj.getPengembangAplikasi());
            pstm.setShort(9, iotype);
//            pstm.executeUpdate();
            
//            stm = conn.createStatement();
            rs = pstm.executeQuery();
//                    stm.executeQuery("select max(ekuitasindex) id from perubahanekuitas");
            if(rs.next()) return rs.getLong("id");
            return 0;
        } catch (SIKDServiceException | SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) { rs.close(); }
//            if (stm != null) { stm.close(); }
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method createPerubahanEkuitasDetail
     * @param indexEkuitas
     * @param obj
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk insert data detail LPE
     */
    @Override
    public String createPerubahanEkuitasDetail(long indexEkuitas, PerubahanEkuitasDetail_WS obj, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
        try {
            String sql = "insert into perubahanekuitasdetail VALUES (?, ?, ?, ?, ?, ?)";
            pstm = conn.prepareStatement(sql);
            pstm.setLong(1, indexEkuitas);
            pstm.setDouble(2, obj.getEkuitasAwal());
            pstm.setDouble(3, obj.getSurplusDefisitLO());
            pstm.setDouble(4, obj.getKoreksiNilaiPersediaan());
            pstm.setDouble(5, obj.getSelisihRevaluasiAset());
            pstm.setDouble(6, obj.getLainLain());
            pstm.executeUpdate();
            
            return "Sukses Membuat Perubahan Ekuitas Detail";
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method deletePerubahanEkuitas
     * @param kodeSatker
     * @param year
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk menghapus data LPE
     */
    @Override
    public String deletePerubahanEkuitas(String kodeSatker, short year, Connection conn) throws SQLException {
        PreparedStatement pstm = null;
        try {
            String sql = "delete from perubahanekuitas where kodesatker=? and tahunanggaran=?";
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, kodeSatker);
            pstm.setShort(2, year);
            pstm.executeUpdate();
            
            return "Sukses Menghapus Perubahan Ekuitas";
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (pstm != null) { pstm.close(); }
        }
    }
    
    /**
     * method getDinasKirimAPBD
     * @param year
     * @param kodeSatker
     * @param kodeData
     * @param jenisCOA
     * @param conn
     * @return
     * @throws SQLException
     * berfungsi untuk mengambil data skpd yang sudah terkirim apbd
     */
    @Override
    public List<Skpd_WS> getDinasKirimAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select distinct keg.kodeurusanpelaksana, keg.kodeskpd, keg.namaskpd from kegiatanapbd keg "
                    + "inner join apbd on keg.apbdindex=apbd.apbdindex where tahunanggaran="
                    + year + " and kodesatker='" + kodeSatker + "' and kodeData=" + kodeData + " and jeniscoa=" + jenisCOA + " "
                    + " order by keg.kodeurusanpelaksana, keg.kodeskpd");
            List<Skpd_WS> result = new ArrayList();

            while (rs.next()) {
                result.add(
                        new Skpd_WS(rs.getString("kodeurusanpelaksana").trim() + "." + rs.getString("kodeskpd"), rs.getString("namaskpd"))
                );
            }

            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil data SKPD");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     * method setStatusAPBD
     * @param year
     * @param kodeSatker
     * @param kodeData
     * @param jenisCOA
     * @param statusData
     * @param conn
     * @throws SQLException
     * berfungsi untuk merubah status data APBD
     */
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void setStatusAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, short statusData, Connection conn) throws SQLException {
        Statement stm = null;
        try {
            stm = conn.createStatement();
            
            stm.executeUpdate("update apbd set statusdata = " + statusData
                    + " where tahunanggaran=" + year 
                    + " and kodesatker='" + kodeSatker + "' "
                    + " and kodeData=" + kodeData 
                    + " and jeniscoa=" + jenisCOA );
        } catch (SQLException ex) {
            throw new SQLException("Gagal merubah status data apbd \n" + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
}