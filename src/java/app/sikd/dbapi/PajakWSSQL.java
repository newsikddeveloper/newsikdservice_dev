/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.dbapi;

import app.sikd.dbapi.apbd.IAPBDConstants;
import app.sikd.entity.ws.AkunDTHSKPD_WS;
import app.sikd.entity.ws.BPHTB_WS;
import app.sikd.entity.ws.DTH_WS;
import app.sikd.entity.ws.Hiburan_WS;
import app.sikd.entity.ws.Hotel_WS;
import app.sikd.entity.ws.IMB_WS;
import app.sikd.entity.ws.IzinUsaha_WS;
import app.sikd.entity.ws.Kendaraan_WS;
import app.sikd.entity.ws.PajakDTHSKPD_WS;
import app.sikd.entity.ws.PajakDanRetribusi_WS;
import app.sikd.entity.ws.Restoran_WS;
import app.sikd.entity.ws.RincianBPHTB_WS;
import app.sikd.entity.ws.RincianDTHSKPD_WS;
import app.sikd.entity.ws.RincianHiburan_WS;
import app.sikd.entity.ws.RincianHotel_WS;
import app.sikd.entity.ws.RincianIMB_WS;
import app.sikd.entity.ws.RincianIzinUsaha_WS;
import app.sikd.entity.ws.RincianKendaraan_WS;
import app.sikd.entity.ws.RincianPajakDanRetribusi_WS;
import app.sikd.entity.ws.RincianRestoran_WS;
import app.sikd.entity.ws.SP2DKegiatan_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;

/**
 *
 * @author detra
 */
public class PajakWSSQL implements IPajakWSSQL{
    
    @Override
    public long getLastDTHIndex(Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select max(dthindex) id from dth");
            if (rs.next()) {
                return rs.getLong("id");
            }
            return 0;
        } catch (SQLException ex) {
            throw new SQLException("gagal mengambil index DTH: " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public boolean cekFinalDTH(String kodeSatker, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select statusdata from dth where kodesatker= '" + kodeSatker + "' "
                    + " and tahunanggaran=" + tahun
            );
            short status = 0;
            if (rs.next()) {
                status = rs.getShort("statusdata");
            }
            if( status == 0 || status==1) return false;
            return true;
        } catch (SQLException ex) {
            throw new SQLException("gagal mengambil Status data DTH: " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public boolean cekFinalDTH(String kodeSatker, short tahun, short periode, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select statusdata from dth where kodesatker= '" + kodeSatker + "' "
                    + " and tahunanggaran=" + tahun 
                    + " and periode=" + periode
            );
            short status = 0;
            if (rs.next()) {
                status = rs.getShort("statusdata");
            }
            if( status == 0 || status==1) return false;
            return true;
        } catch (SQLException ex) {
            throw new SQLException("gagal mengambil Status data DTH: " + ex.getMessage());
        } finally {
            System.out.println("gagal cek final");
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    /**
     *
     * @param kodeSatker
     * @param tahun
     * @param periode
     * @param conn
     * @return
     * @throws SQLException
     */
    @Override
    public String getDTHInfo(String kodeSatker, short tahun, short periode, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "select * from dth WHERE "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? and "
                    + " periode=? ";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, tahun);
            stm.setShort(3, periode);
            
            ResultSet rs = stm.executeQuery();
            String result = "";
            if (rs.next()) {
                result = "Informasi DTH Pemerintah ";
                result+=rs.getString("namapemda") + "\n";
                result+="Tahun Anggaran " + rs.getShort("tahunanggaran")+"\n \n";
                result+="Periode "+ rs.getShort("periode");
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                ParsePosition pos = new ParsePosition(0);
                String tgl = "";
                if(rs.getDate("tanggalpengiriman") != null) tgl = sdf.format(rs.getDate("tanggalpengiriman"));
                
                result+="Tanggal Pengiriman : "  + tgl + "\n";
                result+="Nama Aplikasi : " + rs.getString("namaaplikasi") + "\n";
                result+="Pengembang Aplikasi : " + rs.getString("pengembangaplikasi") + "\n";
                String status = "Belum Final";
                short stat = rs.getShort("statusdata");
                if(stat==1) status="Siap Verifikasi";
                else if (stat==2) status = "Proses Verifikasi";
                else if(stat==3) status = "Selesai Verifikasi";
                result+="Status Data : " + status;
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public long createDTH(DTH_WS dth, short iotype, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql +
                    "insert into dth ("
                    + IAPBDConstants.ATTR_KODE_SATKER + ", "
                    + IAPBDConstants.ATTR_KODE_PEMDA + ", "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + ", "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + ", "
                    + "periode, statusdata, tanggalpengiriman, namaaplikasi, pengembangaplikasi, iotype) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            sql = sql + " returning * ) select max(dthindex) as index from zz";
            
            stm = conn.prepareStatement(sql);
            stm.setString(1, dth.getKodeSatker().trim());
            stm.setString(2, dth.getKodePemda().trim());
            stm.setString(3, dth.getNamaPemda().trim());
            stm.setShort(4, dth.getTahunAnggaran());
            stm.setShort(5, dth.getPeriode());
            stm.setShort(6, dth.getStatusData());
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            stm.setTimestamp(7, time);
            stm.setString(8, dth.getNamaAplikasi());
            stm.setString(9, dth.getPengembangAplikasi());
            stm.setShort(10, iotype);
//            stm.executeUpdate();
            rs = stm.executeQuery();
            long index= 0;
            if(rs.next())  index = rs.getLong("index");
            
            return index;//getLastDTHIndex(conn);
        } catch (SIKDServiceException | SQLException ex) {
            System.out.println("gagal input dth");
            throw new SQLException("gagal input DTH: " + ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
//    @Override
//    public String updateDTH(DTH_WS dth, long dthIndex, Connection conn) throws SQLException {
//        PreparedStatement stm = null;
//        try {
//            String sql = "update dth set "
//                    + IAPBDConstants.ATTR_KODE_SATKER + "=?, "
//                    + IAPBDConstants.ATTR_KODE_PEMDA + "=?, "
//                    + IAPBDConstants.ATTR_NAMA_PEMDA + "=?, "
//                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=?, "                     
//                    + "periode=?, statusdata=?, tanggalpengiriman=?, namaaplikasi=?, pengembangaplikasi=? "
//                    + "WHERE dthindex=?";
//            stm = conn.prepareStatement(sql);
//            stm.setString(1, dth.getKodeSatker().trim());
//            stm.setString(2, dth.getKodePemda().trim());
//            stm.setString(3, dth.getNamaPemda().trim());
//            stm.setShort(4, dth.getTahunAnggaran());
//            stm.setShort(5, dth.getPeriode());
//            stm.setShort(6, dth.getStatusData());
//            Timestamp time = new Timestamp(new java.util.Date().getTime());
//            stm.setTimestamp(7, time);
//            stm.setString(8, dth.getNamaAplikasi());
//            stm.setString(9, dth.getPengembangAplikasi());
//            stm.setLong(10, dthIndex);
//            stm.executeUpdate();
//            return "Sukses Update DTH";
//        } catch (SQLException ex) {
//            throw new SQLException("gagal merubah DTH: " + ex.getMessage());
//        } finally {
//            if (stm != null) {
//                stm.close();
//            }
//        }
//    }

    
    @Override
    public void deleteDTH(String kodeSatker, short thn, short periode, Connection conn) throws SQLException {
//        int level = conn.getTransactionIsolation();
        PreparedStatement stm = null;
        try {
            String sql = "delete from dth where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? and periode =?";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, thn);
            stm.setShort(3, periode);
            stm.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("gagal hapus dth");
            throw new SQLException("gagal menghapus DTH: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public void deleteDTH(String kodeSatker, short thn, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from dth where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? ";
            
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, thn);            
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException("gagal menghapus DTH: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    /*
    @Override
    public long getDTHIndex(String kodeSatker, short thn, short periode, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {            
            String sql = "select dthindex from dth where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? and periode =?";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, thn);
            stm.setShort(3, periode);
            rs = stm.executeQuery();
            long id = 0;
            if( rs.next() ) id = rs.getLong("dthindex");
            return id;
        } catch (SQLException ex) {
            throw new SQLException("gagal mengambil index DTH: " + ex.getMessage());
        } finally {
            if( rs!= null ) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public long getDTHIndex(String kodeSatker, short thn, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {            
            String sql = "select dthindex from dth where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? ";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, thn);
            rs = stm.executeQuery();
            long id = 0;
            if( rs.next() ) id = rs.getLong("dthindex");
            return id;
        } catch (SQLException ex) {
            throw new SQLException("gagal mengambil index DTH SKPD: " + ex.getMessage());
        } finally {
            if( rs!= null ) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    */
    
    @Override
    public long getLastRincianDTHSKPDIndex(Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select max(rinciandthindex) id from rinciandthskpd");
            if (rs.next()) {
                return rs.getLong("id");
            }
            return 0;
        } catch (SQLException ex) {
            System.out.println("gagal ambil index");
            throw new SQLException("gagal mengambil index Rincian DTH SKPD: " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public long createRincianDTHSKPD(RincianDTHSKPD_WS rincian, long dthindex, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql + "insert into rinciandthskpd ( "
                    + "dthindex, spmnumber, sp2dnumber, jenissp2d, tanggalsp2d, "
                    + "nilaisp2d, nilaitotalpajak, nilaitotalpotongan, npwpbud, " 
                    + "npwpskpd, npwppenerima, namapenerima, sumberdana, subsumberdana, tahapsalurdana, keterangan ) "
                    + "VALUES ("
                    + "?, ?, ?, ?, ?, "
                    + "?, ?, ?, ?, " 
                    + "?, ?, ?, ?, ?, ?, ?)";
            sql = sql + " returning * ) select max(rinciandthindex) as index from zz";
            
            stm = conn.prepareStatement(sql);
            stm.setLong(1, dthindex);
            if(rincian.getNomorSPM()!=null)
            stm.setString(2, rincian.getNomorSPM().trim());
            else stm.setString(2, "");
            if(rincian.getNomorSP2D()!=null)
            stm.setString(3, rincian.getNomorSP2D().trim());
            else stm.setString(3, "");
            
            stm.setShort(4, rincian.getJenisSP2D());
            
            if( rincian.getTanggalSP2D() != null )
            stm.setDate(5, new java.sql.Date(rincian.getTanggalSP2D().getTime()));
            else stm.setNull(5, Types.DATE);
            stm.setDouble(6, rincian.getNilaiSP2D());
            stm.setDouble(7, rincian.getNilaiTotalPajak());
            stm.setDouble(8, rincian.getNilaiTotalPotongan());
            
            if( rincian.getNpwpBUD() != null )
            stm.setString(9, rincian.getNpwpBUD().trim());
            else stm.setString(9, "");
            if( rincian.getNpwpSKPD() != null )
            stm.setString(10, rincian.getNpwpSKPD().trim());
            else stm.setString(10, "");
            
            if( rincian.getNpwpPenerima() != null )
            stm.setString(11, rincian.getNpwpPenerima().trim());
            else stm.setString(11, "");
            
            if( rincian.getNamaPenerima() != null )
            stm.setString(12, rincian.getNamaPenerima().trim());
            else stm.setString(12, "");
            
            stm.setShort(13, rincian.getSumberDana());
            
            if( rincian.getSubSumberDana()!= null )
            stm.setString(14, rincian.getSubSumberDana().trim());
            else stm.setString(14, "");
            
            stm.setShort(15, rincian.getTahapSalurDana());
            
            if( rincian.getKeterangan()!= null )
            stm.setString(16, rincian.getKeterangan().trim());
            else stm.setString(16, "");
            rs = stm.executeQuery();
            long index = 0;
            if(rs.next()) index = rs.getLong("index");
//            stm.executeUpdate();
            
            return index;//getLastRincianDTHSKPDIndex(conn);
        } catch (SQLException ex) {
            System.out.println("gagal input rincian");
            throw new SQLException("gagal input Rincian DTH SKPD: " + ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
            
        }
    }
    
    
    
    @Override
    public long getLastSp2DKegiatanIndex(Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select max(kegiatanindex) id from sp2dkegiatan");
            if (rs.next()) {
                return rs.getLong("id");
            }
            return 0;
        } catch (SQLException ex) {
            throw new SQLException("gagal mengambil index SP2D Kegiatan: " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public long createSP2DKegiatan(SP2DKegiatan_WS kegiatan, long rinciandthindex, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            String sql = " with zz as ( ";
            sql = sql +
                    "insert into sp2dkegiatan ( rinciandthindex, " 
                    + "kodeurusanprogram, namaurusanprogram, kodeurusanpelaksana, namaurusanpelaksana, "
                    + "kodeskpd, namaskpd, kodeprogram, namaprogram, kodekegiatan, namakegiatan ) "
                    + "VALUES (?, " 
                    + "?, ?, ?, ?, "
                    + "?, ?, ?, ?, ?, ?)";
            sql = sql + " returning * ) select max(kegiatanindex) as index from zz";
            stm = conn.prepareStatement(sql);
            stm.setLong(1, rinciandthindex);
            stm.setString(2, kegiatan.getKodeUrusanProgram());
            stm.setString(3, kegiatan.getNamaUrusanProgram().trim());
            stm.setString(4, kegiatan.getKodeUrusanPelaksana());
            stm.setString(5, kegiatan.getNamaUrusanPelaksana().trim());
            stm.setString(6, kegiatan.getKodeSKPD());
            stm.setString(7, kegiatan.getNamaSKPD().trim());
            stm.setString(8, kegiatan.getKodeProgram());
            stm.setString(9, kegiatan.getNamaProgram().trim());
            stm.setString(10, kegiatan.getKodeKegiatan());
            stm.setString(11, kegiatan.getNamaKegiatan().trim());
//            stm.executeUpdate();
            rs = stm.executeQuery();
            long index = 0;
            if(rs.next()) index= rs.getLong("index");
            
            return index;//getLastSp2DKegiatanIndex(conn);
        } catch (SIKDServiceException | SQLException ex) {
            System.out.println("gagal input sp2d");
            throw new SQLException("gagal input SP2D Kegiatan: " + ex.getMessage());
        } finally {
            if(rs!=null) rs.close();
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    /*@Override
    public void deleteSP2DKegiatan(long dthIndex, String kodeUrusanProgram, String kodeUrusanPelaksana, String kodeSKPD, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from dthskpd where dthindex=? and"
                    + IAPBDConstants.ATTR_KODE_URUSAN_PROGRAM + "=? and "
                    + IAPBDConstants.ATTR_KODE_URUSAN_PELAKSANA + "=? and "
                    + IAPBDConstants.ATTR_KODE_SKPD + "=?";
            stm = conn.prepareStatement(sql);
            stm.setLong(1, dthIndex);
            stm.setString(2, kodeUrusanProgram.trim());
            stm.setString(3, kodeUrusanPelaksana);
            stm.setString(4, kodeSKPD);
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException("gagal menghapus DTH SKPD: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    */
    
    @Override
    public long getLastAkunDTHIndex(Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select max(akundthindex) id from akundthskpd");
            if (rs.next()) {
                return rs.getLong("id");
            }
            return 0;
        } catch (SQLException ex) {
            throw new SQLException("gagal mengambil index Akun DTH: " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public void createAkunDTHSKPD(AkunDTHSKPD_WS akun, long kegiatanindex, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "insert into akundthskpd ( kegiatanindex, kodeakunutama, namaakunutama, "
                    + "kodeakunkelompok, namaakunkelompok, kodeakunjenis, namaakunjenis, "
                    + "kodeakunobjek, namaakunobjek, kodeakunrincian, namaakunrincian, "
                    + "kodeakunsubrincian, namaakunsubrincian, nilairekening ) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            
            stm = conn.prepareStatement(sql);
            stm.setLong(1, kegiatanindex);
            stm.setString(2, akun.getKodeAkunUtama().trim());
            stm.setString(3, akun.getNamaAkunUtama().trim());
            stm.setString(4, akun.getKodeAkunKelompok().trim());
            stm.setString(5, akun.getNamaAkunKelompok());
            stm.setString(6, akun.getKodeAkunJenis());
            stm.setString(7, akun.getNamaAkunJenis().trim());
            stm.setString(8, akun.getKodeAkunObjek().trim());
            stm.setString(9, akun.getNamaAkunObjek().trim());
            stm.setString(10, akun.getKodeAkunRincian().trim());
            stm.setString(11, akun.getNamaAkunRincian().trim());            
            stm.setString(12, akun.getKodeAkunSubRincian());
            stm.setString(13, akun.getNamaAkunSubRincian());
            stm.setDouble(14, akun.getNilaiRekening());
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException("gagal input Akun DTH SKPD: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    @Override
    public void createPajakDTHSKPD(PajakDTHSKPD_WS pajak, long rinciandthskpdindex, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "insert into pajakdthskpd ( rinciandthskpd, "
                    + "kodeakunpajak, namaakunpajak, jenispajak, nilaipotongan ) "
                    + "VALUES (?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);
            stm.setLong(1, rinciandthskpdindex);
            stm.setString(2, pajak.getKodeAkunPajak());
            stm.setString(3, pajak.getNamaAkunPajak());
            stm.setShort(4, pajak.getJenisPajak());
            stm.setDouble(5, pajak.getNilaiPotongan());
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException("gagal input pajak DTH SKPD: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    
    @Override
    public boolean cekFinalKendaraan(String kodeSatker, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select statusdata from kendaraan where kodesatker= '" + kodeSatker + "' "
                    + " and tahunanggaran=" + tahun 
            );
            short status = 0;
            if (rs.next()) {
                status = rs.getShort("statusdata");
            }
            if( status == 0 ) return false;
            return true;
        } catch (SQLException ex) {
            throw new SQLException("gagal mengambil Status data Kepemilikan Kendaraan: " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public long createKendaraan(Kendaraan_WS kendaraan, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        Statement stm2=null;
        ResultSet rs = null;
        try {            
            String sql = "insert into kendaraan ("
                    + IAPBDConstants.ATTR_KODE_SATKER + ", "
                    + IAPBDConstants.ATTR_KODE_PEMDA + ", "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + ", "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + ", "
                    + "tanggalpengiriman, statusdata, namaaplikasi, pengembangaplikasi ) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kendaraan.getKodeSatker().trim());
            stm.setString(2, kendaraan.getKodePemda().trim());
            stm.setString(3, kendaraan.getNamaPemda().trim());
            stm.setShort(4, kendaraan.getTahunAnggaran());
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            stm.setTimestamp(5, time);
            stm.setShort(6, kendaraan.getStatusData());
            stm.setString(7, kendaraan.getNamaAplikasi());
            stm.setString(8, kendaraan.getPengembangAplikasi());
            stm.executeUpdate();
            
            long id = 0;
            stm2 = conn.createStatement();            
            rs = stm2.executeQuery("select max(kendaraanindex) id from kendaraan");
            if( rs.next() ) id = rs.getLong("id");
            
            return id;
        } catch (SIKDServiceException | SQLException ex) {
            throw new SQLException("gagal input Kepemilikan Kendaraan: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (stm2 != null) {
                stm2.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    @Override
    public void deleteKendaraan(String kodeSatker, String kodePemda, short thn, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from kendaraan where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_KODE_PEMDA + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? ";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setString(2, kodePemda.trim());
            stm.setShort(3, thn);            
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException("gagal menghapus Kepemilikan Kendaraan: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    @Override
    public String createRincianKendaraan(RincianKendaraan_WS rincian, long indexKendaraan, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        Statement stm2=null;
        try {            
            String sql = "insert into rinciankendaraan VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";                    
            stm = conn.prepareStatement(sql);
            stm.setLong(1, indexKendaraan);
            
            stm.setString(2, rincian.getNomorPolisi());
            stm.setString(3, rincian.getPemilik());
            stm.setString(4, rincian.getAlamat());
            stm.setString(5, rincian.getNpwp());
            stm.setString(6, rincian.getKpp());
            stm.setString(7, rincian.getCabangNpwp());
            stm.setShort(8, rincian.getTahunPembuatan());
            stm.setDouble(9, rincian.getNjkb());
            stm.setString(10, rincian.getJenis());
            stm.setString(11, rincian.getMerk());
            stm.setString(12, rincian.getTipe());
            stm.setString(13, rincian.getCc());
            stm.setString(14, rincian.getBahanBakar());            
            stm.executeUpdate();
            return "Sukses input data Rincian Kepemilikan Kendaraan";
        } catch (SQLException ex) {
            throw new SQLException("gagal input Kepemilikan Kendaraan: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (stm2 != null) {
                stm2.close();
            } 
        }
    }
    
    @Override
    public boolean cekFinalHotel(String kodeSatker, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select statusdata from hotel where kodesatker= '" + kodeSatker + "' "
                    + " and tahunanggaran=" + tahun 
            );
            short status = 0;
            if (rs.next()) {
                status = rs.getShort("statusdata");
            }
            if( status == 0 ) return false;
            return true;
        } catch (SQLException ex) {
            throw new SQLException("gagal mengambil Status data Kepemilikan Hotel: " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public long createHotel(Hotel_WS hotel, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        Statement stm2 = null;
        ResultSet rs = null;
        try {            
            String sql = "insert into hotel ("
                    + IAPBDConstants.ATTR_KODE_SATKER + ", "
                    + IAPBDConstants.ATTR_KODE_PEMDA + ", "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + ", "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + ", "
                    + "tanggalpengiriman, statusdata, namaaplikasi, pengembangaplikasi ) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);
            stm.setString(1, hotel.getKodeSatker().trim());
            stm.setString(2, hotel.getKodePemda().trim());
            stm.setString(3, hotel.getNamaPemda().trim());
            stm.setShort(4, hotel.getTahunAnggaran());
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            stm.setTimestamp(5, time);
            stm.setShort(6, hotel.getStatusData());
            stm.setString(7, hotel.getNamaAplikasi());
            stm.setString(8, hotel.getPengembangAplikasi());
            stm.executeUpdate();
            
            long id = 0;
            stm2 = conn.createStatement();            
            rs = stm2.executeQuery("select max(hotelindex) id from hotel");
            if( rs.next() ) id = rs.getLong("id");
            
            return id;
        } catch (SIKDServiceException | SQLException ex) {
            throw new SQLException("gagal input Kepemilikan Hotel: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (stm2 != null) {
                stm2.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    @Override
    public void deleteHotel(String kodeSatker, String kodePemda, short thn, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from hotel where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_KODE_PEMDA + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? ";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setString(2, kodePemda.trim());
            stm.setShort(3, thn);            
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException("gagal menghapus Kepemilikan Hotel: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    @Override
    public String createRincianHotel(RincianHotel_WS rincian, long indexHotel, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        Statement stm2=null;        
        try {            
            String sql = "insert into rincianhotel VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";                    
            stm = conn.prepareStatement(sql);            
            stm.setLong(1, indexHotel);
            stm.setString(2, rincian.getNamaHotel());
            stm.setString(3, rincian.getAlamat());
            stm.setInt(4, rincian.getJumlahKamar());
            stm.setString(5, rincian.getNamaPemilik());
            stm.setString(6, rincian.getAlamatPemilik());
            stm.setString(7, rincian.getNpwpPemilik());
            stm.setString(8, rincian.getKppPemilik());
            stm.setString(9, rincian.getCabangNpwpPemilik());
            stm.setString(10, rincian.getNamaPengelola());
            stm.setString(11, rincian.getAlamatPengelola());
            stm.setString(12, rincian.getNpwpPengelola());
            stm.setString(13, rincian.getKppPengelola());
            stm.setString(14, rincian.getCabangNpwpPengelola());
            stm.setDouble(15, rincian.getJumlahPajak());            
            stm.executeUpdate();
            return "Sukses input data Rincian Kepemilikan Hotel";
        } catch (SQLException ex) {
            throw new SQLException("gagal input Rincian Kepemilikan Hotel: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (stm2 != null) {
                stm2.close();
            } 
        }
    }
    
    @Override
    public boolean cekFinalHiburan(String kodeSatker, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select statusdata from hiburan where kodesatker= '" + kodeSatker + "' "
                    + " and tahunanggaran=" + tahun 
            );
            short status = 0;
            if (rs.next()) {
                status = rs.getShort("statusdata");
            }
            if( status == 0 ) return false;
            return true;
        } catch (SQLException ex) {
            throw new SQLException("gagal mengambil Status data Kepemilikan Tempat Hiburan: " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public long createHiburan(Hiburan_WS hiburan, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        Statement stm2 = null;
        ResultSet rs = null;
        try {            
            String sql = "insert into hiburan ("
                    + IAPBDConstants.ATTR_KODE_SATKER + ", "
                    + IAPBDConstants.ATTR_KODE_PEMDA + ", "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + ", "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + ", "
                    + "tanggalpengiriman, statusdata, namaaplikasi, pengembangaplikasi ) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);
            stm.setString(1, hiburan.getKodeSatker().trim());
            stm.setString(2, hiburan.getKodePemda().trim());
            stm.setString(3, hiburan.getNamaPemda().trim());
            stm.setShort(4, hiburan.getTahunAnggaran());
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            stm.setTimestamp(5, time);
            stm.setShort(6, hiburan.getStatusData());
            stm.setString(7, hiburan.getNamaAplikasi());
            stm.setString(8, hiburan.getPengembangAplikasi());
            stm.executeUpdate();
            
            long id = 0;
            stm2 = conn.createStatement();            
            rs = stm2.executeQuery("select max(hiburanindex) id from hiburan");
            if( rs.next() ) id = rs.getLong("id");
            
            return id;
        } catch (SIKDServiceException | SQLException ex) {
            throw new SQLException("gagal input Kepemilikan Tempat Hiburan: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (stm2 != null) {
                stm2.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    @Override
    public void deleteHiburan(String kodeSatker, String kodePemda, short thn, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from hiburan where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_KODE_PEMDA + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? ";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setString(2, kodePemda.trim());
            stm.setShort(3, thn);            
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException("gagal menghapus Kepemilikan Tempat Hiburan: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public String createRincianHiburan(RincianHiburan_WS rincian, long indexHiburan, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        Statement stm2=null;        
        try {            
            String sql = "insert into rincianhiburan VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);            
            stm.setLong(1, indexHiburan);
            stm.setString(2, rincian.getNamaHiburan());
            stm.setString(3, rincian.getAlamat());
            stm.setString(4, rincian.getNamaPemilik());
            stm.setString(5, rincian.getAlamatPemilik());
            stm.setString(6, rincian.getNpwpPemilik());
            stm.setString(7, rincian.getKppPemilik());
            stm.setString(8, rincian.getCabangNpwpPemilik());
            stm.setString(9, rincian.getNamaPengelola());
            stm.setString(10, rincian.getAlamatPengelola());
            stm.setString(11, rincian.getNpwpPengelola());
            stm.setString(12, rincian.getKppPengelola());
            stm.setString(13, rincian.getCabangNpwpPengelola());
            stm.setDouble(14, rincian.getJumlahPajak());            
            stm.executeUpdate();
            return "Sukses input data Rincian Kepemilikan Hiburan";
        } catch (SQLException ex) {
            throw new SQLException("gagal input Rincian Kepemilikan Hiburan: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (stm2 != null) {
                stm2.close();
            } 
        }
    }
    
    @Override
    public boolean cekFinalIzinUsaha(String kodeSatker, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select statusdata from usaha where kodesatker= '" + kodeSatker + "' "
                    + " and tahunanggaran=" + tahun 
            );
            short status = 0;
            if (rs.next()) {
                status = rs.getShort("statusdata");
            }
            if( status == 0 ) return false;
            return true;
        } catch (SQLException ex) {
            throw new SQLException("gagal mengambil Status data Izin Usaha: " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public long createIzinUsaha(IzinUsaha_WS usaha, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        Statement stm2=null;
        ResultSet rs = null;
        try {            
            String sql = "insert into usaha ("
                    + IAPBDConstants.ATTR_KODE_SATKER + ", "
                    + IAPBDConstants.ATTR_KODE_PEMDA + ", "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + ", "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + ", "
                    + "tanggalpengiriman, statusdata, namaaplikasi, pengembangaplikasi ) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);
            stm.setString(1, usaha.getKodeSatker().trim());
            stm.setString(2, usaha.getKodePemda().trim());
            stm.setString(3, usaha.getNamaPemda().trim());
            stm.setShort(4, usaha.getTahunAnggaran());
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            stm.setTimestamp(5, time);
            stm.setShort(6, usaha.getStatusData());
            stm.setString(7, usaha.getNamaAplikasi());
            stm.setString(8, usaha.getPengembangAplikasi());
            stm.executeUpdate();
            
            long id = 0;
            stm2 = conn.createStatement();            
            rs = stm2.executeQuery("select max(usahaindex) id from usaha");
            if( rs.next() ) id = rs.getLong("id");
            
            return id;
        } catch (SIKDServiceException | SQLException ex) {
            throw new SQLException("gagal input Izin Usaha: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (stm2 != null) {
                stm2.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    @Override
    public void deleteIzinUsaha(String kodeSatker, String kodePemda, short thn, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from usaha where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_KODE_PEMDA + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? ";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setString(2, kodePemda.trim());
            stm.setShort(3, thn);            
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException("gagal menghapus Kepemilikan izin Usaha: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public String createRincianIzinUsaha(RincianIzinUsaha_WS rincian, long indexIzinUsaha, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        Statement stm2=null;        
        try {            
            String sql = "insert into rincianusaha VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);
            stm.setLong(1, indexIzinUsaha);
            stm.setString(2, rincian.getNomorIzin());
            if( rincian.getTanggalIzin() != null )
            stm.setDate(3, new java.sql.Date(rincian.getTanggalIzin().getTime()));
            else stm.setNull(3, Types.DATE);
            stm.setString(4, rincian.getNamaPerusahaan());
            stm.setString(5, rincian.getAlamatPerusahaan());
            stm.setString(6, rincian.getNpwpPerusahaan());
            stm.setString(7, rincian.getKppPerusahaan());
            stm.setString(8, rincian.getCabangNpwpPerusahaan());
            stm.setString(9, rincian.getJenisUsaha());
            stm.setString(10, rincian.getNamaPemilik());
            stm.setString(11, rincian.getAlamatPemilik());
            stm.setString(12, rincian.getNpwpPemilik());
            stm.setString(13, rincian.getKppPemilik());
            stm.setString(14, rincian.getCabangNpwpPemilik());
            stm.setString(15, rincian.getKlasifikasi());
            stm.setDouble(16, rincian.getModal());            
            stm.setInt(17, rincian.getJumlahKaryawan());
            stm.setShort(18, rincian.getMasaBerlaku());            
            stm.executeUpdate();
            return "Sukses input data Rincian Izin Usaha";
        } catch (SQLException ex) {
            throw new SQLException("gagal input Rincian Izin Usaha: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (stm2 != null) {
                stm2.close();
            } 
        }
    }
    
    @Override
    public boolean cekFinalBPHTB(String kodeSatker, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select statusdata from bphtb where kodesatker= '" + kodeSatker + "' "
                    + " and tahunanggaran=" + tahun 
            );
            short status = 0;
            if (rs.next()) {
                status = rs.getShort("statusdata");
            }
            if( status == 0 ) return false;
            return true;
        } catch (SQLException ex) {
            throw new SQLException("gagal mengambil Status data BPHTB: " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public long createBPHTB(BPHTB_WS bphtb, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        Statement stm2=null;
        ResultSet rs = null;
        try {            
            String sql = "insert into bphtb ("
                    + IAPBDConstants.ATTR_KODE_SATKER + ", "
                    + IAPBDConstants.ATTR_KODE_PEMDA + ", "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + ", "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + ", "
                    + "tanggalpengiriman, statusdata, namaaplikasi, pengembangaplikasi ) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);
            stm.setString(1, bphtb.getKodeSatker().trim());
            stm.setString(2, bphtb.getKodePemda().trim());
            stm.setString(3, bphtb.getNamaPemda().trim());
            stm.setShort(4, bphtb.getTahunAnggaran());
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            stm.setTimestamp(5, time);
            stm.setShort(6, bphtb.getStatusData());
            stm.setString(7, bphtb.getNamaAplikasi());
            stm.setString(8, bphtb.getPengembangAplikasi());
            stm.executeUpdate();
            
            long id = 0;
            stm2 = conn.createStatement();            
            rs = stm2.executeQuery("select max(bphtbindex) id from bphtb");
            if( rs.next() ) id = rs.getLong("id");
            
            return id;
        } catch (SIKDServiceException | SQLException ex) {
            throw new SQLException("gagal input BPHTB: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (stm2 != null) {
                stm2.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    @Override
    public void deleteBPHTB(String kodeSatker, String kodePemda, short thn, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from bphtb where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_KODE_PEMDA + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? ";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setString(2, kodePemda.trim());
            stm.setShort(3, thn);            
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException("gagal menghapus BPHTB: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public String createRincianBPHTB(RincianBPHTB_WS rincian, long indexBPHTB, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        Statement stm2=null;        
        try {            
            String sql = "insert into rincianbphtb VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);            
            stm.setLong(1, indexBPHTB);
            stm.setString(2, rincian.getNamaPenerima());
            stm.setString(3, rincian.getAlamatPenerima());
            stm.setString(4, rincian.getNpwpPenerima());
            stm.setString(5, rincian.getKppPenerima());
            stm.setString(6, rincian.getCabangNpwpPenerima());
            stm.setString(7, rincian.getAlamatObjek());
            stm.setDouble(8, rincian.getNilaiPerolehan());
            stm.setDouble(9, rincian.getLuasTanah());
            stm.setDouble(10, rincian.getLuasBangunan());
            if( rincian.getTanggalTransaksi() != null )
            stm.setDate(11, new java.sql.Date(rincian.getTanggalTransaksi().getTime()));
            else stm.setNull(11, Types.DATE);
            stm.setDouble(12, rincian.getNilaiBphtb());
            stm.executeUpdate();
            return "Sukses input data Rincian BPHTB";
        } catch (SQLException ex) {
            throw new SQLException("gagal input Rincian BPHTB: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (stm2 != null) {
                stm2.close();
            } 
        }
    }
    
    @Override
    public boolean cekFinalIMB(String kodeSatker, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select statusdata from imb where kodesatker= '" + kodeSatker + "' "
                    + " and tahunanggaran=" + tahun 
            );
            short status = 0;
            if (rs.next()) {
                status = rs.getShort("statusdata");
            }
            if( status == 0 ) return false;
            return true;
        } catch (SQLException ex) {
            throw new SQLException("gagal mengambil Status data IMB: " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public long createIMB(IMB_WS imb, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        Statement stm2=null;
        ResultSet rs = null;
        try {            
            String sql = "insert into imb ("
                    + IAPBDConstants.ATTR_KODE_SATKER + ", "
                    + IAPBDConstants.ATTR_KODE_PEMDA + ", "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + ", "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + ", "
                    + "tanggalpengiriman, namaaplikasi, pengembangaplikasi ) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);
            stm.setString(1, imb.getKodeSatker().trim());
            stm.setString(2, imb.getKodePemda().trim());
            stm.setString(3, imb.getNamaPemda().trim());
            stm.setShort(4, imb.getTahunAnggaran());
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            stm.setTimestamp(5, time);
            stm.setShort(6, imb.getStatusData());
            stm.setString(7, imb.getNamaAplikasi());
            stm.setString(8, imb.getPengembangAplikasi());
            stm.executeUpdate();
            
            long id = 0;
            stm2 = conn.createStatement();            
            rs = stm2.executeQuery("select max(imbindex) id from imb");
            if( rs.next() ) id = rs.getLong("id");
            
            return id;
        } catch (SIKDServiceException | SQLException ex) {
            throw new SQLException("gagal input IMB: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (stm2 != null) {
                stm2.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    
    @Override
    public void deleteIMB(String kodeSatker, String kodePemda, short thn, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from imb where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_KODE_PEMDA + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? ";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setString(2, kodePemda.trim());
            stm.setShort(3, thn);            
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException("gagal menghapus IMB: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public String createRincianIMB(RincianIMB_WS rincian, long indexIMB, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        Statement stm2=null;        
        try {            
            String sql = "insert into rincianimb VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);            
            
            stm.setLong(1, indexIMB);
            stm.setString(2, rincian.getNomorIzin());
            if( rincian.getTanggalIzin()!= null )
            stm.setDate(3, new java.sql.Date(rincian.getTanggalIzin().getTime()));
            else stm.setNull(3, Types.DATE);
            stm.setString(4, rincian.getNamaPemohon());
            stm.setString(5, rincian.getAlamatPemohon());
            stm.setString(6, rincian.getNpwpPemohon());
            stm.setString(7, rincian.getKppPemohon() );
            stm.setString(8, rincian.getCabangNpwpPemohon());
            stm.setString(9, rincian.getLokasi());
            stm.setDouble(10, rincian.getLuasTanah());
            stm.setDouble(11, rincian.getLuasBangunan());
            stm.setDouble(12, rincian.getJumlahLantai());
            stm.setString(13, rincian.getFungsi());
            stm.setString(14, rincian.getStatusTanah());
            
            stm.executeUpdate();
            return "Sukses input data Rincian IMB";
        } catch (SQLException ex) {
            throw new SQLException("gagal input Rincian IMB: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (stm2 != null) {
                stm2.close();
            } 
        }
    }
    
    @Override
    public boolean cekFinalRestoran(String kodeSatker, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select statusdata from restoran where kodesatker= '" + kodeSatker + "' "
                    + " and tahunanggaran=" + tahun 
            );
            short status = 0;
            if (rs.next()) {
                status = rs.getShort("statusdata");
            }
            if( status == 0 ) return false;
            return true;
        } catch (SQLException ex) {
            throw new SQLException("gagal mengambil Status data Kepemilikan Restoran: " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public long createRestoran(Restoran_WS restoran, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        Statement stm2 = null;
        ResultSet rs = null;
        try {            
            String sql = "insert into restoran ("
                    + IAPBDConstants.ATTR_KODE_SATKER + ", "
                    + IAPBDConstants.ATTR_KODE_PEMDA + ", "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + ", "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + ", "
                    + "tanggalpengiriman, statusdata, namaaplikasi, pengembangaplikasi ) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);
            stm.setString(1, restoran.getKodeSatker().trim());
            stm.setString(2, restoran.getKodePemda().trim());
            stm.setString(3, restoran.getNamaPemda().trim());
            stm.setShort(4, restoran.getTahunAnggaran());
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            stm.setTimestamp(5, time);
            stm.setShort(6, restoran.getStatusData());
            stm.setString(7, restoran.getNamaAplikasi());
            stm.setString(8, restoran.getPengembangAplikasi());
            stm.executeUpdate();
            
            long id = 0;
            stm2 = conn.createStatement();            
            rs = stm2.executeQuery("select max(restoranindex) id from restoran");
            if( rs.next() ) id = rs.getLong("id");
            
            return id;
        } catch (SIKDServiceException | SQLException ex) {
            throw new SQLException("gagal input  Kepemilikan Restoran: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (stm2 != null) {
                stm2.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    @Override
    public void deleteRestoran(String kodeSatker, String kodePemda, short thn, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from restoran where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_KODE_PEMDA + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? ";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setString(2, kodePemda.trim());
            stm.setShort(3, thn);            
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException("gagal menghapus Kepemilikan Restoran: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public String createRincianRestoran(RincianRestoran_WS rincian, long indexRestoran, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        Statement stm2=null;        
        try {            
            String sql = "insert into rincianrestoran VALUES( ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);                        
            stm.setLong(1, indexRestoran);
            stm.setString(2, rincian.getNamaRestoran());
            stm.setString(3, rincian.getAlamat());
            stm.setInt(4, rincian.getKapasitas());
            stm.setString(5, rincian.getNamaPemilik());
            stm.setString(6, rincian.getAlamatPemilik());
            stm.setString(7, rincian.getNpwpPemilik());
            stm.setString(8, rincian.getKppPemilik());
            stm.setString(9, rincian.getCabangNpwpPemilik());
            stm.setString(10, rincian.getNamaPengelola());
            stm.setString(11, rincian.getAlamatPengelola());
            stm.setString(12, rincian.getNpwpPengelola());
            stm.setString(13, rincian.getKppPengelola());
            stm.setString(14, rincian.getCabangNpwpPengelola());
            stm.setDouble(15, rincian.getJumlahPajak());
            stm.setInt(16, rincian.getJumlahKaryawan());
            
            stm.executeUpdate();
            return "Sukses input data Rincian Kepemilikan Restoran";
        } catch (SQLException ex) {
            throw new SQLException("gagal input Rincian Kepemilikan Restoran: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (stm2 != null) {
                stm2.close();
            } 
        }
    }
    
    
    @Override
    public boolean cekFinalPajakDanRetribusiDaerah(String kodeSatker, short tahun, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("select statusdata from pajakdanretribusidaerah where kodesatker= '" + kodeSatker + "' "
                    + " and tahunanggaran=" + tahun 
            );
            short status = 0;
            if (rs.next()) {
                status = rs.getShort("statusdata");
            }
            if( status == 0 ) return false;
            return true;
        } catch (SQLException ex) {
            throw new SQLException("gagal mengambil Status data Pajak Dan Retribusi Daerah: " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public void deletePajakDanRetribusiDaerah(String kodeSatker, short thn, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        try {
            String sql = "delete from pajakdanretribusidaerah where "
                    + IAPBDConstants.ATTR_KODE_SATKER + "=? and "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + "=? ";
            stm = conn.prepareStatement(sql);
            stm.setString(1, kodeSatker.trim());
            stm.setShort(2, thn);            
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException("gagal menghapus Data Pajak Dan Retribusi Daerah: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    @Override
    public long createPajakDanRetribusiDaerah(PajakDanRetribusi_WS obj, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        Statement stm2=null;
        ResultSet rs = null;
        try {            
            String sql = "insert into pajakdanretribusidaerah ("
                    + IAPBDConstants.ATTR_KODE_SATKER + ", "
                    + IAPBDConstants.ATTR_KODE_PEMDA + ", "
                    + IAPBDConstants.ATTR_NAMA_PEMDA + ", "
                    + IAPBDConstants.ATTR_TAHUN_ANGGARAN + ", "
                    + "tanggalpengiriman, statusdata, namaaplikasi, pengembangaplikasi ) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            stm = conn.prepareStatement(sql);
            stm.setString(1, obj.getKodeSatker().trim());
            stm.setString(2, obj.getKodePemda().trim());
            stm.setString(3, obj.getNamaPemda().trim());
            stm.setShort(4, obj.getTahunAnggaran());
            Timestamp time = new Timestamp(new java.util.Date().getTime());
            stm.setTimestamp(5, time);
            stm.setShort(6, obj.getStatusData());
            stm.setString(7, obj.getNamaAplikasi());
            stm.setString(8, obj.getPengembangAplikasi());
            stm.executeUpdate();
            
            long id = 0;
            stm2 = conn.createStatement();            
            rs = stm2.executeQuery("select max(pajakindex) id from pajakdanretribusidaerah");
            if( rs.next() ) id = rs.getLong("id");
            
            return id;
        } catch (SIKDServiceException | SQLException ex) {
            throw new SQLException("gagal input data Pajak Dan Retribusi Daerah: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (stm2 != null) {
                stm2.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    
    @Override
    public String createRincianPajakDanRetribusiDaerah(RincianPajakDanRetribusi_WS rincian, long indexPajak, Connection conn) throws SQLException {
        PreparedStatement stm = null;
        Statement stm2=null;        
        try {            
            String sql = "insert into rincianpajakdanretribusidaerah VALUES(?,?,?,?,?)";
            stm = conn.prepareStatement(sql);            
            stm.setLong(1, indexPajak);
            stm.setString(2, rincian.getNamaPungutan());
            stm.setShort(3, rincian.getJenisPungutan());
            stm.setString(4, rincian.getDasarHukum());
            stm.setDouble(5, rincian.getJumlahPungutan());
            stm.executeUpdate();
            return "Sukses input data Rincian Pajak Dan Retribusi Daerah";
        } catch (SQLException ex) {
            throw new SQLException("gagal input Rincian Pajak Dan Retribusi Daerah: " + ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (stm2 != null) {
                stm2.close();
            } 
        }
    }
    
    
}
