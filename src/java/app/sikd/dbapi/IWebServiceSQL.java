package app.sikd.dbapi;

import app.sikd.entity.ws.Apbd_WS;
import app.sikd.entity.ws.ArusKasSaldo_WS;
import app.sikd.entity.ws.ArusKas_WS;
import app.sikd.entity.ws.ArusKeluarInvestasi_WS;
import app.sikd.entity.ws.ArusKeluarNonAnggaran_WS;
import app.sikd.entity.ws.ArusKeluarOperasi_WS;
import app.sikd.entity.ws.ArusKeluarPembiayaan_WS;
import app.sikd.entity.ws.ArusMasukInvestasi_WS;
import app.sikd.entity.ws.ArusMasukNonAnggaran_WS;
import app.sikd.entity.ws.ArusMasukOperasi_WS;
import app.sikd.entity.ws.ArusMasukPembiayaan_WS;
import app.sikd.entity.ws.DefisitNonOperasional_WS;
import app.sikd.entity.ws.KegiatanAPBD_WS;
import app.sikd.entity.ws.KodeRekeningAPBD_WS;
import app.sikd.entity.ws.LaporanOperasionalAkunJenis_WS;
import app.sikd.entity.ws.LaporanOperasionalAkunKelompok_WS;
import app.sikd.entity.ws.LaporanOperasionalAkunObjek_WS;
import app.sikd.entity.ws.LaporanOperasionalAkunUtama_WS;
import app.sikd.entity.ws.LaporanOperasional_WS;
import app.sikd.entity.ws.NeracaAkunJenis_WS;
import app.sikd.entity.ws.NeracaAkunKelompok_WS;
import app.sikd.entity.ws.NeracaAkunObjek_WS;
import app.sikd.entity.ws.NeracaAkunUtama_WS;
import app.sikd.entity.ws.Neraca_WS;
import app.sikd.entity.ws.PerhitunganFihakKetiga_WS;
import app.sikd.entity.ws.PerubahanEkuitasDetail_WS;
import app.sikd.entity.ws.PerubahanEkuitas_WS;
import app.sikd.entity.ws.PerubahanSalDetail_WS;
import app.sikd.entity.ws.PerubahanSal_WS;
import app.sikd.entity.ws.PinjamanDaerah_WS;
import app.sikd.entity.ws.PosLuarBiasaOperasional_WS;
import app.sikd.entity.ws.RealisasiAPBD_WS;
import app.sikd.entity.ws.RealisasiKegiatanAPBD_WS;
import app.sikd.entity.ws.RealisasiKodeRekeningAPBD_WS;
import app.sikd.entity.ws.RincianPerhitunganFihakKetiga_WS;
import app.sikd.entity.ws.RincianPinjamanDaerah_WS;
import app.sikd.entity.ws.Skpd_WS;
import app.sikd.entity.ws.SurplusNonOperasional_WS;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author detra
 */
public interface IWebServiceSQL {

    public long getLastAPBDIndex(Connection conn) throws SQLException;
    public long getLastKegiatanIndex(Connection conn) throws SQLException;
    public long createAPBD(Apbd_WS apbd, short iotype, Connection conn) throws SQLException;
    public short cekStatusDataAPBD(String kodeSatker, short tahun, short kodeData, short jenisCOA, Connection conn) throws SQLException;
    public String getApbdInfo(String kodeSatker, short tahun, short kodeData, short jenisCOA, Connection conn) throws SQLException;
    public void deleteAPBD(String kodeSatker, String kodePemda, short thn, short kodeData, short jenisCOA, Connection conn) throws SQLException;
    public long getAPBDIndex(String kodeSatker, String kodePemda, short thn, short kodeData, String kodeSkpd, short jenisCOA, Connection conn) throws SQLException;
    public long getAPBDIndex(String kodeSatker, String kodePemda, short thn, short kodeData, short jenisCOA, Connection conn) throws SQLException ;
    public String updateAPBD(Apbd_WS apbd, long apbdIndex, Connection conn) throws SQLException;
    public long createKegiatanAPBD(long indexApbd, KegiatanAPBD_WS kegiatan, Connection conn) throws SQLException;
    public void deleteKegiatanAPBD(long indexApbd, String kodePelaksanaProgram, String kodeSKPD, Connection conn) throws SQLException;
    public String createRekeningAPBD(long indexKegiatan, KodeRekeningAPBD_WS rekening, Connection conn) throws SQLException;
    public Apbd_WS getApbd(String kodeSatker, short tahun, short kodeData, short jenisCOA, String userName, String pass, Connection conn) throws SQLException;

    public long getLastRealisasiAPBDIndex(Connection conn) throws SQLException;
    public long getLastRealisasiKegiatanIndex(Connection conn) throws SQLException;
    public short cekStatusDataRealisasiAPBD(String kodeSatker, short tahun, short kodeData, short periode, short jenisCOA, Connection conn) throws SQLException;
    public String getLRAInfo(String kodeSatker, short tahun, short periode, short kodeData, short jenisCOA, Connection conn) throws SQLException;
    public long createRealisasiAPBD(RealisasiAPBD_WS apbd, short iotype, Connection conn) throws SQLException;
    public void updateRealisasiAPBD(long indexLRA, RealisasiAPBD_WS apbd, Connection conn) throws SQLException;
    public void deleteRealisasiAPBD(String kodeSatker, String kodePemda, short thn, short kodeData, short jenisCOA, Connection conn) throws SQLException;
    public void deleteRealisasiAPBDPerPeriode(String kodeSatker, String kodePemda, short thn, short bulan, short kodeData, short jenisCOA, Connection conn) throws SQLException;
    public long getRealisasiAPBDIndexPerPeriode(String kodeSatker, String kodePemda, short thn, short periode, short kodeData, String kodeSkpd, short jenisCOA, Connection conn) throws SQLException;
    public long getRealisasiAPBDIndex(String kodeSatker, String kodePemda, short thn, short kodeData, short jenisCOA, Connection conn) throws SQLException;
    public String updateRealisasiAPBDPerPeriode(RealisasiAPBD_WS apbd, long indexApbd, Connection conn) throws SQLException;
    public long createRealisasiKegiatanAPBD(long indexApbd, RealisasiKegiatanAPBD_WS kegiatan, Connection conn) throws SQLException;    
    public String createRealisasiRekeningAPBD(long indexKegiatan, RealisasiKodeRekeningAPBD_WS rekening, Connection conn) throws SQLException;
    public RealisasiAPBD_WS getRealisasiApbd(String kodeSatker, short tahun, short bulan, short kodeData, short jenisCOA, String userName, String pass, Connection conn) throws SQLException;
    public void deleteRealisasiAPBDPerPeriode(String kodeSatker, String kodePemda, short thn, short periode, short jenisCOA,Connection conn) throws SQLException;
    
    public short cekStatusDataPinjamanDaerah(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public String getPinjamanDaerahInfo(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public long createPinjamanDaerah(PinjamanDaerah_WS pinjaman, short iotype, Connection conn) throws SQLException;
    public String updatePinjamanDaerah(PinjamanDaerah_WS pinjaman, long pinjamanIndex, Connection conn) throws SQLException;
    public void deletePinjamanDaerah(String kodeSatker, short thn, Connection conn) throws SQLException;
    public long getPinjamanDaerahIndex(String kodeSatker, short thn, Connection conn) throws SQLException;
    public long getLastPinjamanDaerahIndex(Connection conn) throws SQLException;
    public String createRincianPinjamanDaerah(RincianPinjamanDaerah_WS rincian, long indexPinjaman, Connection conn) throws SQLException;
    public void deleteRincianPinjamanDaerah(String kodeSatker, short thn, Connection conn) throws SQLException;
    
    public short cekStatusDataPerhitunganFihakKetiga(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public String getPFKInfo(String kodeSatker, short tahun, Connection conn) throws SQLException ;
    public long createPerhitunganFihakKetiga(PerhitunganFihakKetiga_WS pfk, short iotype, Connection conn) throws SQLException;
    public void createRincianPerhitunganFihakKetiga(long pfkIndex, RincianPerhitunganFihakKetiga_WS rincianpfk, Connection conn) throws SQLException;
    public long getPerhitunganFihakKetigaIndex(String kodeSatker, short tahunAnggaran, Connection conn) throws SQLException ;
    public void deletePerhitunganFihakKetiga(String kodeSatker, short year, Connection conn) throws SQLException;
    
    public short cekStatusDataNeraca(String kodeSatker, short tahun, short semester, String judulNeraca, Connection conn) throws SQLException ;
    public String getNeracaInfo(String kodeSatker, short tahun, short semester, String judulNeraca, Connection conn) throws SQLException;
    public long createNeraca(Neraca_WS neraca, short iotype, Connection conn) throws SQLException;
    public long getNeracaIndex(String kodeSatker, short year, short semester, String judulNeraca, Connection conn) throws SQLException;
    public void deleteNeraca(String kodeSatker, short thn, short semester, String judulNeraca, Connection conn) throws SQLException;
    public long createNeracaAkunUtama(long indexNeraca, NeracaAkunUtama_WS akunUtama, Connection conn) throws SQLException;
    public long createNeracaAkunKelompok(long indexAkunUtama, NeracaAkunKelompok_WS akunKelompok, Connection conn) throws SQLException;
    public long createNeracaAkunJenis(long indexAkunKelompok, NeracaAkunJenis_WS akunJenis, Connection conn) throws SQLException;
    public long createNeracaAkunObjek(long indexAkunJenis, NeracaAkunObjek_WS akunObjek, Connection conn) throws SQLException;

    public short cekStatusDataArusKas(String kodeSatker, short tahun, String judulArusKas, Connection conn) throws SQLException;
    public String getArusKasInfo(String kodeSatker, short tahun, String judulArusKas, Connection conn) throws SQLException;
    public long createArusKas(ArusKas_WS arusKas, short iotype, Connection conn) throws SQLException;
    public long getArusKasIndex(String kodeSatker, short year, String judulArusKas, Connection conn) throws SQLException;
    public void deleteArusKas(String kodeSatker, short thn, String judulArusKas, Connection conn) throws SQLException;
    public long createArusKasSaldo(long indexArusKas, ArusKasSaldo_WS arusKasSaldo, Connection conn) throws SQLException;
    public long createArusKeluarInvestasi(long indexArusKas, ArusKeluarInvestasi_WS arusKeluarInvestasi, Connection conn) throws SQLException;
    public long createArusKeluarNonAnggaran(long indexArusKas, ArusKeluarNonAnggaran_WS arusKeluar, Connection conn) throws SQLException;
    public long createArusKeluarOperasi(long indexArusKas, ArusKeluarOperasi_WS arusKeluar, Connection conn) throws SQLException;
    public long createArusKeluarPembiayaan(long indexArusKas, ArusKeluarPembiayaan_WS arusKeluar, Connection conn) throws SQLException;
    public long createArusMasukInvestasi(long indexArusKas, ArusMasukInvestasi_WS arusMasuk, Connection conn) throws SQLException;
    public long createArusMasukNonAnggaran(long indexArusKas, ArusMasukNonAnggaran_WS arusMasuk, Connection conn) throws SQLException;
    public long createArusMasukOperasi(long indexArusKas, ArusMasukOperasi_WS arusMasuk, Connection conn) throws SQLException;
    public long createArusMasukPembiayaan(long indexArusKas, ArusMasukPembiayaan_WS arusMasuk, Connection conn) throws SQLException;
    
    public short cekStatusDataLaporanOperasional(String kodeSatker, short tahun, short triwulan, Connection conn) throws SQLException;
    public String getLOInfo(String kodeSatker, short tahun, short triwulan, Connection conn) throws SQLException;
    public long createLaporanOperasional(LaporanOperasional_WS obj, short iotype, Connection conn) throws SQLException;
    public long getLOIndex(String kodeSatker, short year, short triwulan, Connection conn) throws SQLException;
    public void deleteLO(String kodeSatker, short thn, short triwulan, Connection conn) throws SQLException;
    public long createLOAkunUtama(long indexNeraca, LaporanOperasionalAkunUtama_WS akunUtama, Connection conn) throws SQLException;
    public long createLOAkunKelompok(long indexAkunUtama, LaporanOperasionalAkunKelompok_WS akunKelompok, Connection conn) throws SQLException;
    public long createLOAkunJenis(long indexAkunKelompok, LaporanOperasionalAkunJenis_WS akunJenis, Connection conn) throws SQLException;
    public long createLOAkunObjek(long indexAkunJenis, LaporanOperasionalAkunObjek_WS akunObjek, Connection conn) throws SQLException;
    public long createSurplusNonOP(long indexLO, SurplusNonOperasional_WS surplus, Connection conn) throws SQLException;
    public long createDefisitNonOP(long indexLO, DefisitNonOperasional_WS defisit, Connection conn) throws SQLException;
    public long createLOPosLuarBiasa(long indexLO, PosLuarBiasaOperasional_WS pos, Connection conn) throws SQLException;
    
    public short cekStatusDataPerubahanSal(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public String getPerubahanSalInfo(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public long createPerubahanSal(PerubahanSal_WS obj, short iotype, Connection conn) throws SQLException;
    public String createPerubahanSalDetail(long indexSal, PerubahanSalDetail_WS obj, Connection conn) throws SQLException;
    public String deletePerubahanSal(String kodeSatker, short year, Connection conn) throws SQLException;
    
    public short cekStatusDataPerubahanEkuitas(String kodeSatker, short tahun, Connection conn) throws SQLException ;
    public String getPerubahanEkuitasInfo(String kodeSatker, short tahun, Connection conn) throws SQLException;
    public long createPerubahanEkuitas(PerubahanEkuitas_WS obj, short iotype, Connection conn) throws SQLException;
    public String createPerubahanEkuitasDetail(long indexEkuitas, PerubahanEkuitasDetail_WS obj, Connection conn) throws SQLException;
    public String deletePerubahanEkuitas(String kodeSatker, short year, Connection conn) throws SQLException;
    
    public List<Skpd_WS> getDinasKirimAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws SQLException;
    public void setStatusAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, short statusData, Connection conn) throws SQLException;

}
