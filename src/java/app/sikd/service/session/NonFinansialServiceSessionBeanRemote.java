/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.service.session;

import app.sikd.entity.ws.DaftarPegawai_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import javax.ejb.Remote;

/**
 *
 * @author detra
 */
@Remote
public interface NonFinansialServiceSessionBeanRemote {
    public String inputDaftarPegawai(DaftarPegawai_WS obj) throws SIKDServiceException;
    
}
