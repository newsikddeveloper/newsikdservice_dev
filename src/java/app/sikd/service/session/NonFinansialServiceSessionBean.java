/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.service.session;

import app.sikd.bl.INonFinansialWSBL;
import app.sikd.bl.NonFinansialWSBL;
import app.sikd.dbapi.SIKDConnectionManager;
import app.sikd.entity.ws.DaftarPegawai_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 *
 * @author detra
 */
@Stateless
@Remote({NonFinansialServiceSessionBeanRemote.class})
public class NonFinansialServiceSessionBean implements NonFinansialServiceSessionBeanRemote {

    SIKDConnectionManager cm;
    INonFinansialWSBL bl;
    
    @PostConstruct    
    public void init() {
        cm = new SIKDConnectionManager();
        bl = new NonFinansialWSBL();
    }
    
    private Connection createConnection() throws Exception{
        try {
            return cm.createConnection("sikd.properties");
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex.getCause());
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public String inputDaftarPegawai(DaftarPegawai_WS obj) throws SIKDServiceException{
        String result = null;
        Connection conn = null;
        try {        
//            bl = new WebServiceBL();
            conn = createConnection();
            if (obj != null) {
                    result = bl.createDaftarPegawai(obj, conn);
            }
            else result = "tidak ada data yang di masukkan";
        } catch (Exception ex) {
            Logger.getLogger(NonFinansialServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        finally{
            if( conn != null ) try {
                conn.close();                
            } catch (SQLException ex) {
                Logger.getLogger(NonFinansialServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
}
