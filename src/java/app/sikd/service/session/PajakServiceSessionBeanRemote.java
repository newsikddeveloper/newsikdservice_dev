/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.service.session;

import app.sikd.entity.ws.BPHTB_WS;
import app.sikd.entity.ws.DTH_WS;
import app.sikd.entity.ws.Hiburan_WS;
import app.sikd.entity.ws.Hotel_WS;
import app.sikd.entity.ws.IMB_WS;
import app.sikd.entity.ws.IzinUsaha_WS;
import app.sikd.entity.ws.Kendaraan_WS;
import app.sikd.entity.ws.PajakDanRetribusi_WS;
import app.sikd.entity.ws.Restoran_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author detra
 */
@Remote
public interface PajakServiceSessionBeanRemote extends Serializable{

    public String inputDTH(List<DTH_WS> objects, short iotype) throws SIKDServiceException;

    public String inputDTHPerPeriode(DTH_WS obj, short iotype) throws SIKDServiceException;

    public String inputKendaraan(Kendaraan_WS object) throws SIKDServiceException;

    public String inputHotel(Hotel_WS object) throws SIKDServiceException;

    public String inputHiburan(Hiburan_WS object) throws SIKDServiceException;

    public String inputIzinUsaha(IzinUsaha_WS object) throws SIKDServiceException;

    public String inputBPHTB(BPHTB_WS object) throws SIKDServiceException;

    public String inputIMB(IMB_WS object) throws SIKDServiceException;

    public String inputRestoran(Restoran_WS object) throws SIKDServiceException;
    
    public String inputPajakDanRetribusiDaerah(PajakDanRetribusi_WS object) throws SIKDServiceException;

    public String getDTHInfo(String kodeSatker, short year, short periode) throws SIKDServiceException;
    
}
