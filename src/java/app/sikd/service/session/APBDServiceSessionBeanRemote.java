/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.service.session;

import app.sikd.entity.ws.Apbd_WS;
import app.sikd.entity.ws.ArusKas_WS;
import app.sikd.entity.ws.LaporanOperasional_WS;
import app.sikd.entity.ws.Neraca_WS;
import app.sikd.entity.ws.PerhitunganFihakKetiga_WS;
import app.sikd.entity.ws.PerubahanEkuitas_WS;
import app.sikd.entity.ws.PerubahanSal_WS;
import app.sikd.entity.ws.PinjamanDaerah_WS;
import app.sikd.entity.ws.RealisasiAPBD_WS;
import app.sikd.entity.ws.Skpd_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author detra
 */
@Remote
public interface APBDServiceSessionBeanRemote {

    String inputAPBD(Apbd_WS apbd, short iotype)throws SIKDServiceException;
    public String inputAPBDperSKPD(Apbd_WS apbd, short iotype) throws SIKDServiceException;
    public List<Skpd_WS> getDinasKirimAPBD(short year, String kodeSatker, short kodeData, short jenisCOA) throws Exception;
    public String updateStatusDtaAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, short statusData) throws Exception;
    public Apbd_WS getApbd(String kodeSatker, short tahun, short kodeData, short jenisCOA, String userName, String pass) throws Exception;

    public String inputRealisasiAPBD(RealisasiAPBD_WS realisasiApbd, short iotype) throws SIKDServiceException;
    public String inputRealisasiAPBDperPeriode(RealisasiAPBD_WS realisasiApbd, short iotype) throws SIKDServiceException;
    public RealisasiAPBD_WS getRealisasiApbd(String kodeSatker, short tahun, short bulan, short kodeData, short jenisCOA, String userName, String pass) throws Exception;
    
    public String inputPerhitunganFihakKetiga(PerhitunganFihakKetiga_WS pfk, short iotype) throws SIKDServiceException;
    public String inputPinjamanDaerah(PinjamanDaerah_WS pinjaman, short iotype) throws SIKDServiceException;
    
    public String inputNeracaPerSemester(Neraca_WS neraca, short iotype)throws SIKDServiceException;
    public String inputArusKas(ArusKas_WS arusKas, short iotype) throws SIKDServiceException;

    public String inputPerubahanEkuitas(PerubahanEkuitas_WS ekuitas, short iotype) throws SIKDServiceException;

    public String inputPerubahanSal(PerubahanSal_WS sal, short iotype) throws SIKDServiceException;

    public String inputLaporanOperasionalPerTriwulan(LaporanOperasional_WS lo, short iotype) throws SIKDServiceException;

    public String getApbdInfo(short year, String kodeSatker, short kodeData, short jenisCOA) throws Exception;

    public String getRealisasiApbdInfo(short year, String kodeSatker, short periode, short kodeData, short jenisCOA) throws Exception;

    public String getPinjamanDaerahInfo(short year, String kodeSatker) throws Exception;

    public String getPerhitunganPihakKetigaInfo(short year, String kodeSatker) throws Exception;

    public String getNeracaInfo(short year, short semester, String kodeSatker, String judulNeraca) throws Exception;

    public String getArusKasInfo(short year, String kodeSatker, String judulArusKas) throws Exception;

    public String getLaporanOperasionalInfo(short year, short triwulan, String kodeSatker) throws Exception;

    public String getPerubahanSalInfo(short year, String kodeSatker) throws Exception;

    public String getPerubahanEkuitasInfo(short year, String kodeSatker) throws Exception;
    
}
