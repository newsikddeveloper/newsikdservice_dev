/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.service.session;

import app.sikd.bl.IPajakWSBL;
import app.sikd.bl.PajakWSBL;
import app.sikd.dbapi.SIKDConnectionManager;
import app.sikd.entity.ws.BPHTB_WS;
import app.sikd.entity.ws.DTH_WS;
import app.sikd.entity.ws.Hiburan_WS;
import app.sikd.entity.ws.Hotel_WS;
import app.sikd.entity.ws.IMB_WS;
import app.sikd.entity.ws.IzinUsaha_WS;
import app.sikd.entity.ws.Kendaraan_WS;
import app.sikd.entity.ws.PajakDanRetribusi_WS;
import app.sikd.entity.ws.Restoran_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 *
 * @author detra
 */
@Stateless
@Remote({PajakServiceSessionBeanRemote.class})
public class PajakServiceSessionBean implements PajakServiceSessionBeanRemote {
    Connection conn;
    SIKDConnectionManager cm;
    IPajakWSBL bl;
    
        
    public PajakServiceSessionBean() {
        cm = new SIKDConnectionManager();
        bl = new PajakWSBL();     
        try {
            if(conn==null || conn.isClosed()){
                if(conn==null) System.out.println("conn null");
                else System.out.println("conn.colse");
                conn = createConnection();
            }
            System.out.println("buat koneksi header");
        } catch (Exception e) {
            System.out.println("gagal Koneksi");
        }        
    }
    
    private Connection createConnection() throws Exception{
        try {
            return cm.createConnection("sikd.properties");
        } catch (Exception ex) {
            Logger.getLogger(PajakServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex.getCause());
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public String inputDTH(List<DTH_WS> objects, short iotype) throws SIKDServiceException{
        String result = null;
        Connection coon = null;
        try {        
            coon = createConnection();
            if (objects != null) {
                    result = bl.createDTH(objects, iotype, coon);
            }
            else result = "tidak ada data yang di masukkan";
        } catch (Exception ex) {
            Logger.getLogger(PajakServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        finally{
            if( coon != null ) try {
                coon.close();                
            } catch (SQLException ex) {
                Logger.getLogger(PajakServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    @Override
    public String inputDTHPerPeriode(DTH_WS obj, short iotype) throws SIKDServiceException{
        String result = null;
        
        try {        
            if(conn==null || conn.isClosed()){ 
                System.out.println("buat koneksi");
                conn = createConnection();
            }
            
            if (obj != null) {
                    result = bl.createDTHperPeriode(obj, iotype, conn);
            }
            else result = "tidak ada data yang di masukkan";
        } catch (Exception ex) {
//            Logger.getLogger(PajakServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        finally{
            if( conn != null ) try {
                conn.close();                
            } catch (SQLException ex) {
                Logger.getLogger(PajakServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    /**
     *
     * @param kodeSatker
     * @param year
     * @param periode
     * @return
     * @throws SIKDServiceException
     */
    @Override
    public String getDTHInfo(String kodeSatker, short year, short periode) throws SIKDServiceException{
        String result = null;
        Connection conn = null;
        try {        
            conn = createConnection();
            result = bl.getDTHInfo(kodeSatker, year, periode, conn);
        } catch (Exception ex) {
            Logger.getLogger(PajakServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        finally{
            if( conn != null ) try {
                conn.close();                
            } catch (SQLException ex) {
                Logger.getLogger(PajakServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    @Override
    public String inputKendaraan(Kendaraan_WS object) throws SIKDServiceException{
        String result = null;
        Connection conn = null;
        try {        
            conn = createConnection();
            if (object != null) {
                    result = bl.createKendaraan(object, conn);
            }
            else result = "tidak ada data yang di masukkan";
        } catch (Exception ex) {
            Logger.getLogger(PajakServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        finally{
            if( conn != null ) try {
                conn.close();                
            } catch (SQLException ex) {
                Logger.getLogger(PajakServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    @Override
    public String inputHotel(Hotel_WS object) throws SIKDServiceException{
        String result = null;
        Connection conn = null;
        try {        
            conn = createConnection();
            if (object != null) {
                    result = bl.createHotel(object, conn);
            }
            else result = "tidak ada data yang di masukkan";
        } catch (Exception ex) {
            Logger.getLogger(PajakServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        finally{
            if( conn != null ) try {
                conn.close();                
            } catch (SQLException ex) {
                Logger.getLogger(PajakServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    @Override
    public String inputHiburan(Hiburan_WS object) throws SIKDServiceException{
        String result = null;
        Connection conn = null;
        try {        
            conn = createConnection();
            if (object != null) {
                    result = bl.createHiburan(object, conn);
            }
            else result = "tidak ada data yang di masukkan";
        } catch (Exception ex) {
            Logger.getLogger(PajakServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        finally{
            if( conn != null ) try {
                conn.close();                
            } catch (SQLException ex) {
                Logger.getLogger(PajakServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    @Override
    public String inputIzinUsaha(IzinUsaha_WS object) throws SIKDServiceException{
        String result = null;
        Connection conn = null;
        try {        
            conn = createConnection();
            if (object != null) {
                    result = bl.createIzinUsaha(object, conn);
            }
            else result = "tidak ada data yang di masukkan";
        } catch (Exception ex) {
            Logger.getLogger(PajakServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        finally{
            if( conn != null ) try {
                conn.close();                
            } catch (SQLException ex) {
                Logger.getLogger(PajakServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    @Override
    public String inputBPHTB(BPHTB_WS object) throws SIKDServiceException{
        String result = null;
        Connection conn = null;
        try {        
            conn = createConnection();
            if (object != null) {
                    result = bl.createBPHTB(object, conn);
            }
            else result = "tidak ada data yang di masukkan";
        } catch (Exception ex) {
            Logger.getLogger(PajakServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        finally{
            if( conn != null ) try {
                conn.close();                
            } catch (SQLException ex) {
                Logger.getLogger(PajakServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    @Override
    public String inputIMB(IMB_WS object) throws SIKDServiceException{
        String result = null;
        Connection conn = null;
        try {        
            conn = createConnection();
            if (object != null) {
                    result = bl.createIMB(object, conn);
            }
            else result = "tidak ada data yang di masukkan";
        } catch (Exception ex) {
            Logger.getLogger(PajakServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        finally{
            if( conn != null ) try {
                conn.close();                
            } catch (SQLException ex) {
                Logger.getLogger(PajakServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    @Override
    public String inputRestoran(Restoran_WS object) throws SIKDServiceException{
        String result = null;
        Connection conn = null;
        try {        
            conn = createConnection();
            if (object != null) {
                    result = bl.createRestoran(object, conn);
            }
            else result = "tidak ada data yang di masukkan";
        } catch (Exception ex) {
            Logger.getLogger(PajakServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        finally{
            if( conn != null ) try {
                conn.close();                
            } catch (SQLException ex) {
                Logger.getLogger(PajakServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    @Override
    public String inputPajakDanRetribusiDaerah(PajakDanRetribusi_WS object) throws SIKDServiceException{
        String result = null;
        Connection conn = null;
        try {        
            conn = createConnection();
            if (object != null) {
                    result = bl.createPajakDanRetribusiDaerah(object, conn);
            }
            else result = "tidak ada data yang di masukkan";
        } catch (Exception ex) {
            Logger.getLogger(PajakServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        finally{
            if( conn != null ) try {
                conn.close();                
            } catch (SQLException ex) {
                Logger.getLogger(PajakServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
}
