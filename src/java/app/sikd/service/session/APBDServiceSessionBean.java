/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.service.session;

import app.sikd.bl.IWebServiceBL;
import app.sikd.bl.WebServiceBL;
import app.sikd.dbapi.SIKDConnectionManager;
import app.sikd.entity.ws.Apbd_WS;
import app.sikd.entity.ws.ArusKas_WS;
import app.sikd.entity.ws.LaporanOperasional_WS;
import app.sikd.entity.ws.Neraca_WS;
import app.sikd.entity.ws.PerhitunganFihakKetiga_WS;
import app.sikd.entity.ws.PerubahanEkuitas_WS;
import app.sikd.entity.ws.PerubahanSal_WS;
import app.sikd.entity.ws.PinjamanDaerah_WS;
import app.sikd.entity.ws.RealisasiAPBD_WS;
import app.sikd.entity.ws.Skpd_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 *
 * @author detra
 */
@Stateless
@Remote({APBDServiceSessionBeanRemote.class})
public class APBDServiceSessionBean implements APBDServiceSessionBeanRemote {
    SIKDConnectionManager cm;
    IWebServiceBL bl;
    
    public APBDServiceSessionBean(){
        cm = new SIKDConnectionManager();
        bl = new WebServiceBL();
    }
    
//     @PostConstruct
//    public void init(){
//        cm = new SIKDConnectionManager();        
//        bl = new WebServiceBL();
//    }
    
    private Connection createConnection() throws Exception{
        try {
            return cm.createConnection("sikd.properties");
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex.getCause());
            throw new Exception(ex.getMessage());
        }
    }

    /**
     *
     * @param apbd
     * @return
     * @throws SIKDServiceException
     */
    @Override
    public String inputAPBD(Apbd_WS apbd, short iotype ) throws SIKDServiceException{
        String result = null;
        Connection conn = null;
        try {        
            bl = new WebServiceBL();
            conn = createConnection();
            if (apbd != null) {
                    result = bl.createAPBD(apbd, iotype, conn);
            }
            else result = "tidak ada data yang di masukkan";
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    /**
     *
     * @param apbd
     * @return
     * @throws SIKDServiceException
     */
    @Override
    public String inputAPBDperSKPD(Apbd_WS apbd, short iotype) throws SIKDServiceException{
        String result = null;
        Connection conn = null;
        try {   
//            bl = new WebServiceBL();
            conn = createConnection();
            if (apbd != null) {                
                    result = bl.createAPBDPerSKPD(apbd, iotype, conn);
            }
            else result = "tidak ada data APBD yang dapat di masukkan";
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex.getCause());
            result = ex.getMessage();
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    
    @Override
    public List<Skpd_WS> getDinasKirimAPBD(short year, String kodeSatker, short kodeData, short jenisCOA) throws Exception{
        List<Skpd_WS> result = null;
        Connection conn = null;
        try {   
//            bl = new WebServiceBL();
            conn = createConnection();
            return result = bl.getDinasKirimAPBD(year, kodeSatker, kodeData, jenisCOA, conn);
            
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex.getCause());            
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    /**
     *
     * @param year
     * @param kodeSatker
     * @param kodeData
     * @param jenisCOA
     * @return
     * @throws Exception
     */
    @Override
    public String getApbdInfo(short year, String kodeSatker, short kodeData, short jenisCOA) throws Exception{
        String result = null;
        Connection conn = null;
        try {   
            conn = createConnection();
            result = bl.getApbdInfo(year, kodeSatker, kodeData, jenisCOA, conn);
            
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex.getCause());            
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    /**
     *
     * @param kodeSatker
     * @param tahun
     * @param kodeData
     * @param jenisCOA
     * @param userName
     * @param pass
     * @return
     * @throws Exception
     */
    @Override
    public Apbd_WS getApbd(String kodeSatker, short tahun, short kodeData, short jenisCOA, String userName, String pass ) throws Exception{
        Apbd_WS result = null;
        Connection conn = null;
        try {
            conn = createConnection();
            result = bl.getApbd(kodeSatker, tahun, kodeData, jenisCOA, userName, pass, conn);
            
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex.getCause());            
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    
    @Override
    public String updateStatusDtaAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, short statusData) throws Exception{
        String result = "Sukses merubah status data APBD";
        Connection conn = null;
        try {   
            conn = createConnection();
            bl.updateStatusDataAPBD(year, kodeSatker, kodeData, jenisCOA, statusData, conn);
            
        } catch (Exception ex) {
            result = ex.getMessage();
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex.getCause());            
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    /**
     *
     * @param realisasiApbd
     * @return
     * @throws SIKDServiceException
     */
    @Override
    public String inputRealisasiAPBDperPeriode(RealisasiAPBD_WS realisasiApbd, short iotype) throws SIKDServiceException{
        String result = null;
        Connection conn = null;
        try {        
            conn = createConnection();
            if (realisasiApbd != null) {
                    result = bl.createRealisasiAPBDPerPeriode(realisasiApbd, iotype, conn);
            }
            else result = "tidak ada data Realisasi APBD yang dapat di masukkan";
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    /**
     *
     * @param realisasiApbd
     * @return
     * @throws SIKDServiceException
     */
    @Override
    public String inputRealisasiAPBD(RealisasiAPBD_WS realisasiApbd, short iotype) throws SIKDServiceException{
        String result = null;
        Connection conn = null;
        try {        
            conn = createConnection();
            if (realisasiApbd != null) {
                    result = bl.createRealisasiAPBD(realisasiApbd, iotype, conn);
            }
            else result = "tidak ada data Realisasi APBD yang dapat di masukkan";
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    /**
     *
     * @param year
     * @param kodeSatker
     * @param periode
     * @param kodeData
     * @param jenisCOA
     * @return
     * @throws Exception
     */
    @Override
    public String getRealisasiApbdInfo(short year, String kodeSatker, short periode, short kodeData, short jenisCOA) throws Exception{
        String result = null;
        Connection conn = null;
        try {   
            conn = createConnection();
            return result = bl.getRealisasiApbdInfo(year, kodeSatker, periode, kodeData, jenisCOA, conn);
            
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex.getCause());            
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    public RealisasiAPBD_WS getRealisasiApbd(String kodeSatker, short tahun, short bulan, short kodeData, short jenisCOA, String userName, String pass) throws Exception{
        RealisasiAPBD_WS result = null;
        Connection conn = null;
        try {   
            conn = createConnection();
            return result = bl.getRealisasiApbd(kodeSatker, tahun, bulan, kodeData, jenisCOA, userName, pass, conn);
            
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex.getCause());            
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    /**
     *
     * @param pfk
     * @return
     * @throws SIKDServiceException
     */
    @Override
    public String inputPerhitunganFihakKetiga(PerhitunganFihakKetiga_WS pfk, short iotype) throws SIKDServiceException{
        String result = null;
        Connection conn = null;
        try {        
            bl = new WebServiceBL();
            conn = createConnection();
            if (pfk != null) {
                    result = bl.createPerhitunganFihakKetiga(pfk, iotype, conn);
            }
            else result = "tidak ada data yang di masukkan";
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    /**
     *
     * @param year
     * @param kodeSatker
     * @return
     * @throws Exception
     */
    @Override
    public String getPerhitunganPihakKetigaInfo(short year, String kodeSatker) throws Exception{
        String result = null;
        Connection conn = null;
        try {   
            conn = createConnection();
            return result = bl.getPerhitunganFihakKetigaInfo(year, kodeSatker, conn);
            
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex.getCause());            
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    /**
     *
     * @param pinjaman
     * @return
     * @throws SIKDServiceException
     */
    @Override
    public String inputPinjamanDaerah(PinjamanDaerah_WS pinjaman, short iotype) throws SIKDServiceException{
        String result = null;
        Connection conn = null;
        try {        
            bl = new WebServiceBL();
            conn = createConnection();
            if (pinjaman != null) {
                    result = bl.createPinjamanDaerah(pinjaman, iotype, conn);
            }
            else result = "tidak ada data yang di masukkan";
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    /**
     *
     * @param year
     * @param kodeSatker
     * @return
     * @throws Exception
     */
    @Override
    public String getPinjamanDaerahInfo(short year, String kodeSatker) throws Exception{
        String result = null;
        Connection conn = null;
        try {   
            conn = createConnection();
            return result = bl.getPinjamanDaerahInfo(year, kodeSatker, conn);
            
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex.getCause());            
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    /**
     *
     * @param neraca
     * @return
     * @throws SIKDServiceException
     */
    @Override
    public String inputNeracaPerSemester(Neraca_WS neraca, short iotype) throws SIKDServiceException{
        String result = null;
        Connection conn = null;
        try {        
            bl = new WebServiceBL();
            conn = createConnection();
            if (neraca != null) {
                    result = bl.createNeracaPerSemester(neraca, iotype, conn);
            }
            else result = "tidak ada data yang di masukkan";
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    /**
     *
     * @param year
     * @param semester
     * @param kodeSatker
     * @param judulNeraca
     * @return
     * @throws Exception
     */
    @Override
    public String getNeracaInfo(short year, short semester, String kodeSatker, String judulNeraca) throws Exception{
        String result = null;
        Connection conn = null;
        try {   
            conn = createConnection();
            return result = bl.getNeracaInfo(year, semester, kodeSatker, judulNeraca, conn);
            
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex.getCause());            
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    
    /**
     *
     * @param arusKas
     * @return
     * @throws SIKDServiceException
     */
    @Override
    public String inputArusKas(ArusKas_WS arusKas, short iotype) throws SIKDServiceException{
        String result = null;
        Connection conn = null;
        try {        
            bl = new WebServiceBL();
            conn = createConnection();
            if (arusKas != null) {
                    result = bl.createArusKas(arusKas, iotype, conn);
            }
            else result = "tidak ada data yang di masukkan";
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    /**
     *
     * @param year
     * @param kodeSatker
     * @param judulArusKas
     * @return
     * @throws Exception
     */
    @Override
    public String getArusKasInfo(short year, String kodeSatker, String judulArusKas) throws Exception{
        String result = null;
        Connection conn = null;
        try {   
            conn = createConnection();
            return result = bl.getArusKasInfo(year, kodeSatker, judulArusKas, conn);
            
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex.getCause());            
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    
    /**
     *
     * @param lo
     * @return
     * @throws SIKDServiceException
     */
    @Override
    public String inputLaporanOperasionalPerTriwulan(LaporanOperasional_WS lo, short iotype) throws SIKDServiceException{
        String result = null;
        Connection conn = null;
        try {        
            bl = new WebServiceBL();
            conn = createConnection();
            if (lo != null) {
                    result = bl.createLaporanOperasionalPerTriwulan(lo, iotype, conn);
            }
            else result = "tidak ada data yang di masukkan";
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    /**
     *
     * @param year
     * @param triwulan
     * @param kodeSatker
     * @return
     * @throws Exception
     */
    @Override
    public String getLaporanOperasionalInfo(short year, short triwulan, String kodeSatker) throws Exception{
        String result = null;
        Connection conn = null;
        try {   
            conn = createConnection();
            return result = bl.getLaporanOperasionalInfo(year, triwulan, kodeSatker, conn);
            
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex.getCause());            
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    
    /**
     *
     * @param sal
     * @return
     * @throws SIKDServiceException
     */
    @Override
    public String inputPerubahanSal(PerubahanSal_WS sal, short iotype) throws SIKDServiceException{
        String result = null;
        Connection conn = null;
        try {        
            bl = new WebServiceBL();
            conn = createConnection();
            if (sal != null) {
                    result = bl.createPerubahanSal(sal, iotype, conn);
            }
            else result = "tidak ada data yang di masukkan";
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    /**
     *
     * @param year
     * @param kodeSatker
     * @return
     * @throws Exception
     */
    @Override
    public String getPerubahanSalInfo(short year, String kodeSatker) throws Exception{
        String result = null;
        Connection conn = null;
        try {   
            conn = createConnection();
            return result = bl.getPerubahanSalInfo(year, kodeSatker, conn);
            
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex.getCause());            
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    
    /**
     *
     * @param ekuitas
     * @return
     * @throws SIKDServiceException
     */
    @Override
    public String inputPerubahanEkuitas(PerubahanEkuitas_WS ekuitas, short iotype) throws SIKDServiceException{
        String result = null;
        Connection conn = null;
        try {        
            bl = new WebServiceBL();
            conn = createConnection();
            if (ekuitas != null) {
                    result = bl.createPerubahanEkuitas(ekuitas, iotype, conn);
            }
            else result = "tidak ada data yang di masukkan";
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    /**
     *
     * @param year
     * @param kodeSatker
     * @return
     * @throws Exception
     */
    @Override
    public String getPerubahanEkuitasInfo(short year, String kodeSatker) throws Exception{
        String result = null;
        Connection conn = null;
        try {   
            conn = createConnection();
            return result = bl.getPerubahanEkuitasInfo(year, kodeSatker, conn);
            
        } catch (Exception ex) {
            Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex.getCause());            
        }
        finally{
            if( conn != null ) try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(APBDServiceSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
}
