/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.service;

import app.sikd.entity.ws.PerubahanEkuitas_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.service.session.APBDServiceSessionBeanRemote;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.naming.Context;

/**
 *
 * @author sora
 */
@WebService(serviceName = "PerubahanEkuitasService")
@Stateless()
public class PerubahanEkuitasService {

    /**
     * Web service operation
     *
     * @param ekuitas
     * @return
     */
    @WebMethod(operationName = "inputPerubahanEkuitas")
    public String inputPerubahanEkuitas(@WebParam(name = "ekuitas") PerubahanEkuitas_WS ekuitas) {
        String result;
        String namaP = "";
        try {
            Context ctxLogin = ServiceServerUtil.getLoginContext();            
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            APBDServiceSessionBeanRemote session = (APBDServiceSessionBeanRemote) ctxOffice.lookup("APBDServiceSessionBean/remote");
            NewLoginSessionBeanRemote logSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            if (ekuitas != null) {
                String n = ekuitas.getUserName();
                String p = ekuitas.getPassword();
                namaP = ekuitas.getNamaPemda();
                System.out.println("Koneksi Awal input Perubahan Ekuitas : " + namaP);
//                String satker = logSession.getKodeSatker(n, p);
//                String kodePemda = logSession.getKodePemda(satker);
                String[] satkerpemda = logSession.getKodeSatkerKodePemda(n, p);
                String satker = "";
                String kodePemda = "";
                if(satkerpemda!=null && satkerpemda.length==2){
                    satker = satkerpemda[0].trim();
                    kodePemda = satkerpemda[1].trim();
                }
                if(kodePemda!=null) kodePemda = kodePemda.trim();
                if (satker.equals(ekuitas.getKodeSatker())) {
                    if( kodePemda.equals(ekuitas.getKodePemda())){
                        result = session.inputPerubahanEkuitas(ekuitas, (short)1);
                    }
                    else result = "Gagal transfer data\n Kode Pemda " + ekuitas.getKodePemda() + " tidak sesuai untuk Kode Satker " + satker;
                } else {
                    result = "Gagal transfer data\n Password untuk pemda tidak sesuai";
                }
            } else {
                result = "tidak ada data yang di masukkan";
            }
        } catch (Exception ex) {
            Logger.getLogger(PerubahanEkuitasService.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        System.out.println("Koneksi Akhir input Perubahan Ekuitas : " + namaP);
        return result;
    }

    /**
     * Web service operation
     * @param kodeSatker
     * @param tahun
     * @return 
     * @throws app.sikd.entity.ws.fault.SIKDServiceException
     */
    @WebMethod(operationName = "ambilPerubahanEkuitasInfo")
    public String ambilPerubahanEkuitasInfo(@WebParam(name = "kodeSatker") String kodeSatker, @WebParam(name = "tahun") short tahun) throws SIKDServiceException {
        String result;
        try {   
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            APBDServiceSessionBeanRemote apbdSession = (APBDServiceSessionBeanRemote)ctxOffice.lookup("APBDServiceSessionBean/remote");
            result = apbdSession.getPerubahanEkuitasInfo(tahun, kodeSatker);
        }
        catch (Exception exc) {
            Logger.getLogger(PerubahanEkuitasService.class.getName()).log(Level.SEVERE, null, exc);
            result = exc.getMessage();
        }
        return result;
    }
}
