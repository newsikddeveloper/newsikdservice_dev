/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.service;

import app.sikd.entity.ws.LaporanOperasional_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.service.session.APBDServiceSessionBeanRemote;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.naming.Context;

/**
 *
 * @author sora
 */
@WebService(serviceName = "LaporanOperasionalService")
@Stateless()
public class LaporanOperasionalService {



    /**
     * Web service operation
     * @param laporanOperasional
     * @return 
     * @throws app.sikd.entity.ws.fault.SIKDServiceException
     */
    @WebMethod(operationName = "inputLaporanOperasionalPerTriwulan")
    public String inputLaporanOperasionalPerTriwulan(@WebParam(name = "laporanOperasional") LaporanOperasional_WS laporanOperasional) throws SIKDServiceException {
        String result;
        String namaP = "";
        try {
            Context ctxLogin = ServiceServerUtil.getLoginContext();            
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            APBDServiceSessionBeanRemote session = (APBDServiceSessionBeanRemote) ctxOffice.lookup("APBDServiceSessionBean/remote");
            NewLoginSessionBeanRemote logSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            if (laporanOperasional != null) {
                String n = laporanOperasional.getUserName();
                String p = laporanOperasional.getPassword();
                namaP = laporanOperasional.getNamaPemda();
                System.out.println("Koneksi Awal input Laporan Operasional : " + namaP);
//                String satker = logSession.getKodeSatker(n, p);
//                String kodePemda = logSession.getKodePemda(satker);
                String[] satkerpemda = logSession.getKodeSatkerKodePemda(n, p);
                String satker = "";
                String kodePemda = "";
                if(satkerpemda!=null && satkerpemda.length==2){
                    satker = satkerpemda[0].trim();
                    kodePemda = satkerpemda[1].trim();
                }
                if(kodePemda!=null) kodePemda = kodePemda.trim();
                if (satker.equals(laporanOperasional.getKodeSatker())) {
                    if( kodePemda.equals(laporanOperasional.getKodePemda())){
                        result = session.inputLaporanOperasionalPerTriwulan(laporanOperasional, (short)1);
                    }
                    else result = "Gagal transfer data\n Kode Pemda " + laporanOperasional.getKodePemda() + " tidak sesuai untuk Kode Satker "+ satker;
                } else {
                    result = "Gagal transfer data\n Password untuk pemda tidak sesuai";
                }
            } else {
                result = "tidak ada data yang di masukkan";
            }
        } catch (Exception ex) {
            Logger.getLogger(LaporanOperasionalService.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        System.out.println("Koneksi Akhir input Laporan Operasional : " + namaP);
        return result;
    }

    /**
     * Web service operation
     * @param kodeSatker
     * @param tahun
     * @param triwulan
     * @return 
     * @throws app.sikd.entity.ws.fault.SIKDServiceException 
     */
    @WebMethod(operationName = "ambilLaporanOperasionalInfo")
    public String ambilLaporanOperasionalInfo(@WebParam(name = "kodeSatker") String kodeSatker, @WebParam(name = "tahun") short tahun, @WebParam(name = "triwulan") short triwulan) throws SIKDServiceException {
        String result;
        try {   
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            APBDServiceSessionBeanRemote apbdSession = (APBDServiceSessionBeanRemote)ctxOffice.lookup("APBDServiceSessionBean/remote");
            result = apbdSession.getLaporanOperasionalInfo(tahun, triwulan, kodeSatker);
        }
        catch (Exception exc) {
            Logger.getLogger(LaporanOperasionalService.class.getName()).log(Level.SEVERE, null, exc);
            result = exc.getMessage();
        }
        return result;
    }

}
