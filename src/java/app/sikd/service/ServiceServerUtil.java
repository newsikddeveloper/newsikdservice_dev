/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.service;

import app.sikd.util.PropertiesLoader;
import java.io.Serializable;
import java.util.Properties;
import javax.naming.Context;
import javax.naming.InitialContext;

/**
 *
 * @author sora
 */
public class ServiceServerUtil implements Serializable{
    public static Context getLoginContext() throws Exception {
        Properties pr = PropertiesLoader.loadProperties("sikd.properties");
        Properties prop = new Properties();
        if (pr.getProperty("aasurl") != null && !pr.getProperty("aasurl").trim().equals("")) {
            prop.put(InitialContext.PROVIDER_URL, pr.getProperty("aasurl"));
        }

        Context ctx = new InitialContext(prop);
        return ctx;
    }
    public static Context getOfficeContext() throws Exception {
        Properties pr = PropertiesLoader.loadProperties("sikd.properties");
        Properties prop = new Properties();
        if (pr.getProperty("officeurl") != null && !pr.getProperty("officeurl").trim().equals("")) {
            prop.put(InitialContext.PROVIDER_URL, pr.getProperty("officeurl"));
        }

        Context ctx = new InitialContext(prop);
        return ctx;
    }
    
}
