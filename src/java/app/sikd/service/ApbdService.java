/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.service;

import app.sikd.entity.ws.Apbd_WS;
import app.sikd.entity.ws.Skpd_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.service.session.APBDServiceSessionBeanRemote;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.naming.Context;

/**
 *
 * @author sora
 */
@WebService(serviceName = "ApbdService")
@Stateless
public class ApbdService {



    /**
     * Web service operation
     * @param apbd
     * @return 
     * @throws app.sikd.entity.ws.fault.SIKDServiceException 
     */
    @WebMethod(operationName = "inputAPBDperSKPD")
    @SuppressWarnings("null")
    public String inputAPBDperSKPD(@WebParam(name = "apbd") Apbd_WS apbd) throws SIKDServiceException{
        String result;
        String namaP = "";
        try {
            Context ctxLogin = ServiceServerUtil.getLoginContext();
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            APBDServiceSessionBeanRemote apbdSession = (APBDServiceSessionBeanRemote) ctxOffice.lookup("APBDServiceSessionBean/remote");
            NewLoginSessionBeanRemote logSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            if (apbd != null) {
                String n = apbd.getUserName();
                String p = apbd.getPassword();
                namaP = apbd.getNamaPemda();
                System.out.println("Koneksi Awal input APBD per skpd: " + namaP);
                String[] satkerpemda = logSession.getKodeSatkerKodePemda(n, p);
                String satker = "";
                String kodePemda = "";
                if(satkerpemda!=null && satkerpemda.length==2){
                    satker = satkerpemda[0].trim();
                    kodePemda = satkerpemda[1].trim();
                }
//                String kodePemda = logSession.getKodePemda(satker);
                System.out.println("koneksi Awal input Apbd per skpd satker " + n);
                if(kodePemda!=null) kodePemda = kodePemda.trim();                
                if (satker.equals(apbd.getKodeSatker())) {
                    if( kodePemda.equals(apbd.getKodePemda()) ){
                        result = apbdSession.inputAPBDperSKPD(apbd, (short)1);
                    }
                    else result = "Gagal transfer data\n Kode Pemda " + apbd.getKodePemda() + " tidak sesuai untuk Kode Satker " + satker;
                } else {
                    result = "Gagal transfer data\n Password untuk pemda tidak sesuai";
                }
            } else {
                result = "tidak ada data yang di masukkan";
            }
        } catch (Exception ex) {
            Logger.getLogger(ApbdService.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        System.out.println("Koneksi Akhir input APBD per skpd: " + namaP);
        return result;
    }

    /**
     * Web service operation
     * @param apbd
     * @return 
     * @throws app.sikd.entity.ws.fault.SIKDServiceException 
     */
    @WebMethod(operationName = "inputSeluruhApbd")
    @SuppressWarnings("null")
    public String inputSeluruhApbd(@WebParam(name = "apbd") Apbd_WS apbd) throws SIKDServiceException {
        String result;
        String namaP = "";
        try {
            Context ctxLogin = ServiceServerUtil.getLoginContext();            
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            APBDServiceSessionBeanRemote apbdSession = (APBDServiceSessionBeanRemote) ctxOffice.lookup("APBDServiceSessionBean/remote");
            NewLoginSessionBeanRemote logSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            if (apbd != null) {
                String n = apbd.getUserName();
                String p = apbd.getPassword();
                namaP = apbd.getNamaPemda();
                System.out.println("Koneksi Awal input seluruh APBD: " + namaP);
                String[] satkerpemda = logSession.getKodeSatkerKodePemda(n, p);
                String satker = "";
                String kodePemda = "";
                if(satkerpemda!=null && satkerpemda.length==2){
                    satker = satkerpemda[0].trim();
                    kodePemda = satkerpemda[1].trim();
                }
//                String satker = logSession.getKodeSatker(n, p);
//                String kodePemda = logSession.getKodePemda(satker);
                if(kodePemda!=null) kodePemda = kodePemda.trim();                
                if (satker.equals(apbd.getKodeSatker())) {
                    if( kodePemda.equals(apbd.getKodePemda()) ){
                        result = apbdSession.inputAPBD(apbd, (short)1);
                    }
                    else result = "Gagal transfer data\n Kode Pemda " + apbd.getKodePemda() + " tidak sesuai untuk Kode Satker " + satker;
                } else {
                    result = "Gagal transfer data\n Password untuk pemda tidak sesuai";
                }
            } else {
                result = "tidak ada data yang di masukkan";
            }
        } catch (Exception ex) {
            Logger.getLogger(ApbdService.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        System.out.println("Koneksi Awal input seluruh APBD: " + namaP);
        return result;
    }

    /**
     * Web service operation
     * @param kodeSatker
     * @param kodePemda
     * @param tahun
     * @param jenisCOA
     * @param kodeData
     * @param userName
     * @param password
     * @return 
     * @throws app.sikd.entity.ws.fault.SIKDServiceException
     */
    @WebMethod(operationName = "ambilSkpdKirimApbd")
    @SuppressWarnings("null")
    public List<Skpd_WS> ambilSkpdKirimApbd(@WebParam(name = "kodeSatker") String kodeSatker, @WebParam(name = "kodePemda") String kodePemda, @WebParam(name = "tahun") short tahun, @WebParam(name = "kodeData") short kodeData, @WebParam(name = "jenisCOA") short jenisCOA, @WebParam(name = "userName") String userName, @WebParam(name = "password") String password)  throws SIKDServiceException {
        List<Skpd_WS> result = null;
        try {
            Context ctxLogin = ServiceServerUtil.getLoginContext();            
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            APBDServiceSessionBeanRemote apbdSession = (APBDServiceSessionBeanRemote) ctxOffice.lookup("APBDServiceSessionBean/remote");
            NewLoginSessionBeanRemote logSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
                String n = userName;
                String p = password;
                String[] satkerpemda = logSession.getKodeSatkerKodePemda(n, p);
                String satker = "";
                String cPemda = "";
                if(satkerpemda!=null && satkerpemda.length==2){
                    satker = satkerpemda[0].trim();
                    cPemda = satkerpemda[1].trim();
                }
                
//                String satker = logSession.getKodeSatker(n, p);
//                String cPemda = logSession.getKodePemda(satker);
                if(cPemda!=null) cPemda = cPemda.trim();                
                if (satker.equals(kodeSatker)) {
                    if( cPemda.equals(kodePemda) ){
                        return apbdSession.getDinasKirimAPBD(tahun, kodeSatker, kodeData, jenisCOA);
                    }
                    throw new SIKDServiceException(
                            "Gagal mengambil data SKPD\n Kode Pemda " + kodePemda + " tidak sesuai untuk Kode Satker " + satker);
                } else {
                    throw new SIKDServiceException("Gagal mengambil data SKPD\n Password untuk pemda tidak sesuai");
                }
        } catch (Exception ex) {
            Logger.getLogger(ApbdService.class.getName()).log(Level.SEVERE, null, ex);
            throw new SIKDServiceException( ex.getMessage());
        }        
    }

    /**
     * Web service operation
     * @param kodeSatker
     * @param kodePemda
     * @param tahun
     * @param kodeData
     * @param jenisCOA
     * @param statusData
     * @param userName
     * @param password
     * @return 
     * @throws app.sikd.entity.ws.fault.SIKDServiceException
     */
    @WebMethod(operationName = "ubahStatusDataApbd")
    @SuppressWarnings("null")
    public String ubahStatusDataApbd(@WebParam(name = "kodeSatker") String kodeSatker, @WebParam(name = "kodePemda") String kodePemda, @WebParam(name = "tahun") short tahun, @WebParam(name = "kodeData") short kodeData, @WebParam(name = "jenisCOA") short jenisCOA, @WebParam(name = "statusData") short statusData, @WebParam(name = "userName") String userName, @WebParam(name = "password") String password)  throws SIKDServiceException {
        String result = "Sukses merubah status Data APBD";
        try {
            Context ctxLogin = ServiceServerUtil.getLoginContext();            
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            APBDServiceSessionBeanRemote apbdSession = (APBDServiceSessionBeanRemote) ctxOffice.lookup("APBDServiceSessionBean/remote");
            NewLoginSessionBeanRemote logSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
                String n = userName;
                String p = password;
                String[] satkerpemda = logSession.getKodeSatkerKodePemda(n, p);
                String satker = "";
                String cPemda = "";
                if(satkerpemda!=null && satkerpemda.length==2){
                    satker = satkerpemda[0].trim();
                    cPemda = satkerpemda[1].trim();
                }
//                String satker = logSession.getKodeSatker(n, p);
//                String cPemda = logSession.getKodePemda(satker);
                if(cPemda!=null) cPemda = cPemda.trim();                
                if (satker.equals(kodeSatker)) {
                    if( cPemda.equals(kodePemda) ){
                        result = apbdSession.updateStatusDtaAPBD(tahun, kodeSatker, kodeData, jenisCOA, statusData);
                        return result;
                    }
                    else {
                        throw new SIKDServiceException(
                            "Gagal merubah status data APBD \n Kode Pemda " + kodePemda + " tidak sesuai untuk Kode Satker " + satker);
                    }
                } else {
                    throw new SIKDServiceException("Gagal merubah status data APBD \n Password untuk pemda tidak sesuai");
                }
        } catch (Exception ex) {
            Logger.getLogger(ApbdService.class.getName()).log(Level.SEVERE, null, ex);
            throw new SIKDServiceException( ex.getMessage());
        }        
    }

    /**
     * Web service operation
     * @param kodeSatker
     * @param tahun
     * @param jenisCOA
     * @param kodeData
     * @return 
     * @throws app.sikd.entity.ws.fault.SIKDServiceException 
     */
    @WebMethod(operationName = "ambilApbdInfo")
    public String ambilApbdInfo(@WebParam(name = "kodeSatker") String kodeSatker, @WebParam(name = "tahun") short tahun, @WebParam(name = "kodeData") short kodeData, @WebParam(name = "jenisCOA") short jenisCOA) throws SIKDServiceException {
        String result;
        try {
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            APBDServiceSessionBeanRemote apbdSession = (APBDServiceSessionBeanRemote) ctxOffice.lookup("APBDServiceSessionBean/remote");
            result = apbdSession.getApbdInfo(tahun, kodeSatker, kodeData, jenisCOA);
            return result;
        } catch (Exception ex) {
            Logger.getLogger(ApbdService.class.getName()).log(Level.SEVERE, null, ex);
            throw new SIKDServiceException( ex.getMessage());
        }
    }



}
