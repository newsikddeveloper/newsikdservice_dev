/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.service;

import app.sikd.entity.ws.DTH_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.service.session.PajakServiceSessionBeanRemote;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.NamingException;


/**
 *
 * @author detra
 */
@WebService(serviceName = "DTHService")
@Stateless()
public class DTHService implements Serializable{
//    @EJB
    PajakServiceSessionBeanRemote session;
//    DthServiceSessionBeanRemote session;
//    @EJB
    NewLoginSessionBeanRemote logSession;

    public DTHService() {
        try {
            Context ctxLogin = ServiceServerUtil.getLoginContext();
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
//            session = (DthServiceSessionBeanRemote) ctxOffice.lookup("DthServiceSessionBean/remote");
            logSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            session = (PajakServiceSessionBeanRemote) ctxOffice.lookup("PajakServiceSessionBean/remote");
        } catch (Exception ex) {
            Logger.getLogger(DTHService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    

    /**
     * Web service operation
     * @param dth
     * @return 
     * @throws app.sikd.entity.ws.fault.SIKDServiceException 
     */
    @WebMethod(operationName = "inputDTH")
    public String inputDTH(@WebParam(name = "dth") List<DTH_WS> dth) throws SIKDServiceException {
        String result;
        String namaP = "";
        try {
//            Context ctxLogin = ServiceServerUtil.getLoginContext();
//            Context ctxOffice = ServiceServerUtil.getOfficeContext();
//            LoginSessionBeanRemote logSession = (LoginSessionBeanRemote) ctxLogin.lookup("LoginSessionBean/remote");
//            PajakServiceSessionBeanRemote session = (PajakServiceSessionBeanRemote) ctxOffice.lookup("PajakServiceSessionBean/remote");
            if (dth != null) {
                DTH_WS d = dth.get(0);
                String n = d.getUserName();
                String p = d.getPassword();
                namaP = d.getNamaPemda();
                System.out.println("koneksi Awal input DTH : " + namaP);
//                String satker = logSession.getKodeSatker(n, p);
//                String kodePemda = logSession.getKodePemda(satker);
                String[] satkerpemda = logSession.getKodeSatkerKodePemda(n, p);
                String satker = "";
                String kodePemda = "";
                if(satkerpemda!=null && satkerpemda.length==2){
                    satker = satkerpemda[0].trim();
                    kodePemda = satkerpemda[1].trim();
                }
                if(kodePemda!=null) kodePemda = kodePemda.trim();
                if (satker.equals(d.getKodeSatker())) {
                    if( kodePemda.equals(d.getKodePemda())){
                        result = session.inputDTH(dth, (short)1);
                    }
                    else result = "Gagal transfer data\n kode Pemda " + d.getKodePemda() + " tidak sesuai untuk kode Satker " + satker;
                } else {
                    result = "Gagal transfer data\n Password untuk pemda tidak sesuai";
                }                
            } else {
                result = "tidak ada data yang di masukkan";
            }
        } catch (SIKDServiceException | NamingException ex) {
            Logger.getLogger(DTHService.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        catch (Exception exc) {
            Logger.getLogger(DTHService.class.getName()).log(Level.SEVERE, null, exc);
            result = exc.getMessage();
        }
        System.out.println("koneksi Akhir input DTH : " + namaP);
        return result;
    }

    /**
     * Web service operation
     * @param dth     
     * @return 
     * @throws app.sikd.entity.ws.fault.SIKDServiceException
     */
    @WebMethod(operationName = "inputDTHPerPeriode")
    public String inputDTHPerPeriode(@WebParam(name = "dth") DTH_WS dth) throws SIKDServiceException{
        String result;
        String namaP = "";
        try {   
//            Context ctxLogin = ServiceServerUtil.getLoginContext();            
//            Context ctxOffice = ServiceServerUtil.getOfficeContext();
//            LoginSessionBeanRemote logSession = (LoginSessionBeanRemote)ctxLogin.lookup("LoginSessionBean/remote");
//            PajakServiceSessionBeanRemote session = (PajakServiceSessionBeanRemote)ctxOffice.lookup("PajakServiceSessionBean/remote");
            if (dth != null) {
                String n = dth.getUserName();
                String p = dth.getPassword();
                namaP = dth.getNamaPemda();
                System.out.println("koneksi Awal input DTH per periode: " + namaP);
//                String satker = logSession.getKodeSatker(n, p);
//                String kodePemda = logSession.getKodePemda(satker);
                String[] satkerpemda = logSession.getKodeSatkerKodePemda(n, p);
                String satker = "";
                String kodePemda = "";
                if(satkerpemda!=null && satkerpemda.length==2){
                    satker = satkerpemda[0].trim();
                    kodePemda = satkerpemda[1].trim();
                }
                if(kodePemda!=null) kodePemda = kodePemda.trim();
                if (satker.equals(dth.getKodeSatker())) {
                    if( kodePemda.equals(dth.getKodePemda())){
                        result = session.inputDTHPerPeriode(dth, (short)1);
                    }
                    else  result = "Gagal transfer data\n Kode Pemda " + dth.getKodePemda() + " tidak sesuai untuk kode satker " + satker;
                }
                else result = "Gagal transfer data\n Password untuk pemda tidak sesuai";                
            }
            else result = "tidak ada data yang di masukkan";
        } catch (SIKDServiceException | NamingException sexc) {
            Logger.getLogger(DTHService.class.getName()).log(Level.SEVERE, null, sexc);
            result = sexc.getMessage();
        } catch (Exception exc) {
            Logger.getLogger(DTHService.class.getName()).log(Level.SEVERE, null, exc);
            result = exc.getMessage();
        }        
        System.out.println("koneksi Akhir input DTH per periode: " + namaP);
        return result;
    }

    /**
     * Web service operation
     * @param kodeSatker
     * @param tahun
     * @param periode
     * @return 
     * @throws app.sikd.entity.ws.fault.SIKDServiceException 
     */
    @WebMethod(operationName = "ambilDthInfo")
    public String ambilDthInfo(@WebParam(name = "kodeSatker") String kodeSatker, @WebParam(name = "tahun") short tahun, @WebParam(name = "periode") short periode) throws SIKDServiceException {
        String result;
        try {   
//            Context ctxOffice = ServiceServerUtil.getOfficeContext();
//            PajakServiceSessionBeanRemote apbdSession = (PajakServiceSessionBeanRemote)ctxOffice.lookup("PajakServiceSessionBean/remote");
            result = session.getDTHInfo(kodeSatker, tahun, periode);                        
        }
        catch (Exception exc) {
            Logger.getLogger(DTHService.class.getName()).log(Level.SEVERE, null, exc);
            result = exc.getMessage();
        }
        return result;
    }
    
    
    

}
