/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.service;

import app.sikd.entity.ws.Restoran_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.service.session.PajakServiceSessionBeanRemote;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.NamingException;

/**
 *
 * @author sora
 */
@WebService(serviceName = "RestoranService")
@Stateless()
public class RestoranService {

    /**
     * Web service operation
     *
     * @param kepemilikanRestoran
     * @return
     */
    @WebMethod(operationName = "inputKepemilikanRestoran")
    public String inputKepemilikanRestoran(@WebParam(name = "kepemilikanRestoran") Restoran_WS kepemilikanRestoran) {
        String result;
        String namaP = "";
        try {
            Context ctxLogin = ServiceServerUtil.getLoginContext();
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            NewLoginSessionBeanRemote logSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            PajakServiceSessionBeanRemote session = (PajakServiceSessionBeanRemote) ctxOffice.lookup("PajakServiceSessionBean/remote");
            if (kepemilikanRestoran != null) {
                String n = kepemilikanRestoran.getUserName();
                String p = kepemilikanRestoran.getPassword();
                namaP = kepemilikanRestoran.getNamaPemda();
                System.out.println("Koneksi Awal input Kepemilikan Restoran : " + namaP);
                String[] satkerpemda = logSession.getKodeSatkerKodePemda(n, p);
                String satker = "";
                String kodePemda = "";
                if(satkerpemda!=null && satkerpemda.length==2){
                    satker = satkerpemda[0].trim();
                    kodePemda = satkerpemda[1].trim();
                }
//                String satker = logSession.getKodeSatker(n, p);
//                String kodePemda = logSession.getKodePemda(satker);
                if(kodePemda!=null) kodePemda = kodePemda.trim();
                if (satker.equals(kepemilikanRestoran.getKodeSatker())) {
                    if( kodePemda.equals(kepemilikanRestoran.getKodePemda())){
                        result = session.inputRestoran(kepemilikanRestoran);
                    }
                    else result = "Gagal transfer data\n Kode Pemda tidak sesuai untuk Kode Satker " + satker;
                } else {
                    result = "Gagal transfer data\n Password untuk pemda tidak sesuai";
                }
            } else {
                result = "tidak ada data yang di masukkan";
            }
        } catch (SIKDServiceException | NamingException ex) {
            Logger.getLogger(RestoranService.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        } catch (Exception exc) {
            Logger.getLogger(RestoranService.class.getName()).log(Level.SEVERE, null, exc);
            result = exc.getMessage();
        }
        System.out.println("Koneksi Akhir input Kepemilikan Restoran : " + namaP);
        return result;
    }
}
