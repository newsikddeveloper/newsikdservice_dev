/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.service;

import app.sikd.entity.ws.ArusKas_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.service.session.APBDServiceSessionBeanRemote;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.naming.Context;


/**
 *
 * @author sora
 */
@WebService(serviceName = "ArusKasService")
@Stateless()
public class ArusKasService {

    /**
     * Web service operation
     * @param arusKas
     * @return 
     */
    @WebMethod(operationName = "inputArusKas")
    public String inputArusKas(@WebParam(name = "arusKas") ArusKas_WS arusKas) {
        String result;
        String namaP = "";
        try {   
            Context ctxLogin = ServiceServerUtil.getLoginContext();            
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            APBDServiceSessionBeanRemote apbdSession = (APBDServiceSessionBeanRemote)ctxOffice.lookup("APBDServiceSessionBean/remote");
            NewLoginSessionBeanRemote logSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            if (arusKas != null) {
                String n = arusKas.getUserName();
                String p = arusKas.getPassword();
                namaP = arusKas.getNamaPemda();
                System.out.println("Koneksi Awal input Arus Kas : " + namaP);
                String[] satkerpemda = logSession.getKodeSatkerKodePemda(n, p);
                String satker = "";
                String kodePemda = "";
                if(satkerpemda!=null && satkerpemda.length==2){
                    satker = satkerpemda[0].trim();
                    kodePemda = satkerpemda[1].trim();
                }
//                String satker = logSession.getKodeSatker(n, p);
//                String kodePemda = logSession.getKodePemda(satker);
                if(kodePemda!=null) kodePemda = kodePemda.trim();
                if (satker.equals(arusKas.getKodeSatker())) {
                    if( kodePemda.equals(arusKas.getKodePemda())){
                        result = apbdSession.inputArusKas(arusKas, (short)1);                        
                    }
                    else result = "Gagal transfer data\n kode Pemda " + arusKas.getKodePemda() + " tidak sesuai untuk kode satker " + satker;
                } 
                else {
                    result = "Gagal transfer data\n Password untuk pemda tidak sesuai";
                }
            }
            else result = "tidak ada data yang di masukkan";
        }
        catch (Exception exc) {
            Logger.getLogger(ArusKasService.class.getName()).log(Level.SEVERE, null, exc);
            result = exc.getMessage();
        }
        System.out.println("Koneksi Akhir input Arus Kas : " + namaP);
        return result;
    }

    /**
     * Web service operation
     * @param kodeSatker
     * @param tahun
     * @param judulArusKas
     * @return 
     * @throws app.sikd.entity.ws.fault.SIKDServiceException 
     */
    @WebMethod(operationName = "ambilArusKasInfo")
    public String ambilArusKasInfo(@WebParam(name = "kodeSatker") String kodeSatker, @WebParam(name = "tahun") short tahun, @WebParam(name = "judulArusKas") String judulArusKas) throws SIKDServiceException {
        String result;
        try {   
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            APBDServiceSessionBeanRemote apbdSession = (APBDServiceSessionBeanRemote)ctxOffice.lookup("APBDServiceSessionBean/remote");
            result = apbdSession.getArusKasInfo(tahun, kodeSatker, judulArusKas);
        }
        catch (Exception exc) {
            Logger.getLogger(ArusKasService.class.getName()).log(Level.SEVERE, null, exc);
            result = exc.getMessage();
        }
        return result;
    }

}
