/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.service;

import app.sikd.entity.ws.DaftarPegawai_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.service.session.NonFinansialServiceSessionBeanRemote;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.naming.Context;

/**
 *
 * @author detra
 */
@WebService(serviceName = "DaftarPegawaiService")
@Stateless()
public class DaftarPegawaiService {

    /**
     * Web service operation
     * @param daftarPegawai
     * @return 
     * @throws app.sikd.entity.ws.fault.SIKDServiceException
     */
    @WebMethod(operationName = "inputDaftarPegawai")
    public String inputDaftarPegawai(@WebParam(name = "daftarPegawai") DaftarPegawai_WS daftarPegawai)  throws SIKDServiceException{
        String result;
        String namaP = "";
        try {   
            Context ctxLogin = ServiceServerUtil.getLoginContext();            
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            NonFinansialServiceSessionBeanRemote session = (NonFinansialServiceSessionBeanRemote)ctxOffice.lookup("NonFinansialServiceSessionBean/remote");
            NewLoginSessionBeanRemote logSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            if (daftarPegawai != null) {
                String n = daftarPegawai.getUserName();
                String p = daftarPegawai.getPassword();
                namaP = daftarPegawai.getNamaPemda();
                System.out.println("Koneksi Awal input Daftar pegawai : " + namaP);
//                String satker = logSession.getKodeSatker(n, p);
//                String kodePemda = logSession.getKodePemda(satker);
                String[] satkerpemda = logSession.getKodeSatkerKodePemda(n, p);
                String satker = "";
                String kodePemda = "";
                if(satkerpemda!=null && satkerpemda.length==2){
                    satker = satkerpemda[0].trim();
                    kodePemda = satkerpemda[1].trim();
                }
                if(kodePemda!=null) kodePemda = kodePemda.trim();
                if (satker.equals(daftarPegawai.getKodeSatker())) {
                    if( kodePemda.equals(daftarPegawai.getKodePemda()) ){
                        result = session.inputDaftarPegawai(daftarPegawai);
                    }
                    else result = "Gagal transfer data\n kode Pemda " +daftarPegawai.getKodePemda() + " tidak sesuai untuk kode satker " + satker;
                }
                else {
                    result = "Gagal transfer data\n Password untuk pemda tidak sesuai";
                }
            }
            else result = "tidak ada data yang di masukkan";
        } catch (Exception ex) {
            Logger.getLogger(DaftarPegawaiService.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }        
        System.out.println("koneksi Akhir input Daftar Pegawai : " + namaP);
        return result;
    }
   
}
