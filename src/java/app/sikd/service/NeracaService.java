/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.service;

import app.sikd.entity.ws.Neraca_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.service.session.APBDServiceSessionBeanRemote;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.naming.Context;

/**
 *
 * @author sora
 */
@WebService(serviceName = "NeracaService")
@Stateless()
public class NeracaService {


    /**
     * Web service operation
     * @param neraca
     * @return 
     * @throws app.sikd.entity.ws.fault.SIKDServiceException 
     */
    @WebMethod(operationName = "inputNeracaPerSemester")
    public String inputNeracaPerSemester(@WebParam(name = "neraca") Neraca_WS neraca) throws SIKDServiceException {
        String result;
        String namaP = "";
        try {
            Context ctxLogin = ServiceServerUtil.getLoginContext();            
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            APBDServiceSessionBeanRemote apbdSession = (APBDServiceSessionBeanRemote) ctxOffice.lookup("APBDServiceSessionBean/remote");
            NewLoginSessionBeanRemote logSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            if (neraca != null) {
                String n = neraca.getUserName();
                String p = neraca.getPassword();
                namaP = neraca.getNamaPemda();
                System.out.println("Koneksi Awal input Neraca : " + namaP);
//                String satker = logSession.getKodeSatker(n, p);
//                String kodePemda = logSession.getKodePemda(satker);
                String[] satkerpemda = logSession.getKodeSatkerKodePemda(n, p);
                String satker = "";
                String kodePemda = "";
                if(satkerpemda!=null && satkerpemda.length==2){
                    satker = satkerpemda[0].trim();
                    kodePemda = satkerpemda[1].trim();
                }
                if(kodePemda!=null) kodePemda = kodePemda.trim();
                if (satker.equals(neraca.getKodeSatker())) {
                    if( kodePemda.equals(neraca.getKodePemda())){
                        result = apbdSession.inputNeracaPerSemester(neraca, (short)1);
                    }
                    else result = "Gagal transfer data\n Kode Pemda " + neraca.getKodePemda() + " tidak sesuai untuk Kode Satker " + satker;
                } else {
                    result = "Gagal transfer data\n Password untuk pemda tidak sesuai";
                }
            } else {
                result = "tidak ada data yang di masukkan";
            }
        } catch (Exception ex) {
            Logger.getLogger(NeracaService.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        System.out.println("Koneksi Akhir input Neraca : " + namaP);
        return result;
    }

    /**
     * Web service operation
     * @param kodeSatker
     * @param tahun
     * @param semester
     * @param judulNeraca
     * @return 
     * @throws app.sikd.entity.ws.fault.SIKDServiceException
     */
    @WebMethod(operationName = "ambilNeracaInfo")
    public String ambilNeracaInfo(@WebParam(name = "kodeSatker") String kodeSatker, @WebParam(name = "tahun") short tahun, @WebParam(name = "semester") short semester, @WebParam(name = "judulNeraca") String judulNeraca) throws SIKDServiceException {
        String result;
        try {   
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            APBDServiceSessionBeanRemote apbdSession = (APBDServiceSessionBeanRemote)ctxOffice.lookup("APBDServiceSessionBean/remote");
            result = apbdSession.getNeracaInfo(tahun, semester, kodeSatker, judulNeraca);
        }
        catch (Exception exc) {
            Logger.getLogger(NeracaService.class.getName()).log(Level.SEVERE, null, exc);
            result = exc.getMessage();
        }
        return result;
    }
}
