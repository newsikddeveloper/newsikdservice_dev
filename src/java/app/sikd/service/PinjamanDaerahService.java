/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.service;

import app.sikd.entity.ws.PinjamanDaerah_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.service.session.APBDServiceSessionBeanRemote;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.naming.Context;

/**
 *
 * @author sora
 */
@WebService(serviceName = "PinjamanDaerahService")
@Stateless()
public class PinjamanDaerahService {

    /**
     * Web service operation
     *
     * @param pinjamanDaerah
     * @return
     */
    @WebMethod(operationName = "inputPinjamanDaerah")
    public String inputPinjamanDaerah(@WebParam(name = "pinjamanDaerah") PinjamanDaerah_WS pinjamanDaerah) {
        String result;
        String namaP = "";
        try {
            Context ctxLogin = ServiceServerUtil.getLoginContext();            
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            APBDServiceSessionBeanRemote apbdSession = (APBDServiceSessionBeanRemote) ctxOffice.lookup("APBDServiceSessionBean/remote");
            NewLoginSessionBeanRemote logSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            if (pinjamanDaerah != null) {
                String n = pinjamanDaerah.getUserName();
                String p = pinjamanDaerah.getPassword();
                namaP = pinjamanDaerah.getNamaPemda();
                System.out.println("Koneksi Awal input Pinjaman Daerah : " + namaP);
                String[] satkerpemda = logSession.getKodeSatkerKodePemda(n, p);
                String satker = "";
                String kodePemda = "";
                if(satkerpemda!=null && satkerpemda.length==2){
                    satker = satkerpemda[0].trim();
                    kodePemda = satkerpemda[1].trim();
                }
//                String satker = logSession.getKodeSatker(n, p);
//                String kodePemda = logSession.getKodePemda(satker);
                if(kodePemda!=null) kodePemda = kodePemda.trim();
                if (satker.equals(pinjamanDaerah.getKodeSatker())) {
                    if( kodePemda.equals(pinjamanDaerah.getKodePemda())){
                        result = apbdSession.inputPinjamanDaerah(pinjamanDaerah, (short)1);
                    }
                    else result = "Gagal transfer data\n Kode Pemda " + pinjamanDaerah.getKodePemda() + " tidak sesuai untuk Kode Satker " + satker;
                } else {
                    result = "Gagal transfer data\n Password untuk pemda tidak sesuai";
                }
            } else {
                result = "tidak ada data yang di masukkan";
            }
        } catch (Exception ex) {
            Logger.getLogger(PinjamanDaerahService.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        System.out.println("Koneksi Akhir input Pinjaman Daerah : " + namaP);
        return result;
    }

    /**
     * Web service operation
     * @param kodeSatker
     * @param tahun
     * @return 
     * @throws app.sikd.entity.ws.fault.SIKDServiceException
     */
    @WebMethod(operationName = "ambilPinjamanDaerahInfo")
    public String ambilPinjamanDaerahInfo(@WebParam(name = "kodeSatker") String kodeSatker, @WebParam(name = "tahun") short tahun) throws SIKDServiceException {
        String result;
        try {   
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            APBDServiceSessionBeanRemote apbdSession = (APBDServiceSessionBeanRemote)ctxOffice.lookup("APBDServiceSessionBean/remote");
            result = apbdSession.getPinjamanDaerahInfo(tahun, kodeSatker);
        }
        catch (Exception exc) {
            Logger.getLogger(PinjamanDaerahService.class.getName()).log(Level.SEVERE, null, exc);
            result = exc.getMessage();
        }
        return result;
    }

}
