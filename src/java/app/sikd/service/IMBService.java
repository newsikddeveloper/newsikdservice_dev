/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.service;

import app.sikd.entity.ws.IMB_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.service.session.PajakServiceSessionBeanRemote;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.NamingException;

/**
 *
 * @author sora
 */
@WebService(serviceName = "IMBService")
@Stateless()
public class IMBService {

    /**
     * Web service operation
     *
     * @param imb
     * @return
     */
    @WebMethod(operationName = "inputIMB")
    public String inputIMB(@WebParam(name = "imb") IMB_WS imb) {
        String result;
        String namaP = "";
        try {
            Context ctxLogin = ServiceServerUtil.getLoginContext();
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            NewLoginSessionBeanRemote logSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            PajakServiceSessionBeanRemote session = (PajakServiceSessionBeanRemote) ctxOffice.lookup("PajakServiceSessionBean/remote");
            if (imb != null) {
                String n = imb.getUserName();
                String p = imb.getPassword();
                namaP = imb.getNamaPemda();
                System.out.println("Koneksi Awal input IMB : " + namaP);
//                String satker = logSession.getKodeSatker(n, p);
//                String kodePemda = logSession.getKodePemda(satker);
                String[] satkerpemda = logSession.getKodeSatkerKodePemda(n, p);
                String satker = "";
                String kodePemda = "";
                if(satkerpemda!=null && satkerpemda.length==2){
                    satker = satkerpemda[0].trim();
                    kodePemda = satkerpemda[1].trim();
                }
                if(kodePemda!=null) kodePemda = kodePemda.trim();
                if (satker.equals(imb.getKodeSatker())) {
                    if( kodePemda.equals(imb.getKodePemda())){
                        result = session.inputIMB(imb);
                    }
                    else result = "Gagal transfer data\n kode Pemda " + imb.getKodePemda() + " tidak sesuai untuk kode Satker "+ satker;
                } else {
                    result = "Gagal transfer data\n Password untuk pemda tidak sesuai";
                }
            } else {
                result = "tidak ada data yang di masukkan";
            }
        } catch (SIKDServiceException | NamingException ex) {
            Logger.getLogger(IMBService.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        } catch (Exception exc) {
            Logger.getLogger(IMBService.class.getName()).log(Level.SEVERE, null, exc);
            result = exc.getMessage();
        }
        System.out.println("koneksi Akhir input IMB : " + namaP);
        return result;
    }
}
