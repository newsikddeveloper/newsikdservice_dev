/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.service;

import app.sikd.entity.ws.BPHTB_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.service.session.PajakServiceSessionBeanRemote;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.NamingException;

/**
 *
 * @author sora
 */
@WebService(serviceName = "BPHTBService")
@Stateless()
public class BPHTBService {

    /**
     * Web service operation
     *
     * @param bphtb
     * @return
     */
    @WebMethod(operationName = "inputBPHTB")
    public String inputBPHTB(@WebParam(name = "bphtb") BPHTB_WS bphtb) {
        String result;
        String namaP = "";
        try {
            Context ctxLogin = ServiceServerUtil.getLoginContext();
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            NewLoginSessionBeanRemote logSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            PajakServiceSessionBeanRemote session = (PajakServiceSessionBeanRemote) ctxOffice.lookup("PajakServiceSessionBean/remote");
            if (bphtb != null) {
                String n = bphtb.getUserName();
                String p = bphtb.getPassword();
                namaP = bphtb.getNamaPemda();
                System.out.println("Koneksi Awal input BPHTB : " + namaP);
                String[] satkerpemda = logSession.getKodeSatkerKodePemda(n, p);
                String satker = "";
                String kodePemda = "";
                if(satkerpemda!=null && satkerpemda.length==2){
                    satker = satkerpemda[0].trim();
                    kodePemda = satkerpemda[1].trim();
                }
                
//                String satker = logSession.getKodeSatker(n, p);
//                String kodePemda = logSession.getKodePemda(satker);    
                if(kodePemda!=null) kodePemda = kodePemda.trim();
                if (satker.equals(bphtb.getKodeSatker())) {
                    if(kodePemda.equals(bphtb.getKodePemda())){
                        result = session.inputBPHTB(bphtb);
                    }
                    else result = "Gagal transfer data\n kode Pemda " + bphtb.getKodePemda() + " tidak sesuai untuk kode satker " + satker;
                } else {
                    result = "Gagal transfer data\n Password untuk pemda tidak sesuai";
                }
            } else {
                result = "tidak ada data yang di masukkan";
            }
        } catch (SIKDServiceException | NamingException ex) {
            Logger.getLogger(BPHTBService.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        } catch (Exception exc) {
            Logger.getLogger(BPHTBService.class.getName()).log(Level.SEVERE, null, exc);
            result = exc.getMessage();
        }
        System.out.println("Koneksi Akhir input BPHTB : " + namaP);
        return result;
    }
}
