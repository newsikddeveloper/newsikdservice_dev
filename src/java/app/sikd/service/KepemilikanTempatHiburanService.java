/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.service;

import app.sikd.entity.ws.Hiburan_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.service.session.PajakServiceSessionBeanRemote;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.NamingException;

/**
 *
 * @author sora
 */
@WebService(serviceName = "KepemilikanTempatHiburanService")
@Stateless()
public class KepemilikanTempatHiburanService {

    /**
     * Web service operation
     *
     * @param kepemilikanTempatHiburan     
     * @return
     */
    @WebMethod(operationName = "inputKepemilikanTempatHiburan")
    public String inputKepemilikanTempatHiburan(@WebParam(name = "kepemilikanTempatHiburan") Hiburan_WS kepemilikanTempatHiburan) {
        String result;
        String namaP = "";
        try {
            Context ctxLogin = ServiceServerUtil.getLoginContext();
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            NewLoginSessionBeanRemote logSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            PajakServiceSessionBeanRemote session = (PajakServiceSessionBeanRemote) ctxOffice.lookup("PajakServiceSessionBean/remote");
            if (kepemilikanTempatHiburan != null) {
                String n = kepemilikanTempatHiburan.getUserName();
                String p = kepemilikanTempatHiburan.getPassword();
                namaP = kepemilikanTempatHiburan.getNamaPemda();
                System.out.println("Koneksi Awal input Kepemilikan Tempat Hiburan : " + namaP);
//                String satker = logSession.getKodeSatker(n, p);
//                String kodePemda = logSession.getKodePemda(satker);
                String[] satkerpemda = logSession.getKodeSatkerKodePemda(n, p);
                String satker = "";
                String kodePemda = "";
                if(satkerpemda!=null && satkerpemda.length==2){
                    satker = satkerpemda[0].trim();
                    kodePemda = satkerpemda[1].trim();
                }
                if(kodePemda!=null) kodePemda = kodePemda.trim();
                if (satker.equals(kepemilikanTempatHiburan.getKodeSatker())) {
                    if( kodePemda.equals(kepemilikanTempatHiburan.getKodePemda())){
                        result = session.inputHiburan(kepemilikanTempatHiburan);
                    } else result = "Gagal transfer data\n Kode Pemda " + kepemilikanTempatHiburan.getKodePemda() + " tidak sesuai untuk Kode Satker " + satker;
                } else {
                    result = "Gagal transfer data\n Password untuk pemda tidak sesuai";
                }
            } else {
                result = "tidak ada data yang di masukkan";
            }
        } catch (SIKDServiceException | NamingException ex) {
            Logger.getLogger(KepemilikanTempatHiburanService.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        } catch (Exception exc) {
            Logger.getLogger(KepemilikanTempatHiburanService.class.getName()).log(Level.SEVERE, null, exc);
            result = exc.getMessage();
        }
        System.out.println("Koneksi Akhir input Kepemilikan Tempat Hiburan : " + namaP);
        return result;
    }
}
