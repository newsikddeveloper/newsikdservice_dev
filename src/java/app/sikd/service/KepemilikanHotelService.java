/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.service;

import app.sikd.entity.ws.Hotel_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.service.session.PajakServiceSessionBeanRemote;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.NamingException;

/**
 *
 * @author sora
 */
@WebService(serviceName = "KepemilikanHotelService")
@Stateless()
public class KepemilikanHotelService {

      /**
     * Web service operation
     *
     * @param kepemilikanHotel
     * @return
     */
    @WebMethod(operationName = "inputKepemilikanHotel")
    public String inputKepemilikanHotel(@WebParam(name = "kepemilikanHotel") Hotel_WS kepemilikanHotel) {
        String result;
        String namaP = "";
        try {
            Context ctxLogin = ServiceServerUtil.getLoginContext();
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            NewLoginSessionBeanRemote logSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            PajakServiceSessionBeanRemote session = (PajakServiceSessionBeanRemote) ctxOffice.lookup("PajakServiceSessionBean/remote");
            if (kepemilikanHotel != null) {
                String n = kepemilikanHotel.getUserName();
                String p = kepemilikanHotel.getPassword();
                namaP = kepemilikanHotel.getNamaPemda();
                System.out.println("Koneksi Awal input Kepemilikan Hotel : " + namaP);
                String[] satkerpemda = logSession.getKodeSatkerKodePemda(n, p);
                String satker = "";
                String kodePemda = "";
                if(satkerpemda!=null && satkerpemda.length==2){
                    satker = satkerpemda[0].trim();
                    kodePemda = satkerpemda[1].trim();
                }
//                String satker = logSession.getKodeSatker(n, p);
//                String kodePemda = logSession.getKodePemda(satker);
                if(kodePemda!=null) kodePemda = kodePemda.trim();
                if (satker.equals(kepemilikanHotel.getKodeSatker())) {
                    if( kodePemda.equals(kepemilikanHotel.getKodePemda())){
                        result = session.inputHotel(kepemilikanHotel);
                    }
                    else result = "Gagal transfer data\n Kode Pemda " + kepemilikanHotel.getKodePemda() + " tidak sesuai untuk kode Satker " + satker ;
                } else {
                    result = "Gagal transfer data\n Password untuk pemda tidak sesuai";
                }
            } else {
                result = "tidak ada data yang di masukkan";
            }
        } catch (SIKDServiceException | NamingException ex) {
            Logger.getLogger(KepemilikanHotelService.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        } catch (Exception exc) {
            Logger.getLogger(KepemilikanHotelService.class.getName()).log(Level.SEVERE, null, exc);
            result = exc.getMessage();
        }
        System.out.println("Koneksi Akhir input Kepemilikan Hotel : " + namaP);
        return result;
    }
}
