/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.service;

import app.sikd.entity.ws.Kendaraan_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.service.session.PajakServiceSessionBeanRemote;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.naming.Context;
import javax.naming.NamingException;

/**
 *
 * @author sora
 */
@WebService(serviceName = "KepemilikanKendaraanService")
@Stateless()
public class KepemilikanKendaraanService {


    /**
     * Web service operation
     * @param kepemilikanKendaraan
     * @return 
     */
    @WebMethod(operationName = "inputKepemilikanKendaraan")
    public String inputKepemilikanKendaraan(@WebParam(name = "kepemilikanKendaraan") Kendaraan_WS kepemilikanKendaraan) {
        String result;
        String namaP = "";
        try {
            Context ctxLogin = ServiceServerUtil.getLoginContext();            
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            NewLoginSessionBeanRemote logSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            PajakServiceSessionBeanRemote session = (PajakServiceSessionBeanRemote) ctxOffice.lookup("PajakServiceSessionBean/remote");
            if (kepemilikanKendaraan != null) {                
                String n = kepemilikanKendaraan.getUserName();
                String p = kepemilikanKendaraan.getPassword();
                namaP = kepemilikanKendaraan.getNamaPemda();
                System.out.println("Koneksi Awal input Kepemilikan Kendaraan : " + namaP);
//                String satker = logSession.getKodeSatker(n, p);
//                String kodePemda = logSession.getKodePemda(satker);
                String[] satkerpemda = logSession.getKodeSatkerKodePemda(n, p);
                String satker = "";
                String kodePemda = "";
                if(satkerpemda!=null && satkerpemda.length==2){
                    satker = satkerpemda[0].trim();
                    kodePemda = satkerpemda[1].trim();
                }
                if(kodePemda!=null) kodePemda = kodePemda.trim();
                if (satker.equals(kepemilikanKendaraan.getKodeSatker())) {
                    if( kodePemda.equals(kepemilikanKendaraan.getKodePemda())){
                        result = session.inputKendaraan(kepemilikanKendaraan);
                    }
                    else result = "Gagal transfer data\n Kode Pemda " + kepemilikanKendaraan.getKodePemda() + " tidak sesuai untuk Kode Satker "+ satker;
                } else {
                    result = "Gagal transfer data\n Password untuk pemda tidak sesuai";
                }
            } else {
                result = "tidak ada data yang di masukkan";
            }
        } catch (SIKDServiceException | NamingException ex) {
            Logger.getLogger(KepemilikanKendaraanService.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        catch (Exception exc) {
            Logger.getLogger(KepemilikanKendaraanService.class.getName()).log(Level.SEVERE, null, exc);
            result = exc.getMessage();
        }
        System.out.println("Koneksi Akhir input Kepemilikan Kendaraan : " + namaP);
        return result;
    }
}
