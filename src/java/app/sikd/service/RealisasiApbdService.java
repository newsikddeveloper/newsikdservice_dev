package app.sikd.service;

import app.sikd.entity.ws.RealisasiAPBD_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.service.session.APBDServiceSessionBeanRemote;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.naming.Context;

/**
 *
 * @author detra
 */
@WebService(serviceName = "RealisasiApbdService")
@Stateless()
public class RealisasiApbdService {

    /**
     * Web service operation
     *
     * @param realisasiApbd
     * @return
     * @throws app.sikd.entity.ws.fault.SIKDServiceException
     */
    @WebMethod(operationName = "inputRealisasiAPBDperPeriode")
    public String inputRealisasiAPBDperPeriode(@WebParam(name = "realisasiApbd") RealisasiAPBD_WS realisasiApbd) throws SIKDServiceException {
        String result;
        String namaP="";
        try {
            Context ctxLogin = ServiceServerUtil.getLoginContext();            
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            APBDServiceSessionBeanRemote apbdSession = (APBDServiceSessionBeanRemote) ctxOffice.lookup("APBDServiceSessionBean/remote");
            NewLoginSessionBeanRemote logSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            if (realisasiApbd != null) {
                String n = realisasiApbd.getUserName();
                String p = realisasiApbd.getPassword();
                namaP = realisasiApbd.getNamaPemda();
                System.out.println("Koneksi Awal input lra per periode: " + namaP);
//                String satker = logSession.getKodeSatker(n, p);
//                String kodePemda = logSession.getKodePemda(satker);
                String[] satkerpemda = logSession.getKodeSatkerKodePemda(n, p);
                String satker = "";
                String kodePemda = "";
                if(satkerpemda!=null && satkerpemda.length==2){
                    satker = satkerpemda[0].trim();
                    kodePemda = satkerpemda[1].trim();
                }
                if(kodePemda!=null) kodePemda = kodePemda.trim();                
                if (satker.equals(realisasiApbd.getKodeSatker())) {
                    if( kodePemda.equals(realisasiApbd.getKodePemda()) ){
                        result = apbdSession.inputRealisasiAPBDperPeriode(realisasiApbd, (short)1);
                    }
                    else result = "Gagal transfer data\n Kode Pemda " + realisasiApbd.getKodePemda() + " tidak sesuai untuk Kode Satker " + satker;
                } else {
                    result = "Gagal transfer data\n Password untuk pemda tidak sesuai";
                }                
            } else {
                result = "tidak ada data yang di masukkan";
            }
        } catch (Exception ex) {
            Logger.getLogger(RealisasiApbdService.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        System.out.println("Koneksi Akhir input lra per periode: " + namaP);
        return result;
    }

    /**
     * Web service operation
     *
     * @param realisasiApbd
     * @return
     * @throws app.sikd.entity.ws.fault.SIKDServiceException
     */
    @WebMethod(operationName = "inputRealisasiAPBD")
    public String inputRealisasiAPBD(@WebParam(name = "realisasiApbd") RealisasiAPBD_WS realisasiApbd) throws SIKDServiceException {
        String result;
        String namaP = "";
        try {
            Context ctxLogin = ServiceServerUtil.getLoginContext();            
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            APBDServiceSessionBeanRemote apbdSession = (APBDServiceSessionBeanRemote) ctxOffice.lookup("APBDServiceSessionBean/remote");
            NewLoginSessionBeanRemote logSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            if (realisasiApbd != null) {
                String n = realisasiApbd.getUserName();
                String p = realisasiApbd.getPassword();                
                namaP = realisasiApbd.getNamaPemda();
                System.out.println("Koneksi Awal input lra: " + namaP);
                String[] satkerpemda = logSession.getKodeSatkerKodePemda(n, p);
                String satker = "";
                String kodePemda = "";
                if(satkerpemda!=null && satkerpemda.length==2){
                    satker = satkerpemda[0].trim();
                    kodePemda = satkerpemda[1].trim();
                }
//                String satker = logSession.getKodeSatker(n, p);
//                String kodePemda = logSession.getKodePemda(satker);
                if(kodePemda!=null) kodePemda = kodePemda.trim();
                if (satker.equals(realisasiApbd.getKodeSatker())) {
                    if( kodePemda.equals(realisasiApbd.getKodePemda())){
                        result = apbdSession.inputRealisasiAPBD(realisasiApbd, (short)1);
                    }
                    else result = "Gagal transfer data\n Kode Pemda "+realisasiApbd.getKodePemda()+" tidak sesuai untuk Kode Satker "+ satker;
                } else {
                    result = "Gagal transfer data\n Password untuk pemda tidak sesuai";
                }                
            } else {
                result = "tidak ada data yang di masukkan";
            }
        } catch (Exception ex) {
            Logger.getLogger(RealisasiApbdService.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        }
        System.out.println("Koneksi Akhir input lra: " + namaP);
        return result;
    }

    /**
     * Web service operation
     * @param kodeSatker
     * @param tahun
     * @param bulan
     * @param kodeData
     * @param jenisCOA
     * @return 
     * @throws app.sikd.entity.ws.fault.SIKDServiceException
     */
    @WebMethod(operationName = "ambilRealisasiApbdInfo")
    public String ambilRealisasiApbdInfo(@WebParam(name = "kodeSatker") String kodeSatker, @WebParam(name = "tahun") short tahun, @WebParam(name = "bulan") short bulan, @WebParam(name = "kodeData") short kodeData, @WebParam(name = "jenisCOA") short jenisCOA) throws SIKDServiceException {
        String result;
        try {   
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            APBDServiceSessionBeanRemote apbdSession = (APBDServiceSessionBeanRemote)ctxOffice.lookup("APBDServiceSessionBean/remote");
            result = apbdSession.getRealisasiApbdInfo(tahun, kodeSatker, bulan, kodeData, jenisCOA);
        }
        catch (Exception exc) {
            Logger.getLogger(RealisasiApbdService.class.getName()).log(Level.SEVERE, null, exc);
            result = exc.getMessage();
        }
        return result;
    }
    
    
}
