/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.service;

import app.sikd.entity.ws.IzinUsaha_WS;
import app.sikd.entity.ws.fault.SIKDServiceException;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.service.session.PajakServiceSessionBeanRemote;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.NamingException;

/**
 *
 * @author sora
 */
@WebService(serviceName = "KepemilikanIzinUsahaService")
@Stateless()
public class KepemilikanIzinUsahaService {

    /**
     * Web service operation
     *
     * @param kepemilikanIzinUsaha
     * @return
     */
    @WebMethod(operationName = "inputKepemilikanIzinUsaha")
    public String inputKepemilikanIzinUsaha(@WebParam(name = "kepemilikanIzinUsaha") IzinUsaha_WS kepemilikanIzinUsaha) {
        String result;
        String namaP = "";
        try {
            Context ctxLogin = ServiceServerUtil.getLoginContext();
            Context ctxOffice = ServiceServerUtil.getOfficeContext();
            NewLoginSessionBeanRemote logSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            PajakServiceSessionBeanRemote session = (PajakServiceSessionBeanRemote) ctxOffice.lookup("PajakServiceSessionBean/remote");
            if (kepemilikanIzinUsaha != null) {
                String n = kepemilikanIzinUsaha.getUserName();
                String p = kepemilikanIzinUsaha.getPassword();
                namaP = kepemilikanIzinUsaha.getNamaPemda();
                System.out.println("Koneksi Awal input Kepemilikan Izin Usaha : " + namaP);
//                String satker = logSession.getKodeSatker(n, p);
//                String kodePemda = logSession.getKodePemda(satker);
                String[] satkerpemda = logSession.getKodeSatkerKodePemda(n, p);
                String satker = "";
                String kodePemda = "";
                if(satkerpemda!=null && satkerpemda.length==2){
                    satker = satkerpemda[0].trim();
                    kodePemda = satkerpemda[1].trim();
                }
                if(kodePemda!=null) kodePemda = kodePemda.trim();
                if (satker.equals(kepemilikanIzinUsaha.getKodeSatker())) {
                    if(kodePemda.equals(kepemilikanIzinUsaha.getKodePemda())){
                        result = session.inputIzinUsaha(kepemilikanIzinUsaha);
                    }
                    else result = "Gagal transfer data\n Kode Pemda " + kepemilikanIzinUsaha.getKodePemda() + " tidak sesuai untuk Kode Satker "+ satker;
                } else {
                    result = "Gagal transfer data\n Password untuk pemda tidak sesuai";
                }
            } else {
                result = "tidak ada data yang di masukkan";
            }
        } catch (SIKDServiceException | NamingException ex) {
            Logger.getLogger(KepemilikanIzinUsahaService.class.getName()).log(Level.SEVERE, null, ex);
            result = ex.getMessage();
        } catch (Exception exc) {
            Logger.getLogger(KepemilikanIzinUsahaService.class.getName()).log(Level.SEVERE, null, exc);
            result = exc.getMessage();
        }
        System.out.println("Koneksi Akhir input Kepemilikan Izin Usaha : " + namaP);
        return result;
    }
}
