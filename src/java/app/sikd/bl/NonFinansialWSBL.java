/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.bl;

import app.sikd.dbapi.INonFinansialWSSQL;
import app.sikd.dbapi.NonFinansialWSSQL;
import app.sikd.entity.ws.DaftarPegawai_WS;
import app.sikd.entity.ws.RincianPegawai_WS;
import app.sikd.entity.ws.SKPDPegawai_WS;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author detra
 */
public class NonFinansialWSBL implements INonFinansialWSBL{
    
    INonFinansialWSSQL sql;

    public NonFinansialWSBL() {
        sql = new NonFinansialWSSQL();
    }
    
    

    @Override
    public String createDaftarPegawai(DaftarPegawai_WS obj, Connection conn) throws Exception {
        try {
//            int currentLevel = conn.getTransactionIsolation();
//            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (obj != null) {
                if (obj.getKodeSatker() != null && obj.getKodePemda() != null) {
                boolean fin = sql.cekFinalDaftarPegawai(obj.getKodeSatker(), obj.getTahunAnggaran(), obj.getBulanGaji(), obj.getJenisData(), conn);
                if( !fin ){                
                    sql.deleteDaftarPegawai(obj.getKodeSatker(), obj.getTahunAnggaran(), obj.getBulanGaji(), obj.getJenisData(), conn);
                
                    sql.createDaftarPegawai(obj, conn);
                    long index = sql.getLastDaftarPegawaiIndex(conn);
                    if (obj.getSkpd()!= null) {
                        for (SKPDPegawai_WS r : obj.getSkpd()) {
                            long idSKPD = sql.createSKPDPegawai(index, r, conn);
                            if( r.getRincians() != null ){
                                for (RincianPegawai_WS rincian : r.getRincians()) {
                                    sql.createRincianPegawai(idSKPD, rincian, conn);
                                }
                            }
                        }
                    }
                }
            }
            }
            conn.commit();
            conn.setAutoCommit(true);
//            conn.setTransactionIsolation(currentLevel);
            return "Sukses melakukan input data Daftar Pegawai";
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal input Daftar Pegawai \n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }
    }
    
    
}
