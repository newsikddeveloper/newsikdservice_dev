/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.bl;

import app.sikd.entity.ws.BPHTB_WS;
import app.sikd.entity.ws.DTH_WS;
import app.sikd.entity.ws.Hiburan_WS;
import app.sikd.entity.ws.Hotel_WS;
import app.sikd.entity.ws.IMB_WS;
import app.sikd.entity.ws.IzinUsaha_WS;
import app.sikd.entity.ws.Kendaraan_WS;
import app.sikd.entity.ws.PajakDanRetribusi_WS;
import app.sikd.entity.ws.Restoran_WS;
import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author detra
 */
public interface IPajakWSBL extends Serializable{
    public String createDTH(List<DTH_WS> objects, short iotype, Connection conn) throws Exception;
    public String getDTHInfo(String kodeSatker, short year, short periode, Connection conn) throws Exception;
    public String createDTHperPeriode(DTH_WS obj, short iotype, Connection conn) throws Exception;
    
    public String createKendaraan(Kendaraan_WS object, Connection conn) throws Exception;
    public String createHotel(Hotel_WS object, Connection conn) throws Exception;
    public String createHiburan(Hiburan_WS object, Connection conn) throws Exception ;
    public String createIzinUsaha(IzinUsaha_WS object, Connection conn) throws Exception;
    public String createBPHTB(BPHTB_WS object, Connection conn) throws Exception;
    public String createIMB(IMB_WS object, Connection conn) throws Exception ;
    public String createRestoran(Restoran_WS object, Connection conn) throws Exception;
    
    public String createPajakDanRetribusiDaerah(PajakDanRetribusi_WS object, Connection conn) throws Exception ;
    
}
