package app.sikd.bl;

import app.sikd.entity.ws.Apbd_WS;
import app.sikd.entity.ws.ArusKas_WS;
import app.sikd.entity.ws.LaporanOperasional_WS;
import app.sikd.entity.ws.Neraca_WS;
import app.sikd.entity.ws.PerhitunganFihakKetiga_WS;
import app.sikd.entity.ws.PerubahanEkuitas_WS;
import app.sikd.entity.ws.PerubahanSal_WS;
import app.sikd.entity.ws.PinjamanDaerah_WS;
import app.sikd.entity.ws.RealisasiAPBD_WS;
import app.sikd.entity.ws.Skpd_WS;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author detra
 */
public interface IWebServiceBL {
    public String createAPBD(Apbd_WS apbd, short iotype, Connection conn ) throws Exception;
    public String createAPBDPerSKPD(Apbd_WS apbd, short iotype, Connection conn) throws Exception;
    public List<Skpd_WS> getDinasKirimAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws Exception;
    public String getApbdInfo(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws Exception;
    public void updateStatusDataAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, short statusData, Connection conn) throws Exception;
    public Apbd_WS getApbd(String kodeSatker, short tahun, short kodeData, short jenisCOA, String userName, String pass, Connection conn) throws Exception;
    
    
    public String createRealisasiAPBDPerPeriode(RealisasiAPBD_WS apbd, short iotype, Connection conn ) throws Exception;
    public void deleteRealisasiAPBDPerPeriode(RealisasiAPBD_WS apbd, Connection conn) throws Exception;
    public String createRealisasiAPBD(RealisasiAPBD_WS apbd, short iotype, Connection conn) throws Exception;
    public String getRealisasiApbdInfo(short year, String kodeSatker, short periode, short kodeData, short jenisCOA, Connection conn) throws Exception;
    public RealisasiAPBD_WS getRealisasiApbd(String kodeSatker, short tahun, short bulan, short kodeData, short jenisCOA, String userName, String pass, Connection conn) throws Exception;
    
    public String createPerhitunganFihakKetiga(PerhitunganFihakKetiga_WS pfk, short iotype, Connection conn) throws Exception;
    public String createPinjamanDaerah(PinjamanDaerah_WS pinjaman, short iotype, Connection conn) throws Exception;
    public String getPerhitunganFihakKetigaInfo(short year, String kodeSatker, Connection conn) throws Exception;
    public String getPinjamanDaerahInfo(short year, String kodeSatker, Connection conn) throws Exception;
    
    public String createNeracaPerSemester(Neraca_WS neraca, short iotype, Connection conn) throws Exception;
    public String getNeracaInfo(short year, short semester, String kodeSatker, String judulNeraca, Connection conn) throws Exception;
    public String getArusKasInfo(short year, String kodeSatker, String judulArusKas, Connection conn) throws Exception;
    public String createArusKas(ArusKas_WS arusKas, short iotype, Connection conn) throws Exception;
    
    public String createLaporanOperasionalPerTriwulan(LaporanOperasional_WS lo, short iotype, Connection conn) throws Exception;
    public String getLaporanOperasionalInfo(short year, short triwulan, String kodeSatker, Connection conn) throws Exception;
    public String getPerubahanSalInfo(short year, String kodeSatker, Connection conn) throws Exception;
    public String getPerubahanEkuitasInfo(short year, String kodeSatker, Connection conn) throws Exception;
    public String createPerubahanSal(PerubahanSal_WS sal, short iotype, Connection conn) throws Exception;
    public String createPerubahanEkuitas(PerubahanEkuitas_WS ekuitas, short iotype, Connection conn) throws Exception;
}
