package app.sikd.bl;

import app.sikd.dbapi.IWebServiceSQL;
import app.sikd.dbapi.WebServiceSQL;
import app.sikd.entity.ws.Apbd_WS;
import app.sikd.entity.ws.ArusKas_WS;
import app.sikd.entity.ws.ArusKeluarInvestasi_WS;
import app.sikd.entity.ws.ArusKeluarNonAnggaran_WS;
import app.sikd.entity.ws.ArusKeluarOperasi_WS;
import app.sikd.entity.ws.ArusKeluarPembiayaan_WS;
import app.sikd.entity.ws.ArusMasukInvestasi_WS;
import app.sikd.entity.ws.ArusMasukNonAnggaran_WS;
import app.sikd.entity.ws.ArusMasukOperasi_WS;
import app.sikd.entity.ws.ArusMasukPembiayaan_WS;
import app.sikd.entity.ws.KegiatanAPBD_WS;
import app.sikd.entity.ws.KodeRekeningAPBD_WS;
import app.sikd.entity.ws.LaporanOperasionalAkunJenis_WS;
import app.sikd.entity.ws.LaporanOperasionalAkunKelompok_WS;
import app.sikd.entity.ws.LaporanOperasionalAkunObjek_WS;
import app.sikd.entity.ws.LaporanOperasionalAkunUtama_WS;
import app.sikd.entity.ws.LaporanOperasional_WS;
import app.sikd.entity.ws.NeracaAkunJenis_WS;
import app.sikd.entity.ws.NeracaAkunKelompok_WS;
import app.sikd.entity.ws.NeracaAkunObjek_WS;
import app.sikd.entity.ws.NeracaAkunUtama_WS;
import app.sikd.entity.ws.Neraca_WS;
import app.sikd.entity.ws.PerhitunganFihakKetiga_WS;
import app.sikd.entity.ws.PerubahanEkuitas_WS;
import app.sikd.entity.ws.PerubahanSal_WS;
import app.sikd.entity.ws.PinjamanDaerah_WS;
import app.sikd.entity.ws.RealisasiAPBD_WS;
import app.sikd.entity.ws.RealisasiKegiatanAPBD_WS;
import app.sikd.entity.ws.RealisasiKodeRekeningAPBD_WS;
import app.sikd.entity.ws.RincianPerhitunganFihakKetiga_WS;
import app.sikd.entity.ws.RincianPinjamanDaerah_WS;
import app.sikd.entity.ws.Skpd_WS;
import app.sikd.util.SIKDUtil;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author detra
 */
public class WebServiceBL implements IWebServiceBL {

    IWebServiceSQL sql;
    SimpleDateFormat sdf;

    public WebServiceBL() {
        sql = new WebServiceSQL();
        sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    }
    
    void parseWaktu(String str, Date date1, Date date2 ){
        long diff = date2.getTime()-date1.getTime();
        long diffS = diff/1000%60;
        long diffM = diff/(60*1000)%60;
        long diffH = diff/(60*60*1000)%24;
        long diffD = diff/(24*60*60*1000);
        String s = "";
        if(diffD>0) s = s + diffD + " Hari ";
        if(diffH>0) s = s + diffH + " Jam ";
        if(diffM>0) s = s + diffM + " Menit ";
        if(diffS>0) s = s + diffS + " Detik ";
        if(s.trim().equals("")) s = "0 detik";
        s = str + " " + s;
        
        System.out.println(s);
        
        
    }
    
    /**
     *
     * @param kodeSatker
     * @param tahun
     * @param kodeData
     * @param jenisCOA
     * @param userName
     * @param pass
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public Apbd_WS getApbd(String kodeSatker, short tahun, short kodeData, short jenisCOA, String userName, String pass, Connection conn) throws Exception{
        try {            
                return sql.getApbd(kodeSatker, tahun, kodeData, jenisCOA, userName, pass, conn);
        } catch (SQLException ex) {
            throw new Exception("Gagal ambil data APBD \n" + ex.getMessage());
        } 
    }

    @Override
    public String createAPBD(Apbd_WS apbd, short iotype, Connection conn) throws Exception {
//        int currentLevel = conn.getTransactionIsolation();
        String result = "";
        try {            
//            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (apbd != null && apbd.getKodeSatker() != null) {
                short ss = sql.cekStatusDataAPBD(apbd.getKodeSatker(), apbd.getTahunAnggaran(), apbd.getKodeData(), apbd.getJenisCOA(), conn);
                if (ss == SIKDUtil.STATUS_BELUM_SIAP || ss == SIKDUtil.STATUS_SIAP || ss == SIKDUtil.STATUS_TELAH_NOT_OK) {
                    if (apbd.getKodePemda() != null) {
                        Date sdel = new Date();
                        System.out.println(sdf.format(sdel) + " Mulai Delete APBD : " + apbd.getNamaPemda());                        
                        sql.deleteAPBD(apbd.getKodeSatker(), apbd.getKodePemda(), apbd.getTahunAnggaran(), apbd.getKodeData(), apbd.getJenisCOA(), conn);
                        Date edel = new Date();
                        System.out.println(sdf.format(edel)+" Akhir Delete APBD: " + apbd.getNamaPemda());
                        parseWaktu("Jumlah Waktu Delete APBD : ", sdel, edel);
                    }
                    
                    Date sins = new Date();
                    System.out.println(sdf.format(sins) + " Mulai Insert APBD: " + apbd.getNamaPemda());
                    long indexAPBD = sql.createAPBD(apbd, iotype, conn);
//                    long indexAPBD = sql.getLastAPBDIndex(conn);
                    if (apbd.getKegiatans() != null) {
                        for (KegiatanAPBD_WS keg : apbd.getKegiatans()) {
                            long indexKegiatan = sql.createKegiatanAPBD(indexAPBD, keg, conn);
//                            long indexKegiatan = sql.getLastKegiatanIndex(conn);
                            if (keg.getKodeRekenings() != null) {
                                for (KodeRekeningAPBD_WS kodeRekening : keg.getKodeRekenings()) {
                                    sql.createRekeningAPBD(indexKegiatan, kodeRekening, conn);
                                }
                            }
                        }
                    }
                    Date eins = new Date();
                    System.out.println(sdf.format(eins) + " Akhir Insert APBD: " + apbd.getNamaPemda());
                    parseWaktu("Jumlah Waktu Insert APBD : ", sins, eins);
                    result = "Sukses melakukan input data APBD";
                }
                else result = "Gagal input data APBD \n Status Data APBD sedang dalam proses Verifikasi";
            }
            conn.commit();
//            conn.setAutoCommit(true);
//            conn.setTransactionIsolation(currentLevel);
            return result;//"Sukses melakukan input data APBD";
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal input APBD \n" + ex.getMessage());
        } finally {
//            if (conn != null) {
                conn.setAutoCommit(true);
//                conn.setTransactionIsolation(currentLevel);
//            }
        }
    }

    @Override
    public String createAPBDPerSKPD(Apbd_WS apbd, short iotype, Connection conn) throws Exception {
//        int currentLevel = conn.getTransactionIsolation();
        String result="";
        try {
//            System.out.println("Mulai Masuk Logic ");
//            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (apbd != null && apbd.getKodeSatker() != null) {
                short ss = sql.cekStatusDataAPBD(apbd.getKodeSatker(), apbd.getTahunAnggaran(), apbd.getKodeData(), apbd.getJenisCOA(), conn);
                if (ss == SIKDUtil.STATUS_BELUM_SIAP || ss == SIKDUtil.STATUS_SIAP || ss == SIKDUtil.STATUS_TELAH_NOT_OK) {
                    long indexAPBD = 0;
                    if (apbd.getKodePemda() != null) {
//                        && apbd.getKegiatans()!= null && apbd.getKegiatans().size() > 0 && apbd.getKegiatans().get(0).getKodeSKPD() != null ) {
                        indexAPBD = sql.getAPBDIndex(apbd.getKodeSatker(), apbd.getKodePemda(),
                                apbd.getTahunAnggaran(), apbd.getKodeData(), apbd.getJenisCOA(), conn);
                    }
                    Date sins = new Date();
                    System.out.println(sdf.format(sins) + " Mulai Insert APBD per skpd: " + apbd.getNamaPemda());
                    if (indexAPBD <= 0) {
                        indexAPBD = sql.createAPBD(apbd, iotype, conn);
//                        indexAPBD = sql.getLastAPBDIndex(conn);
                    } else {
                        sql.updateAPBD(apbd, indexAPBD, conn);
                    }

                    if (apbd.getKegiatans() != null && apbd.getKegiatans().size() > 0) {
                        KegiatanAPBD_WS keg1 = apbd.getKegiatans().get(0);
                        sql.deleteKegiatanAPBD(indexAPBD, keg1.getKodeUrusanPelaksana(), keg1.getKodeSKPD().trim(), conn);
                        for (KegiatanAPBD_WS keg : apbd.getKegiatans()) {
                            long indexKegiatan = sql.createKegiatanAPBD(indexAPBD, keg, conn);
//                            long indexKegiatan = sql.getLastKegiatanIndex(conn);
                            if (keg.getKodeRekenings() != null) {
                                for (KodeRekeningAPBD_WS kodeRekening : keg.getKodeRekenings()) {
                                    sql.createRekeningAPBD(indexKegiatan, kodeRekening, conn);
                                }
                            }
                        }
                    }
                    Date eins = new Date();
                    System.out.println(sdf.format(eins)+" Akhir Insert APBD per skpd: " + apbd.getNamaPemda());
                    parseWaktu("Jumlah Waktu Insert Apbd per SKPD : ", sins, eins);
                    result = "Sukses melakukan input data APBD Per SKPD";
                }
                else result = "Gagal input data APBD \n Status Data APBD sedang dalam proses Verifikasi";
            }
            conn.commit();            
            return result;//"Sukses melakukan input data APBD Per SKPD";
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal input APBD \n" + ex.getMessage());
        } finally {
            conn.setAutoCommit(true);
//            conn.setTransactionIsolation(currentLevel);
        }
    }
    
    /**
     *
     * @param year
     * @param kodeSatker
     * @param kodeData
     * @param jenisCOA
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public List<Skpd_WS> getDinasKirimAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws Exception{
        try {
            List<Skpd_WS> result = new ArrayList();
            if (kodeSatker != null) {
                result = sql.getDinasKirimAPBD(year, kodeSatker, kodeData, jenisCOA, conn);
            }
            return result;
        } catch (SQLException ex) {
            throw new Exception("Gagal mengambil data Dinas yang sudah mengirim APBD \n" + ex.getMessage());
        }
    }
    
    @Override
    public String getApbdInfo(short year, String kodeSatker, short kodeData, short jenisCOA, Connection conn) throws Exception{
        try {
            String result = "";
            if (kodeSatker != null) {
                result = sql.getApbdInfo(kodeSatker, year, kodeData, jenisCOA, conn);
            }
            return result;
        } catch (SQLException ex) {
            throw new Exception("Gagal mengambil Informasi data APBD \n" + ex.getMessage());
        }
    }
    
    @Override
    public void updateStatusDataAPBD(short year, String kodeSatker, short kodeData, short jenisCOA, short statusData, Connection conn) throws Exception{
//        int currentLevel = conn.getTransactionIsolation();
        try {
//            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            if (kodeSatker != null) {
                sql.setStatusAPBD(year, kodeSatker, kodeData, jenisCOA, statusData, conn);
            }
//            conn.setTransactionIsolation(currentLevel);
        } catch (SQLException ex) {
            throw new Exception(ex.getMessage());
        }
        finally{
//            conn.setTransactionIsolation(currentLevel);
        }
    }
    
    
    @Override
    public void deleteRealisasiAPBDPerPeriode(RealisasiAPBD_WS apbd, Connection conn) throws Exception {
        try {
            if (apbd != null) {
                short ss = sql.cekStatusDataRealisasiAPBD(apbd.getKodeSatker(), apbd.getTahunAnggaran(), apbd.getKodeData(), apbd.getPeriode(), apbd.getJenisCOA(), conn);
                if (ss == SIKDUtil.STATUS_BELUM_SIAP || ss == SIKDUtil.STATUS_SIAP || ss == SIKDUtil.STATUS_TELAH_NOT_OK) {
                    if (apbd.getKodeSatker() != null && apbd.getKodePemda() != null) {
                        sql.deleteRealisasiAPBDPerPeriode(apbd.getKodeSatker(), apbd.getKodePemda(), apbd.getTahunAnggaran(), apbd.getPeriode(), apbd.getKodeData(), apbd.getJenisCOA(), conn);
                    }
                }
            }
            
        } catch (SQLException ex) {
            
            throw new Exception("Gagal Hapus data Realisasi APBD Per Periode\n" + ex.getMessage());
        } 
    }

    @Override
    @SuppressWarnings("null")
    public String createRealisasiAPBDPerPeriode(RealisasiAPBD_WS apbd, short iotype, Connection conn) throws Exception {
        int currentLevel = conn.getTransactionIsolation();
        String result = "";
        try {
            conn.setTransactionIsolation(Connection.TRANSACTION_REPEATABLE_READ);//NSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (apbd != null) {
                short ss = sql.cekStatusDataRealisasiAPBD(apbd.getKodeSatker(), apbd.getTahunAnggaran(), apbd.getKodeData(), apbd.getPeriode(), apbd.getJenisCOA(), conn);
                if (ss == SIKDUtil.STATUS_BELUM_SIAP || ss == SIKDUtil.STATUS_SIAP || ss == SIKDUtil.STATUS_TELAH_NOT_OK) {
                    Date sdel = new Date();
                    System.out.println(sdf.format(sdel) + " Mulai Delete Realisasi APBD per periode: " + apbd.getNamaPemda());
//                    sql.deleteRealisasiAPBDPerPeriode(apbd.getKodeSatker(), apbd.getKodePemda(), apbd.getTahunAnggaran(), apbd.getPeriode(), apbd.getKodeData(), apbd.getJenisCOA(), conn);
                    sql.deleteRealisasiAPBDPerPeriode(apbd.getKodeSatker(), apbd.getKodePemda(), apbd.getTahunAnggaran(), apbd.getPeriode(), apbd.getJenisCOA(), conn);
                    Date edel = new Date();
                    System.out.println( sdf.format(edel) + " Akhir Delete Realisasi APBD per periode: " + apbd.getNamaPemda());
                    parseWaktu("Jumlah Waktu Delete Realisasi APBD per periode : ", sdel, edel);
                    Date sins = new Date();
                    System.out.println( sdf.format(sins) + " Mulai Insert Realisasi APBD per periode: " + apbd.getNamaPemda());
                    long indexAPBD = sql.createRealisasiAPBD(apbd, iotype, conn);
                    if (apbd.getKegiatans() != null) {
                        for (RealisasiKegiatanAPBD_WS keg : apbd.getKegiatans()) {
                            long indexKegiatan=sql.createRealisasiKegiatanAPBD(indexAPBD, keg, conn);
                            if (keg.getKodeRekenings() != null) {
                                for (RealisasiKodeRekeningAPBD_WS kodeRekening : keg.getKodeRekenings()) {
                                    sql.createRealisasiRekeningAPBD(indexKegiatan, kodeRekening, conn);
                                }
                            }
                        }
                    }
                    Date eins = new Date();
                    System.out.println(sdf.format(eins) + " Akhir Insert Realisasi APBD per periode: " + apbd.getNamaPemda());
                    parseWaktu("Jumlah Waktu Insert Realisasi APBD per periode : ", sins, eins);
                    result = "Sukses melakukan input data Realisasi APBD Per Periode";
                }
                else result = "Gagal input data Realisasi APBD \n Status data Realisasi APBD dalam keadaan Final Audited";
            }
            conn.commit();
            
            return result;
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal input data Realisasi APBD Per Periode\n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
                conn.setTransactionIsolation(currentLevel);
            }
        }
    }
    
    /**
     *
     * @param year
     * @param kodeSatker
     * @param periode
     * @param kodeData
     * @param jenisCOA
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public String getRealisasiApbdInfo(short year, String kodeSatker, short periode, short kodeData, short jenisCOA, Connection conn) throws Exception{
        try {
            String result = "";
            if (kodeSatker != null) {
                result = sql.getLRAInfo(kodeSatker, year, periode, kodeData, jenisCOA, conn);
            }
            return result;
        } catch (SQLException ex) {
            throw new Exception("Gagal mengambil Informasi data APBD \n" + ex.getMessage());
        }

    }
    
    /**
     *
     * @param kodeSatker
     * @param tahun
     * @param bulan
     * @param kodeData
     * @param jenisCOA
     * @param userName
     * @param pass
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public RealisasiAPBD_WS getRealisasiApbd(String kodeSatker, short tahun, short bulan, short kodeData, short jenisCOA, String userName, String pass, Connection conn) throws Exception{
        try {
            RealisasiAPBD_WS result = new RealisasiAPBD_WS();
            if (kodeSatker != null) {
                result = sql.getRealisasiApbd(kodeSatker, tahun, bulan, kodeData, jenisCOA, userName, pass, conn);
            }
            return result;
        } catch (SQLException ex) {
            throw new Exception("Gagal mengambil Informasi data Realisasi APBD \n" + ex.getMessage());
        }

    }

    /**
     *
     * @param apbd
     * @param conn
     * @return
     * @throws Exception
     */
    @Override
    public String createRealisasiAPBD(RealisasiAPBD_WS apbd, short iotype, Connection conn) throws Exception {
//        int currentLevel = conn.getTransactionIsolation();
        String result="";
        try {
//            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (apbd != null && apbd.getKodeSatker() != null) {
                short ss = sql.cekStatusDataRealisasiAPBD(apbd.getKodeSatker(), apbd.getTahunAnggaran(), apbd.getKodeData(), apbd.getPeriode(), apbd.getJenisCOA(), conn);
                if (ss == SIKDUtil.STATUS_BELUM_SIAP || ss == SIKDUtil.STATUS_SIAP || ss == SIKDUtil.STATUS_TELAH_NOT_OK ) {
                    Date sdel = new Date();
                    System.out.println(sdf.format(sdel)+" Mulai Delete Realisasi APBD : " + apbd.getNamaPemda());
                    sql.deleteRealisasiAPBDPerPeriode(apbd.getKodeSatker(), apbd.getKodePemda(), apbd.getTahunAnggaran(), apbd.getPeriode(), apbd.getKodeData(), apbd.getJenisCOA(), conn);
                    Date edel = new Date();
                    System.out.println(sdf.format(edel) + " Akhir Delete Realisasi APBD: " + apbd.getNamaPemda());
                    parseWaktu("Jumlah Waktu Delete Realisasi APBD : ", sdel, edel);
                    Date sins = new Date();
                    System.out.println(sdf.format(sins) + " Mulai Insert Realisasi APBD: " + apbd.getNamaPemda());
                    long indexAPBD=sql.createRealisasiAPBD(apbd, iotype, conn);
                    if (apbd.getKegiatans() != null) {
                        for (RealisasiKegiatanAPBD_WS keg : apbd.getKegiatans()) {
                            long indexKegiatan=sql.createRealisasiKegiatanAPBD(indexAPBD, keg, conn);
                            if (keg.getKodeRekenings() != null) {
                                for (RealisasiKodeRekeningAPBD_WS kodeRekening : keg.getKodeRekenings()) {
                                    sql.createRealisasiRekeningAPBD(indexKegiatan, kodeRekening, conn);
                                }
                            }
                        }
                    }
                    Date eins = new Date();
                    System.out.println(sdf.format(eins) + " Akhir Insert Realisasi APBD: " + apbd.getNamaPemda());
                    parseWaktu("Jumlah Waktu Insert Realisasi APBD :  ", sins, eins);
                    result = "Sukses melakukan input data Realisasi APBD";
                }
                else result = "Gagal input data Realisasi APBD \n Status data Realisasi APBD dalam keadaan Final Audited";
            }
            conn.commit();
//            conn.setAutoCommit(true);
//            conn.setTransactionIsolation(currentLevel);
            return result;
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal input data Realisasi APBD Per SKPD Per Periode\n" + ex.getMessage());
        } finally {
            conn.setAutoCommit(true);
//            conn.setTransactionIsolation(currentLevel);
        }
    }

    @Override
    public String createPerhitunganFihakKetiga(PerhitunganFihakKetiga_WS pfk, short iotype, Connection conn) throws Exception {
//        int currentLevel = conn.getTransactionIsolation();
//        String result = "";
        try {            
//            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (pfk != null) {
                if (pfk.getKodeSatker() != null && pfk.getKodePemda() != null) {
                    short ss = sql.cekStatusDataPerhitunganFihakKetiga(pfk.getKodeSatker(), pfk.getTahunAnggaran(), conn);
                    if (ss == SIKDUtil.STATUS_BELUM_SIAP || ss == SIKDUtil.STATUS_SIAP || ss == SIKDUtil.STATUS_TELAH_NOT_OK) {
                        Date sdel = new Date();
                        System.out.println(sdf.format(sdel)+" Mulai Delete Perhitungan Fihak Ketiga: " + pfk.getNamaPemda());
                        sql.deletePerhitunganFihakKetiga(pfk.getKodeSatker(), pfk.getTahunAnggaran(), conn);
                        Date edel = new Date();
                        System.out.println(sdf.format(edel)+" Akhir Delete Perhitungan Fihak Ketiga: " + pfk.getNamaPemda());
                        parseWaktu("Jumlah Waktu Delete PFK : ", sdel, edel);
                        Date sins = new Date();
                        System.out.println(sdf.format(sins)+" Mulai Insert Perhitungan Fihak Ketiga: " + pfk.getNamaPemda());
                        long indexPfk = sql.createPerhitunganFihakKetiga(pfk, iotype, conn);
                        if (pfk.getRincians() != null) {
                            for (RincianPerhitunganFihakKetiga_WS rinc : pfk.getRincians()) {
                                sql.createRincianPerhitunganFihakKetiga(indexPfk, rinc, conn);
                            }
                        }
                        Date eins = new Date();
                        System.out.println(sdf.format(eins)+" Akhir Insert Perhitungan Fihak Ketiga: " + pfk.getNamaPemda());
                        parseWaktu("Jumlah Waktu Insert PFK : ", sins, eins);
                    } else {
                        throw new Exception("Status Data Perhitungan Pihak Ketiga Dalam Keadaan Final Audited, \n Tidak Dapat Dirubah Kembali");
                    }
                }
            }
            conn.commit();            
            return "Sukses melakukan input data Perhitungan Fihak Ketiga";
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal input Perhitungan Fihak Ketiga \n" + ex.getMessage());
        } finally {
//            if (conn != null) {
                conn.setAutoCommit(true);
//                conn.setTransactionIsolation(currentLevel);                
//            }
        }
    }
    
    @Override
    public String getPerhitunganFihakKetigaInfo(short year, String kodeSatker, Connection conn) throws Exception{
        try {
            String result = "";
            if (kodeSatker != null) {
                result = sql.getPFKInfo(kodeSatker, year, conn);
            }
            return result;
        } catch (SQLException ex) {
            throw new Exception("Gagal mengambil Informasi data Perhitungan Pihak Ketiga \n" + ex.getMessage());
        }
    }

    @Override
    public String createPinjamanDaerah(PinjamanDaerah_WS pinjaman, short iotype, Connection conn) throws Exception {
//        int currentLevel = conn.getTransactionIsolation();
        try {            
//            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (pinjaman != null) {
                if (pinjaman.getKodeSatker() != null && pinjaman.getKodePemda() != null) {
                    short ss = sql.cekStatusDataPinjamanDaerah(pinjaman.getKodeSatker(), pinjaman.getTahunAnggaran(), conn);
                    if (ss == SIKDUtil.STATUS_BELUM_SIAP || ss == SIKDUtil.STATUS_SIAP || ss==SIKDUtil.STATUS_TELAH_NOT_OK) {
                        Date sdel= new Date();
                        System.out.println(sdf.format(sdel)+" Mulai Delete Pinjaman Daerah: " + pinjaman.getNamaPemda());
                        sql.deletePinjamanDaerah(pinjaman.getKodeSatker(), pinjaman.getTahunAnggaran(), conn);
                        Date edel= new Date();
                        System.out.println(sdf.format(edel)+" Akhir Delete Pinjaman Daerah: " + pinjaman.getNamaPemda());
                        parseWaktu("Jumlah Waktu delete Pinjaman Daerah : ", sdel, edel);
                        Date sins= new Date();
                        System.out.println(sdf.format(sins) + " Mulai Insert Pinjaman Daerah: " + pinjaman.getNamaPemda());
                        long indexPinjaman = sql.createPinjamanDaerah(pinjaman, iotype, conn);
//                        long indexPinjaman = sql.getLastPinjamanDaerahIndex(conn);
                        if (pinjaman.getRincians() != null) {
                            for (RincianPinjamanDaerah_WS rinc : pinjaman.getRincians()) {
                                sql.createRincianPinjamanDaerah(rinc, indexPinjaman, conn);
                            }
                        }
                        Date eins= new Date();
                        System.out.println(sdf.format(eins)+" Akhir Insert Pinjaman Daerah: " + pinjaman.getNamaPemda());
                        parseWaktu("Jumlah Waktu Insert Pinjaman Daerah :  ", sins, eins);
                    } else {
                        throw new Exception("Status data Pinjaman Daerah Dalam Keadaan Final Audited,\n Data Tidak Dapat Dirubah Kembali");
                    }
                }
            }
            conn.commit();
//            conn.setAutoCommit(true);
//            conn.setTransactionIsolation(currentLevel);
            return "Sukses melakukan input data Pinjaman Daerah";
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal input Pinjaman Daerah \n" + ex.getMessage());
        } finally {
//            if (conn != null) {
                conn.setAutoCommit(true);
//                conn.setTransactionIsolation(currentLevel);
//            }
        }
    }
    
    @Override
    public String getPinjamanDaerahInfo(short year, String kodeSatker, Connection conn) throws Exception{
        try {
            String result = "";
            if (kodeSatker != null) {
                result = sql.getPinjamanDaerahInfo(kodeSatker, year, conn);
            }
            return result;
        } catch (SQLException ex) {
            throw new Exception("Gagal mengambil Informasi data Pinjaman Daerah \n" + ex.getMessage());
        }
    }

    @Override
    public String createNeracaPerSemester(Neraca_WS neraca, short iotype, Connection conn) throws Exception {
//        int currentLevel = conn.getTransactionIsolation();
        try {            
//            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (neraca != null) {
                if (neraca.getKodeSatker() != null && neraca.getKodePemda() != null && neraca.getJudulNeraca() != null) {
                    short ss = sql.cekStatusDataNeraca(neraca.getKodeSatker(), neraca.getTahunAnggaran(), neraca.getSemester(), neraca.getJudulNeraca(), conn);
                    if (ss == SIKDUtil.STATUS_BELUM_SIAP || ss == SIKDUtil.STATUS_SIAP || ss==SIKDUtil.STATUS_TELAH_NOT_OK) {
                        Date sdel= new Date();
                        System.out.println(sdf.format(sdel)+ " Mulai Delete Neraca: " + neraca.getNamaPemda());
                        sql.deleteNeraca(neraca.getKodeSatker(), neraca.getTahunAnggaran(), neraca.getSemester(), neraca.getJudulNeraca(), conn);
                        Date edel= new Date();
                        System.out.println(sdf.format(edel) + " Akhir Delete Neraca: " + neraca.getNamaPemda());
                        parseWaktu("jumlah waktu delete Neraca : ", sdel, edel);
                        Date sins= new Date();
                        System.out.println(sdf.format(sins)+" Mulai Insert Neraca: " + neraca.getNamaPemda());
                        long indexNeraca = sql.createNeraca(neraca, iotype, conn);
                        if (neraca.getAkunUtamas() != null) {
                            for (NeracaAkunUtama_WS akunUtama : neraca.getAkunUtamas()) {
                                long indexAkunUtama = sql.createNeracaAkunUtama(indexNeraca, akunUtama, conn);
                                if (akunUtama.getAkunKelompoks() != null) {
                                    for (NeracaAkunKelompok_WS akunKelompok : akunUtama.getAkunKelompoks()) {
                                        long indexAkunKelompok = sql.createNeracaAkunKelompok(indexAkunUtama, akunKelompok, conn);
                                        if (akunKelompok.getAkunJeniss() != null) {
                                            for (NeracaAkunJenis_WS akunJenis : akunKelompok.getAkunJeniss()) {
                                                long indexAkunJenis = sql.createNeracaAkunJenis(indexAkunKelompok, akunJenis, conn);
                                                if (akunJenis.getAkunObjeks() != null) {
                                                    for (NeracaAkunObjek_WS akunObjek : akunJenis.getAkunObjeks()) {
//                                                    System.out.println("index Jenis " + indexAkunJenis);
                                                        sql.createNeracaAkunObjek(indexAkunJenis, akunObjek, conn);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        Date eins= new Date();
                        System.out.println(sdf.format(eins)+" Akhir Insert Neraca: " + neraca.getNamaPemda());
                        parseWaktu("Jumlah waktu insert Neraca : ", sins, eins);
                    } else {
                        throw new Exception("Status Data Neraca dalam Kedaan Final Audited, \nTidak dapat Dirubah Kembali");
                    }
                }
            }
            conn.commit();            
            return "Sukses melakukan input data Neraca";
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal input Neraca \n" + ex.getMessage());
        } finally {
//            if (conn != null) {
                conn.setAutoCommit(true);
//                conn.setTransactionIsolation(currentLevel);
//            }
        }
    }
    
    
    @Override
    public String getNeracaInfo(short year, short semester, String kodeSatker, String judulNeraca, Connection conn) throws Exception{
        try {
            String result = "";
            if (kodeSatker != null) {
                result = sql.getNeracaInfo(kodeSatker, year, semester, judulNeraca, conn);
            }
            return result;
        } catch (SQLException ex) {
            throw new Exception("Gagal mengambil Informasi data Neraca \n" + ex.getMessage());
        }
    }

    @Override
    public String createArusKas(ArusKas_WS arusKas, short iotype, Connection conn) throws Exception {
//        int currentLevel = conn.getTransactionIsolation();
        try {
//            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (arusKas != null) {
                short ss = sql.cekStatusDataArusKas(arusKas.getKodeSatker(), arusKas.getTahunAnggaran(), arusKas.getJudulArusKas(), conn);
                if (ss == SIKDUtil.STATUS_BELUM_SIAP || ss == SIKDUtil.STATUS_SIAP || ss==SIKDUtil.STATUS_TELAH_NOT_OK) {
                    if (arusKas.getKodeSatker() != null && arusKas.getKodePemda() != null && arusKas.getJudulArusKas() != null) {
                        Date sdel= new Date();
                        System.out.println(sdf.format(sdel)+" Mulai Delete Arus Kas: " + arusKas.getNamaPemda());
                        sql.deleteArusKas(arusKas.getKodeSatker(), arusKas.getTahunAnggaran(), arusKas.getJudulArusKas(), conn);
                        Date edel= new Date();
                        System.out.println(sdf.format(edel)+" Akhir Delete Arus Kas: " + arusKas.getNamaPemda());
                        parseWaktu("Jumlah waktu Delete Arus Kas :  ", sdel, edel);
                    }Date sins= new Date();
                    System.out.println(sdf.format(sins)+" Mulai Insert Arus Kas: " + arusKas.getNamaPemda());
                    long indexArusKas = sql.createArusKas(arusKas, iotype, conn);
                    if (arusKas.getArusKasSaldo() != null) {
                        sql.createArusKasSaldo(indexArusKas, arusKas.getArusKasSaldo(), conn);
                    }
                    if (arusKas.getArusKeluarInvestasis() != null) {
                        for (ArusKeluarInvestasi_WS arusKeluarInvestasi : arusKas.getArusKeluarInvestasis()) {
                            sql.createArusKeluarInvestasi(indexArusKas, arusKeluarInvestasi, conn);
                        }
                    }
                    if (arusKas.getArusMasukInvestasis() != null) {
                        for (ArusMasukInvestasi_WS arusMasukInvestasi : arusKas.getArusMasukInvestasis()) {
                            sql.createArusMasukInvestasi(indexArusKas, arusMasukInvestasi, conn);
                        }
                    }

                    if (arusKas.getArusKeluarNonAnggarans() != null) {
                        for (ArusKeluarNonAnggaran_WS arusKeluarNonAnggaran : arusKas.getArusKeluarNonAnggarans()) {
                            sql.createArusKeluarNonAnggaran(indexArusKas, arusKeluarNonAnggaran, conn);
                        }
                    }
                    if (arusKas.getArusMasukNonAnggarans() != null) {
                        for (ArusMasukNonAnggaran_WS arusMasukNonAnggaran : arusKas.getArusMasukNonAnggarans()) {
                            sql.createArusMasukNonAnggaran(indexArusKas, arusMasukNonAnggaran, conn);
                        }
                    }

                    if (arusKas.getArusKeluarOperasis() != null) {
                        for (ArusKeluarOperasi_WS arusKeluarOperasi : arusKas.getArusKeluarOperasis()) {
                            sql.createArusKeluarOperasi(indexArusKas, arusKeluarOperasi, conn);
                        }
                    }
                    if (arusKas.getArusMasukOperasis() != null) {
                        for (ArusMasukOperasi_WS arusMasukOperasi : arusKas.getArusMasukOperasis()) {
                            sql.createArusMasukOperasi(indexArusKas, arusMasukOperasi, conn);
                        }
                    }
                    if (arusKas.getArusKeluarPembiayaans() != null) {
                        for (ArusKeluarPembiayaan_WS arusKeluarPembiayaan : arusKas.getArusKeluarPembiayaans()) {
                            sql.createArusKeluarPembiayaan(indexArusKas, arusKeluarPembiayaan, conn);
                        }
                    }
                    if (arusKas.getArusMasukPembiayaans() != null) {
                        for (ArusMasukPembiayaan_WS arusMasukPembiayaan : arusKas.getArusMasukPembiayaans()) {
                            sql.createArusMasukPembiayaan(indexArusKas, arusMasukPembiayaan, conn);
                        }
                    }
                    Date eins= new Date();
                    System.out.println(sdf.format(eins)+" Akhir Insert Arus Kas: " + arusKas.getNamaPemda());
                    parseWaktu("Jumlah Waktu Inert Arus Kas : ", sins, eins);
                } else {
                    throw new Exception("Status Data Arus Kas Dalam Keadaan Final Audited, \nTidak Dapat dirubah Kembali");
                }
            }
            conn.commit();
            
            return "Sukses melakukan input data Arus Kas";
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal input Arus Kas \n" + ex.getMessage());
        } finally {
            conn.setAutoCommit(true);
//            conn.setTransactionIsolation(currentLevel);
        }
    }
    
    @Override
    public String getArusKasInfo(short year, String kodeSatker, String judulArusKas, Connection conn) throws Exception{
        try {
            String result = "";
            if (kodeSatker != null) {
                result = sql.getArusKasInfo(kodeSatker, year, judulArusKas, conn);
            }
            return result;
        } catch (SQLException ex) {
            throw new Exception("Gagal mengambil Informasi data Arus Kas \n" + ex.getMessage());
        }
    }

    @Override
    public String createLaporanOperasionalPerTriwulan(LaporanOperasional_WS lo, short iotype, Connection conn) throws Exception {
//        int currentLevel = conn.getTransactionIsolation();
        try {            
//            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (lo != null) {
                if (lo.getKodeSatker() != null && lo.getKodePemda() != null) {
                    short ss = sql.cekStatusDataLaporanOperasional(lo.getKodeSatker(), lo.getTahunAnggaran(), lo.getTriwulan(), conn);
                    if (ss == SIKDUtil.STATUS_BELUM_SIAP || ss == SIKDUtil.STATUS_SIAP || ss==SIKDUtil.STATUS_TELAH_NOT_OK) {
                        Date sdel= new Date();
                        System.out.println(sdf.format(sdel)+" Mulai Delete Laporan Operasional: " + lo.getNamaPemda());
                        sql.deleteLO(lo.getKodeSatker(), lo.getTahunAnggaran(), lo.getTriwulan() , conn);
                        Date edel= new Date();
                        System.out.println(sdf.format(edel)+" Akhir Delete Laporan Operasional: " + lo.getNamaPemda());
                        parseWaktu("Jumlah Waktu Delete Laporan Operasional : ", sdel, edel);
                        Date sins= new Date();
                        System.out.println(sdf.format(sins)+" Mulai Insert Laporan Operasional: " + lo.getNamaPemda());
                        long indexLO = sql.createLaporanOperasional(lo, iotype, conn);
                        if (lo.getAkunUtamas() != null) {
                            for (LaporanOperasionalAkunUtama_WS akunUtama : lo.getAkunUtamas()) {
                                long indexAkunUtama = sql.createLOAkunUtama(indexLO, akunUtama, conn);
                                if (akunUtama.getAkunKelompoks() != null) {
                                    for (LaporanOperasionalAkunKelompok_WS akunKelompok : akunUtama.getAkunKelompoks()) {
                                        long indexAkunKelompok = sql.createLOAkunKelompok(indexAkunUtama, akunKelompok, conn);
                                        if (akunKelompok.getAkunJeniss() != null) {
                                            for (LaporanOperasionalAkunJenis_WS akunJenis : akunKelompok.getAkunJeniss()) {
                                                long indexAkunJenis = sql.createLOAkunJenis(indexAkunKelompok, akunJenis, conn);
                                                if (akunJenis.getAkunObjeks() != null) {
                                                    for (LaporanOperasionalAkunObjek_WS akunObjek : akunJenis.getAkunObjeks()) {
                                                        sql.createLOAkunObjek(indexAkunJenis, akunObjek, conn);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (lo.getSurplusNonOperasional() != null) {
                            sql.createSurplusNonOP(indexLO, lo.getSurplusNonOperasional(), conn);
                        }
                        if (lo.getDefisitNonOperasional() != null) {
                            sql.createDefisitNonOP(indexLO, lo.getDefisitNonOperasional(), conn);
                        }
                        if (lo.getPosLuarBiasaOperasional() != null) {
                            sql.createLOPosLuarBiasa(indexLO, lo.getPosLuarBiasaOperasional(), conn);
                        }
                        Date eins= new Date();
                        System.out.println(sdf.format(eins)+" Akhir Insert Laporan Operasional: " + lo.getNamaPemda());
                        parseWaktu("Jumlah waktu insert Laporan Operasional : ", sins, eins);
                    } else {
                        throw new Exception("Status data Laporan Operasional dalam Kedaan Final Audited, \nTidak Dapat Dirubah Kembali");
                    }
                }
            }
            conn.commit();            
            return "Sukses melakukan input data Laporan Operasional";
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal input Laporan Operasional \n" + ex.getMessage());
        } finally {
//            if (conn != null) {
                conn.setAutoCommit(true);
//                conn.setTransactionIsolation(currentLevel);
//            }
        }
    }
    
    @Override
    public String getLaporanOperasionalInfo(short year, short triwulan, String kodeSatker, Connection conn) throws Exception{
        try {
            String result = "";
            if (kodeSatker != null) {
                result = sql.getLOInfo(kodeSatker, year, triwulan, conn);
            }
            return result;
        } catch (SQLException ex) {
            throw new Exception("Gagal mengambil Informasi data Laporan Operasional \n" + ex.getMessage());
        }
    }

    @Override
    public String createPerubahanSal(PerubahanSal_WS sal, short iotype, Connection conn) throws Exception {
//        int currentLevel = conn.getTransactionIsolation();
        try {            
//            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (sal != null) {
                if (sal.getKodeSatker() != null && sal.getKodePemda() != null) {
                    short ss = sql.cekStatusDataPerubahanSal(sal.getKodeSatker(), sal.getTahunAnggaran(), conn);
                    if (ss == SIKDUtil.STATUS_BELUM_SIAP || ss == SIKDUtil.STATUS_SIAP || ss==SIKDUtil.STATUS_TELAH_NOT_OK) {
                        Date sdel= new Date();
                        System.out.println(sdf.format(sdel)+" Mulai Delete Perubahan Sal: " + sal.getNamaPemda());
                        sql.deletePerubahanSal(sal.getKodeSatker(), sal.getTahunAnggaran(), conn);
                        Date edel= new Date();
                        System.out.println(sdf.format(edel)+" Akhir Delete Perubahan Sal: " + sal.getNamaPemda());
                        parseWaktu("Jumlah Waktu Delete Perubahan Sal : ", sdel, edel);
                        Date sins= new Date();
                        System.out.println(sdf.format(sins)+" Mulai Insert Perubahan Sal: " + sal.getNamaPemda());

                        long indexSal = sql.createPerubahanSal(sal, iotype, conn);
                        if (sal.getDetil() != null) {
                            sql.createPerubahanSalDetail(indexSal, sal.getDetil(), conn);
                        }
                        Date eins= new Date();
                        System.out.println(sdf.format(eins)+" Akhir Insert Perubahan Sal: " + sal.getNamaPemda());
                        parseWaktu("Jumlah waktu insert Perubahan Sal : ", sins, eins);
                    }
                    else throw new Exception("Status Data Perubahan SAL dalam keadaan Final Audited, \n Data Tidak Dapat Dirubah Kembali");
                }
            }
            conn.commit();            
            return "Sukses melakukan input data Perubahan Sal";
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal input Perubahan Sal \n" + ex.getMessage());
        } finally {
//            if (conn != null) {
                conn.setAutoCommit(true);
//                conn.setTransactionIsolation(currentLevel);
//            }
        }
    }
    
    @Override
    public String getPerubahanSalInfo(short year, String kodeSatker, Connection conn) throws Exception{
        try {
            String result = "";
            if (kodeSatker != null) {
                result = sql.getPerubahanSalInfo(kodeSatker, year, conn);
            }
            return result;
        } catch (SQLException ex) {
            throw new Exception("Gagal mengambil Informasi data Perubahan Sal \n" + ex.getMessage());
        }
    }

    @Override
    public String createPerubahanEkuitas(PerubahanEkuitas_WS ekuitas, short iotype, Connection conn) throws Exception {
//        int currentLevel = conn.getTransactionIsolation();
        try {            
//            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (ekuitas != null) {
                if (ekuitas.getKodeSatker() != null && ekuitas.getKodePemda() != null) {
                    short ss = sql.cekStatusDataPerubahanEkuitas(ekuitas.getKodeSatker(), ekuitas.getTahunAnggaran(), conn);
                    if (ss == SIKDUtil.STATUS_BELUM_SIAP || ss == SIKDUtil.STATUS_SIAP || ss==SIKDUtil.STATUS_TELAH_NOT_OK) {
                        Date sdel= new Date();
                        System.out.println(sdf.format(sdel)+" Mulai Delete Perubahan Ekuitas: " + ekuitas.getNamaPemda());
                        sql.deletePerubahanEkuitas(ekuitas.getKodeSatker(), ekuitas.getTahunAnggaran(), conn);
                        Date edel= new Date();
                        System.out.println(sdf.format(edel)+" Akhir Delete Perubahan Ekuitas: " + ekuitas.getNamaPemda());
                        parseWaktu("Jumlah Waktu Delete Perubahan Ekuitas : ", sdel, edel);
                        Date sins= new Date();
                        System.out.println(sdf.format(sins)+" Mulai Insert Perubahan Ekuitas: " + ekuitas.getNamaPemda());
                        long indexEkuitas = sql.createPerubahanEkuitas(ekuitas, iotype, conn);
                        if (ekuitas.getDetil() != null) {
                            sql.createPerubahanEkuitasDetail(indexEkuitas, ekuitas.getDetil(), conn);
                        }
                        Date eins= new Date();
                        System.out.println(sdf.format(eins)+" Akhir Insert Perubahan Ekuitas: " + ekuitas.getNamaPemda());
                        parseWaktu("Jumlah waktu insert Perubahan Ekuitas : ", sins, eins);
                    } else {
                        throw new Exception("Status Data Perubahan Ekuitas dalam Keadaan Final Audited, \n Tidak Dapat Dirubah Kembali");
                    }
                }
            }
            conn.commit();
//            conn.setAutoCommit(true);
//            conn.setTransactionIsolation(currentLevel);
            return "Sukses melakukan input data Perubahan Ekuitas";
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal input Perubahan Ekuitas \n" + ex.getMessage());
        } finally {
//            if (conn != null) {
                conn.setAutoCommit(true);
//                conn.setTransactionIsolation(currentLevel);
//            }
        }
    }
    
    @Override
    public String getPerubahanEkuitasInfo(short year, String kodeSatker, Connection conn) throws Exception{
        try {
            String result = "";
            if (kodeSatker != null) {
                result = sql.getPerubahanEkuitasInfo(kodeSatker, year, conn);
            }
            return result;
        } catch (SQLException ex) {
            throw new Exception("Gagal mengambil Informasi data APBD \n" + ex.getMessage());
        }
    }
}
