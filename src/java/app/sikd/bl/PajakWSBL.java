/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.bl;

import app.sikd.dbapi.IPajakWSSQL;
import app.sikd.dbapi.PajakWSSQL;
import app.sikd.entity.ws.AkunDTHSKPD_WS;
import app.sikd.entity.ws.BPHTB_WS;
import app.sikd.entity.ws.DTH_WS;
import app.sikd.entity.ws.Hiburan_WS;
import app.sikd.entity.ws.Hotel_WS;
import app.sikd.entity.ws.IMB_WS;
import app.sikd.entity.ws.IzinUsaha_WS;
import app.sikd.entity.ws.Kendaraan_WS;
import app.sikd.entity.ws.PajakDTHSKPD_WS;
import app.sikd.entity.ws.PajakDanRetribusi_WS;
import app.sikd.entity.ws.Restoran_WS;
import app.sikd.entity.ws.RincianBPHTB_WS;
import app.sikd.entity.ws.RincianDTHSKPD_WS;
import app.sikd.entity.ws.RincianHiburan_WS;
import app.sikd.entity.ws.RincianHotel_WS;
import app.sikd.entity.ws.RincianIMB_WS;
import app.sikd.entity.ws.RincianIzinUsaha_WS;
import app.sikd.entity.ws.RincianKendaraan_WS;
import app.sikd.entity.ws.RincianPajakDanRetribusi_WS;
import app.sikd.entity.ws.RincianRestoran_WS;
import app.sikd.entity.ws.SP2DKegiatan_WS;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author detra
 */
public class PajakWSBL implements IPajakWSBL {

    IPajakWSSQL sql;
    SimpleDateFormat sdf;
    public PajakWSBL() {
        sql = new PajakWSSQL();
        sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    }
    
    void parseWaktu(String str, Date date1, Date date2 ){
        long diff = date2.getTime()-date1.getTime();
        long diffS = diff/1000%60;
        long diffM = diff/(60*1000)%60;
        long diffH = diff/(60*60*1000)%24;
        long diffD = diff/(24*60*60*1000);
        String s = "";
        if(diffD>0) s = s + diffD + " Hari ";
        if(diffH>0) s = s + diffH + " Jam ";
        if(diffM>0) s = s + diffM + " Menit ";
        if(diffS>0) s = s + diffS + " Detik ";
        if(s.trim().equals("")) s = "0 detik";
        s = str + " " + s;
        
        System.out.println(s);
        
        
    }

    @Override
    public String createDTH(List<DTH_WS> objects, short iotype, Connection conn) throws Exception {
        try {
//            int currentLevel = conn.getTransactionIsolation();
//            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (objects != null) {
                DTH_WS dthf = objects.get(0);
                if (dthf.getKodeSatker() != null) {
                    boolean fin = sql.cekFinalDTH(dthf.getKodeSatker(), dthf.getTahunAnggaran(), conn);
                    if (!fin) {
                        Date sdel = new Date();
                        System.out.println(sdf.format(sdel)+ " Awal Delete DTH : " + dthf.getNamaPemda());
                        sql.deleteDTH(dthf.getKodeSatker(), dthf.getTahunAnggaran(), conn);
                        Date edel = new Date();
                        System.out.println(sdf.format(edel)+ " Akhir Delete DTH : " + dthf.getNamaPemda());
                        parseWaktu("Jumlah Waktu Delete DTH : ", sdel, edel);
                        Date sins = new Date();
                        System.out.println(sdf.format(sins)+ " Awal Insert DTH : " + dthf.getNamaPemda());
                        for (DTH_WS obj : objects) {
                            long index = sql.createDTH(obj, iotype, conn);

                            if (obj.getRincians() != null) {
                                for (RincianDTHSKPD_WS rincian : obj.getRincians()) {
                                    long idrincian = sql.createRincianDTHSKPD(rincian, index, conn);

                                    if (rincian.getKegiatans() != null) {
                                        for (SP2DKegiatan_WS kegiatan : rincian.getKegiatans()) {
                                            long kegiatanindex = sql.createSP2DKegiatan(kegiatan, idrincian, conn);
                                            if (kegiatan.getAkuns() != null) {
                                                if (kegiatan.getAkuns().size() > 0) {
                                                    for (AkunDTHSKPD_WS a : kegiatan.getAkuns()) {
                                                        sql.createAkunDTHSKPD(a, kegiatanindex, conn);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (rincian.getPajaks() != null) {
                                        for (PajakDTHSKPD_WS pajak : rincian.getPajaks()) {
                                            sql.createPajakDTHSKPD(pajak, idrincian, conn);
                                        }
                                    }

                                }
                            }
                        }
                        Date eins = new Date();
                        System.out.println(sdf.format(eins)+ " Akhir Insert DTH : " + dthf.getNamaPemda());
                        parseWaktu("Jumlah Waktu Insert DTH : ", sins, eins);
                    } else {
                        throw new Exception("Tidak dapat input DTH karena data DTH sudah dalam status Final");
                    }
                }
            }
            conn.commit();
//            conn.setAutoCommit(true);
//            conn.setTransactionIsolation(currentLevel);
            return "Sukses melakukan input data DTH";
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
                throw new Exception("Gagal input DTH \n" + ex.getMessage() + "\n" + exc.getMessage());
            }
            throw new Exception("Gagal input DTH \n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }
    }
    
    @Override
    public String createDTHperPeriode(DTH_WS obj, short iotype, Connection conn) throws Exception {
//        int currentLevel = conn.getTransactionIsolation();
        try {    
//            System.out.println("transaksi sebelum " + conn.getTransactionIsolation());
//            if(conn.getTransactionIsolation() != Connection.TRANSACTION_SERIALIZABLE)
//                conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
//                System.out.println("transaksi proses " + conn.getTransactionIsolation());
            
            conn.setAutoCommit(false);
            if (obj != null && obj.getKodeSatker() != null) {
                boolean fin = sql.cekFinalDTH(obj.getKodeSatker(), obj.getTahunAnggaran(), obj.getPeriode(), conn);
                if (!fin) {
                    Date sdel = new Date();
                    System.out.println(sdf.format(sdel)+ " Awal Delete DTH per periode : " + obj.getNamaPemda());
                    sql.deleteDTH(obj.getKodeSatker(), obj.getTahunAnggaran(), obj.getPeriode(), conn);
                    Date edel = new Date();
                    System.out.println(sdf.format(edel)+ " Akhir Delete DTH per periode : " + obj.getNamaPemda());
                    parseWaktu("Jumlah Waktu Delete DTH per periode : ", sdel, edel);
                    Date sins = new Date();
                    System.out.println(sdf.format(sins)+ " Awal Insert DTH per periode : " + obj.getNamaPemda());
                    long index = sql.createDTH(obj, iotype, conn);
                    if (obj.getRincians() != null) {
                                for (RincianDTHSKPD_WS rincian : obj.getRincians()) {
                                    long idrincian = sql.createRincianDTHSKPD(rincian, index, conn);
                                    if (rincian.getKegiatans() != null) {
                                        for (SP2DKegiatan_WS kegiatan : rincian.getKegiatans()) {
                                            long kegiatanindex = sql.createSP2DKegiatan(kegiatan, idrincian, conn);
                                            if (kegiatan.getAkuns() != null) {
                                                if (kegiatan.getAkuns().size() > 0) {
                                                    for (AkunDTHSKPD_WS a : kegiatan.getAkuns()) {
                                                        sql.createAkunDTHSKPD(a, kegiatanindex, conn);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (rincian.getPajaks() != null) {
                                        for (PajakDTHSKPD_WS pajak : rincian.getPajaks()) {
                                            sql.createPajakDTHSKPD(pajak, idrincian, conn);
                                        }
                                    }
                                }
                            }         
                    Date eins = new Date();
                    System.out.println(sdf.format(eins)+ " Akhir Insert DTH per periode : " + obj.getNamaPemda());
                    parseWaktu("Jumlah waktu insert DTH per periode : ", sins, eins);
                } else {
                    throw new Exception("Tidak dapat input DTH karena data DTH sudah dalam status Final");
                }
            }
            conn.commit();
            return "Sukses melakukan input data DTH ";
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal input DTH \n" + ex.getMessage());
        } finally {            
            conn.setAutoCommit(true);
//            try {
//                conn.setTransactionIsolation(currentLevel);
//                System.out.println("transaksi setelah " + conn.getTransactionIsolation());
//            } catch (Exception e) {
//                throw new Exception("rubah transksi \n" + e.getMessage());
//            }            
        }
    }

    /*@Override
    public String createDTHperPeriode(DTH_WS obj, Connection conn) throws Exception {
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (obj != null) {
                if (obj.getKodeSatker() != null) {
                boolean fin = sql.cekFinalDTH(obj.getKodeSatker(), obj.getTahunAnggaran(), obj.getPeriode(), conn);
                if (!fin) {
                    sql.deleteDTH(obj.getKodeSatker(), obj.getTahunAnggaran(), obj.getPeriode(), conn);
                    long index = sql.createDTH(obj, conn);
                    if (obj.getRincians() != null) {
                                for (RincianDTHSKPD_WS rincian : obj.getRincians()) {
                                    long idrincian = sql.createRincianDTHSKPD(rincian, index, conn);
                                    if (rincian.getKegiatans() != null) {
                                        for (SP2DKegiatan_WS kegiatan : rincian.getKegiatans()) {
                                            long kegiatanindex = sql.createSP2DKegiatan(kegiatan, idrincian, conn);
                                            if (kegiatan.getAkuns() != null) {
                                                if (kegiatan.getAkuns().size() > 0) {
                                                    for (AkunDTHSKPD_WS a : kegiatan.getAkuns()) {
                                                        sql.createAkunDTHSKPD(a, kegiatanindex, conn);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (rincian.getPajaks() != null) {
                                        for (PajakDTHSKPD_WS pajak : rincian.getPajaks()) {
                                            sql.createPajakDTHSKPD(pajak, idrincian, conn);
                                        }
                                    }
                                }
                            }                    
                } else {
                    throw new Exception("Tidak dapat input DTH karena data DTH sudah dalam status Final");
                }
            }
            }
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);
            return "Sukses melakukan input data DTH";
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
            }
            throw new Exception("Gagal input DTH \n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }
    }*/
    
    @Override
    public String getDTHInfo(String kodeSatker, short year, short periode, Connection conn) throws Exception{
        try {
            String result = "";
            if (kodeSatker != null) {
                result = sql.getDTHInfo(kodeSatker, year, periode, conn);
            }
            return result;
        } catch (SQLException ex) {
            throw new Exception("Gagal mengambil Informasi data APBD \n" + ex.getMessage());
        }
    }

    @Override
    public String createKendaraan(Kendaraan_WS object, Connection conn) throws Exception {
        try {
//            int currentLevel = conn.getTransactionIsolation();
//            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (object != null) {
                if (object.getKodeSatker() != null && object.getKodePemda() != null) {
                    boolean ss = sql.cekFinalKendaraan(object.getKodeSatker(), object.getTahunAnggaran(), conn);
                    if (!ss) {
                        sql.deleteKendaraan(object.getKodeSatker(), object.getKodePemda(), object.getTahunAnggaran(), conn);

                        long index = sql.createKendaraan(object, conn);
                        if (object.getRincians() != null) {
                            for (RincianKendaraan_WS rincian : object.getRincians()) {
                                sql.createRincianKendaraan(rincian, index, conn);
                            }
                        }
                    } else {
                        throw new Exception("Status Data Kepemilikan Kendaraan dalam Keadaan Final,\n Data Tidak Dapat Dirubah Kembali");
                    }
                }
            }
            conn.commit();
            conn.setAutoCommit(true);
//            conn.setTransactionIsolation(currentLevel);
            return "Sukses melakukan input data Kepemilikan Kendaraan";
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
                throw new Exception("Gagal input data Kepemilikan Kendaraan \n" + exc.getMessage());
            }
            throw new Exception("Gagal input data Kepemilikan Kendaraan  \n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }
    }

    @Override
    public String createHotel(Hotel_WS object, Connection conn) throws Exception {
        try {
//            int currentLevel = conn.getTransactionIsolation();
//            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (object != null) {
                if (object.getKodeSatker() != null && object.getKodePemda() != null) {
                    boolean ss = sql.cekFinalHotel(object.getKodeSatker(), object.getTahunAnggaran(), conn);
                    if (!ss) {
                        sql.deleteHotel(object.getKodeSatker(), object.getKodePemda(), object.getTahunAnggaran(), conn);

                        long index = sql.createHotel(object, conn);
                        if (object.getRincians() != null) {
                            for (RincianHotel_WS rincian : object.getRincians()) {
                                sql.createRincianHotel(rincian, index, conn);
                            }
                        }
                    } else {
                        throw new Exception("Status Data Kepemilikan Hotel dalam Keadaan Final,\n Data Tidak Dapat Dirubah Kembali");
                    }
                }
            }
            conn.commit();
            conn.setAutoCommit(true);
//            conn.setTransactionIsolation(currentLevel);
            return "Sukses melakukan input data Kepemilikan Hotel";
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
                throw new Exception("Gagal input data Kepemilikan Hotel\n" + exc.getMessage());
            }
            throw new Exception("Gagal input data Kepemilikan Hotel\n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }
    }

    @Override
    public String createHiburan(Hiburan_WS object, Connection conn) throws Exception {
        try {
//            int currentLevel = conn.getTransactionIsolation();
//            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (object != null) {
                if (object.getKodeSatker() != null && object.getKodePemda() != null) {
                    boolean ss = sql.cekFinalHiburan(object.getKodeSatker(), object.getTahunAnggaran(), conn);
                    if (!ss) {
                        sql.deleteHiburan(object.getKodeSatker(), object.getKodePemda(), object.getTahunAnggaran(), conn);

                        long index = sql.createHiburan(object, conn);
                        if (object.getRincians() != null) {
                            for (RincianHiburan_WS rincian : object.getRincians()) {
                                sql.createRincianHiburan(rincian, index, conn);
                            }
                        }
                    } else {
                        throw new Exception("Status Data Kepemilikan Tempat Hiburan dalam Keadaan Final,\n Data Tidak Dapat Dirubah Kembali");
                    }
                }
            }
            conn.commit();
            conn.setAutoCommit(true);
//            conn.setTransactionIsolation(currentLevel);
            return "Sukses melakukan input data Kepemilikan Tempat Hiburan";
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
                throw new Exception("Gagal input data Kepemilikan Tempat Hiburan\n" + exc.getMessage());
            }
            throw new Exception("Gagal input data Kepemilikan Tempat Hiburan\n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }
    }

    @Override
    public String createIzinUsaha(IzinUsaha_WS object, Connection conn) throws Exception {
        try {
//            int currentLevel = conn.getTransactionIsolation();
//            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (object != null) {
                if (object.getKodeSatker() != null && object.getKodePemda() != null) {
                    boolean ss = sql.cekFinalIzinUsaha(object.getKodeSatker(), object.getTahunAnggaran(), conn);
                    if (!ss) {
                        sql.deleteIzinUsaha(object.getKodeSatker(), object.getKodePemda(), object.getTahunAnggaran(), conn);

                        long index = sql.createIzinUsaha(object, conn);
                        if (object.getRincians() != null) {
                            for (RincianIzinUsaha_WS rincian : object.getRincians()) {
                                sql.createRincianIzinUsaha(rincian, index, conn);
                            }
                        }
                    } else {
                        throw new Exception("Status Data Izin Usaha dalam Keadaan Final,\n Data Tidak Dapat Dirubah Kembali");
                    }
                }
            }
            conn.commit();
            conn.setAutoCommit(true);
//            conn.setTransactionIsolation(currentLevel);
            return "Sukses melakukan input data Izin Usaha";
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
                throw new Exception("Gagal input data Izin Usaha\n" + exc.getMessage());
            }
            throw new Exception("Gagal input data Izin Usaha\n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }
    }

    @Override
    public String createBPHTB(BPHTB_WS object, Connection conn) throws Exception {
        try {
//            int currentLevel = conn.getTransactionIsolation();
//            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (object != null) {
                if (object.getKodeSatker() != null && object.getKodePemda() != null) {
                    boolean ss = sql.cekFinalBPHTB(object.getKodeSatker(), object.getTahunAnggaran(), conn);
                    if (!ss) {
                        sql.deleteBPHTB(object.getKodeSatker(), object.getKodePemda(), object.getTahunAnggaran(), conn);

                        long index = sql.createBPHTB(object, conn);
                        if (object.getRincians() != null) {
                            for (RincianBPHTB_WS rincian : object.getRincians()) {
                                sql.createRincianBPHTB(rincian, index, conn);
                            }
                        }
                    } else {
                        throw new Exception("Status data BPHTB dalam Keadaan Final, \n Data Tidak Dapat Dirubah Kembali");
                    }
                }
            }
            conn.commit();
            conn.setAutoCommit(true);
//            conn.setTransactionIsolation(currentLevel);
            return "Sukses melakukan input data BPHTB";
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
                throw new Exception("Gagal input data BPHTB\n" + exc.getMessage());
            }
            throw new Exception("Gagal input data BPHTB\n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }
    }

    @Override
    public String createIMB(IMB_WS object, Connection conn) throws Exception {
        try {
//            int currentLevel = conn.getTransactionIsolation();
//            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (object != null) {
                if (object.getKodeSatker() != null && object.getKodePemda() != null) {
                    boolean ss = sql.cekFinalIMB(object.getKodeSatker(), object.getTahunAnggaran(), conn);
                    if (!ss) {
                        sql.deleteIMB(object.getKodeSatker(), object.getKodePemda(), object.getTahunAnggaran(), conn);

                        long index = sql.createIMB(object, conn);
                        if (object.getRincians() != null) {
                            for (RincianIMB_WS rincian : object.getRincians()) {
                                sql.createRincianIMB(rincian, index, conn);
                            }
                        }
                    } else {
                        throw new Exception("Status Data IMB dalam Keadaan Final,\n Data Tidak Dapat Dirubah Kembali");
                    }
                }
            }
            conn.commit();
            conn.setAutoCommit(true);
//            conn.setTransactionIsolation(currentLevel);
            return "Sukses melakukan input data IMB";
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
                throw new Exception("Gagal input data IMB\n" + exc.getMessage());
            }
            throw new Exception("Gagal input data IMB\n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }
    }

    @Override
    public String createRestoran(Restoran_WS object, Connection conn) throws Exception {
        try {
//            int currentLevel = conn.getTransactionIsolation();
//            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (object != null) {
                if (object.getKodeSatker() != null && object.getKodePemda() != null) {
                    boolean ss = sql.cekFinalRestoran(object.getKodeSatker(), object.getTahunAnggaran(), conn);
                    if (!ss) {
                        sql.deleteRestoran(object.getKodeSatker(), object.getKodePemda(), object.getTahunAnggaran(), conn);

                        long index = sql.createRestoran(object, conn);
                        if (object.getRincians() != null) {
                            for (RincianRestoran_WS rincian : object.getRincians()) {
                                sql.createRincianRestoran(rincian, index, conn);
                            }
                        }
                    } else {
                        throw new Exception("Status Data Kepemilikan Restoran dalam Keadaan Final,\n Data Tidak Dapat Dirubah Kembali");
                    }
                }
            }
            conn.commit();
            conn.setAutoCommit(true);
//            conn.setTransactionIsolation(currentLevel);
            return "Sukses melakukan input data Kepemilikan Restoran";
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
                throw new Exception("Gagal input data Kepemilikan Restoran\n" + exc.getMessage());
            }
            throw new Exception("Gagal input data Kepemilikan Restoran\n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }
    }
    
    
    @Override
    public String createPajakDanRetribusiDaerah(PajakDanRetribusi_WS object, Connection conn) throws Exception {
        try {
//            int currentLevel = conn.getTransactionIsolation();
//            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            if (object != null) {
                if (object.getKodeSatker() != null ) {
                    boolean ss = sql.cekFinalPajakDanRetribusiDaerah(object.getKodeSatker(), object.getTahunAnggaran(), conn);
                    if (!ss) {
                        sql.deletePajakDanRetribusiDaerah(object.getKodeSatker(), object.getTahunAnggaran(), conn);

                        long index = sql.createPajakDanRetribusiDaerah(object, conn);
                        if (object.getRincians() != null) {
                            for (RincianPajakDanRetribusi_WS rincian : object.getRincians()) {
                                sql.createRincianPajakDanRetribusiDaerah(rincian, index, conn);
                            }
                        }
                    } else {
                        throw new Exception("Status data Pajak Dan Retribusi Daerah dalam Keadaan Final, \n Data Tidak Dapat Dirubah Kembali");
                    }
                }
            }
            conn.commit();
            conn.setAutoCommit(true);
//            conn.setTransactionIsolation(currentLevel);
            return "Sukses melakukan input data Pajak Dan Reribusi Daerah";
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException exc) {
                throw new Exception("Gagal input data Pajak Dan Reribusi Daerah\n" + exc.getMessage());
            }
            throw new Exception("Gagal input data Pajak Dan Reribusi Daerah\n" + ex.getMessage());
        } finally {
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        }
    }

}
